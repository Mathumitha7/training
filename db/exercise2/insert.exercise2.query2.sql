2.Write a query to insert at least 5 employees for each department

INSERT INTO `employee`.`department` (`department_id`, `department_name`) VALUES ('1001', 'IT Desk');
INSERT INTO `employee`.`department` (`department_id`, `department_name`) VALUES ('1002', 'Finance');
INSERT INTO `employee`.`department` (`department_id`, `department_name`) VALUES ('1003', 'Engineering');
INSERT INTO `employee`.`department` (`department_id`, `department_name`) VALUES ('1004', 'HR');
INSERT INTO `employee`.`department` (`department_id`, `department_name`) VALUES ('1005', 'Recruitment');
INSERT INTO `employee`.`department` (`department_id`, `department_name`) VALUES ('1006', 'Facility');


INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('a101', 'arjun', 'rav', '2001-01-01', '2022-08-09', '300000', '1001');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('a102', 'birbal', 'm', '1998-07-23', '2020-12-15', '150000', '1001');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('a103', 'sujith', 'kumar', '2001-07-03', '2022-03-12', '200000', '1001');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('a104', 'vijay', 'w', '1990-03-23', '2011-09-16', '300000', '1001');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('a105', 'mala', 'shree', '1993-05-05', '2015-09-14', '500000', '1001');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('b101', 'roja', 'r', '1991-07-03', '2013-08-01', '150000', '1002');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('b102', 'preetha', 'vaishnavi', '1980-04-08', '2002-07-12', '450000', '1002');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('b103', 'isbella', 'm', '1992-06-06', '2015-09-12', '200000', '1002');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('b104', 'zara', '1990-05-23', '2011-04-05', '250000', '1002');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('b105', 'fasila', 'asil', '2000-03-26', '2023-06-08', '350000', '1002');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('c101', 'ram','charan', '2000-04-21', '2022-08-09', '150000', '1003');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('c102', 'siva', 'sankari', '2001-03-04', '2024-05-15', '300000', '1003');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('c103', 'diya', 'agarwal', '2003-03-14', '2023-04-10', '230000', '1003');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('c104', 'dev', 'e', '2005-01-05', '2025-01-05', '410000', '1003');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('c105', 'kajol', '2012-09-08', '2001-06-18', '250000', '1003');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('d101', 'sneha', 'e', '2001-03-08', '2020-12-09', '300000', '1004');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('d102', 'ashok', 'raj', '2012-08-06', '2032-03-03', '120000', '1004');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('d103', 'akshay', 'kumar', '2003-04-17', '2025-12-29', '180000', '1004');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('d104', 'jeevan', 'prakash', '1998-05-23', '2018-07-18', '150000', '1004');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('d105', 'aravind', 'kumar', '2000-10-16', '2021-01-30', '120000', '1004');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('e101', 'reshma', 'sri', '2006-11-09', '2026-01-31', '300000', '1005');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('e102', 'thiranya', '2013-07-21', '2033-08-15', '450000', '1005');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('e103', 'girish', '2009-08-19', '2029-05-15', '300000', '1005');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('e104', 'athulya', 'ravi', '2008-07-28', '2027-07-30', '100000', '1005');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('e105', 'archana', 'singh', '2003-04-17', '2023-06-29', '120000', '1005');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('f101', 'sumithra', 'o', '2003-11-11', '2022-09-12', '400000', '1006');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('f102', 'rajesh', 'p', '2002-02-02', '2022-09-29', '490000', '1006');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('f103', 'elkkiya', 'g', '2003-02-14', '2023-10-10', '520000', '1006');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('f104', 'deepika', '2000-06-27', '2020-10-05', '560000', '1006');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `department_id`) VALUES ('f105', 'aparna', 'thangaraj', '2003-08-26', '2025-11-23', '320000', '1006');

