1. Create tables to hold employee(emp_id, first_name, surname, dob, date_of_joining, annual_salary), possible depts: ITDesk, Finance, Engineering, HR, Recruitment, Facility

    employee number must be the primary key in employee table
    department number must be the primary key in department table
    department number is the foreign key in the employee table

CREATE TABLE `employee`.`employee` (
  `emp_id` VARCHAR(50) NOT NULL,
  `first_name` VARCHAR(45) NOT NULL,
  `surname` VARCHAR(15) NULL,
  `dob` DATE NOT NULL,
  `date_of_joining` DATE NOT NULL,
  `annual_salary` INT NOT NULL,
  PRIMARY KEY (`emp_id`));
  
  CREATE TABLE `employee`.`department` (
  `department_id` INT NOT NULL,
  `department_name` VARCHAR(45) NOT NULL,
  `location` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`department_id`));
  
ALTER TABLE `employee`.`employee` 
ADD COLUMN `department_id` INT NULL AFTER `annual_salary`;
ALTER TABLE `employee`.`employee` 
ADD INDEX `department_id_department_idx` (`department_id` ASC) VISIBLE;
;

ALTER TABLE `employee`.`employee` 
ADD CONSTRAINT `department_id_department`
  FOREIGN KEY (`department_id`)
  REFERENCES `employee`.`department` (`department_id`)
  ON DELETE RESTRICT
  ON UPDATE RESTRICT;
