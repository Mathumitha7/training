10. Prepare a query to find out fresher(no department allocated employee) in the employee, where no matching records in the department table.

INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `area`) VALUES ('s100', 'bharath', 'k', '2000-03-01', '2020-12-12', '230000', 'annur');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `dob`, `date_of_joining`, `annual_salary`, `area`) VALUES ('s101', 'surya', '2001-09-30', '2021-02-09', '300000', 'nilgiri');
INSERT INTO `employee`.`employee` (`emp_id`, `first_name`, `surname`, `dob`, `date_of_joining`, `annual_salary`, `area`) VALUES ('h500', 'jooshika', 'anad', '2002-02-14', '2024-07-13', '120000', 'ariyalur');

select  first_name,surname,dob,annual_salary,area from employee where department_id is null;
