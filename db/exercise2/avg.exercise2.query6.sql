4. Write a query to increment 10% of every employee salary

select annual_salary+(annual_salary*10/100) from employee;


6.Find out the averagesalary

select AVG(annual_salary) from employee;

5.Find out the highest and least paid in all the department
select department_id,first_name,surname,dob,date_of_joining,max(annual_salary) from employee;
select department_id,first_name,surname,dob,date_of_joining,min(annual_salary) from employee;




7.Prepare an example for self-join
select a1.first_name,a1.surname from employee a1,employee a2 where a1.department_id=a2.department_id and a2.first_name="birbal";


8. Write a query to list out employees from the same area, and from the same department
ALTER TABLE `employee`.`employee` 
ADD COLUMN `area` VARCHAR(45) NOT NULL AFTER `department_id`;

UPDATE `employee`.`employee` SET `area` = 'chennai' WHERE (`emp_id` = 'a101');
UPDATE `employee`.`employee` SET `area` = 'erode' WHERE (`emp_id` = 'a102');
UPDATE `employee`.`employee` SET `area` = 'tripur' WHERE (`emp_id` = 'a103');
UPDATE `employee`.`employee` SET `area` = 'vellore' WHERE (`emp_id` = 'a104');
UPDATE `employee`.`employee` SET `area` = 'annur' WHERE (`emp_id` = 'a105');
UPDATE `employee`.`employee` SET `area` = 'kodaikanal' WHERE (`emp_id` = 'b101');
UPDATE `employee`.`employee` SET `area` = 'mysore' WHERE (`emp_id` = 'b102');
UPDATE `employee`.`employee` SET `area` = 'theni' WHERE (`emp_id` = 'b103');
UPDATE `employee`.`employee` SET `area` = 'sivagangai' WHERE (`emp_id` = 'b104');
UPDATE `employee`.`employee` SET `area` = 'trichy' WHERE (`emp_id` = 'b105');
UPDATE `employee`.`employee` SET `area` = 'covai' WHERE (`emp_id` = 'c101');
UPDATE `employee`.`employee` SET `area` = 'palani' WHERE (`emp_id` = 'c102');
UPDATE `employee`.`employee` SET `area` = 'utterkhand' WHERE (`emp_id` = 'c103');
UPDATE `employee`.`employee` SET `area` = 'kerala' WHERE (`emp_id` = 'c104');
UPDATE `employee`.`employee` SET `area` = 'kochin' WHERE (`emp_id` = 'c105');
UPDATE `employee`.`employee` SET `area` = 'trivandram' WHERE (`emp_id` = 'd101');
UPDATE `employee`.`employee` SET `area` = 'karnataka' WHERE (`emp_id` = 'd102');
UPDATE `employee`.`employee` SET `area` = 'dindugal' WHERE (`emp_id` = 'd103');
UPDATE `employee`.`employee` SET `area` = 'cuddalore' WHERE (`emp_id` = 'd104');
UPDATE `employee`.`employee` SET `area` = 'ooty' WHERE (`emp_id` = 'd105');
UPDATE `employee`.`employee` SET `area` = 'nilgiris' WHERE (`emp_id` = 'e101');
UPDATE `employee`.`employee` SET `area` = 'covai' WHERE (`emp_id` = 'e102');
UPDATE `employee`.`employee` SET `area` = 'vellore' WHERE (`emp_id` = 'e103');
UPDATE `employee`.`employee` SET `area` = 'madhyapradesh' WHERE (`emp_id` = 'e104');
UPDATE `employee`.`employee` SET `area` = 'odisha' WHERE (`emp_id` = 'e105');
UPDATE `employee`.`employee` SET `area` = 'salem' WHERE (`emp_id` = 'f101');
UPDATE `employee`.`employee` SET `area` = 'madurai' WHERE (`emp_id` = 'f102');
UPDATE `employee`.`employee` SET `area` = 'vandalur' WHERE (`emp_id` = 'f103');
UPDATE `employee`.`employee` SET `area` = 'chengalpattu' WHERE (`emp_id` = 'f104');

9. Write a query to find out employee birthday falls on that day
10. Prepare a query to find out fresher(no department allocated employee) in the employee, where no matching records in the department table.
11. Write a query to get employee names and their respective department name
