10.. Display below result in one SQL run
a) Shows students details who scored above 8 GPA for the given
semester

SELECT student.`id`
	  ,student.`roll_number`
      ,student.`name`
      ,student.`gender`
      ,college.`code`
      ,college.`name`
      ,semester_result.`grade`
      ,semester_result.`gpa`
FROM university
      ,college
      ,student
      ,semester_result
WHERE university.`univ_code` = college.`univ_code`
AND student.`college_id` = college.`id`
AND semester_result.`stud_id` = student.`id`
AND semester_result.`gpa`>8;
 
 b) Shows students details who scored above 5 GPA for the given
semester 
SELECT student.`id`
	  ,student.`roll_number`
      ,student.`name`
      ,student.`gender`
      ,college.`code`
      ,college.`name`
      ,semester_result.`grade`
      ,semester_result.`gpa`
FROM university
      ,college
      ,student
      ,semester_result
WHERE university.`univ_code` = college.`univ_code`
AND student.`college_id` = college.`id`
AND semester_result.`stud_id` = student.`id`
AND semester_result.`gpa`>5
ORDER BY `gpa`;