9. Show consolidated result for the following scenarios;
a) collected and uncollected semester fees amount per semester
for each college under an university. Result should be filtered
based on given year.
b) Collected semester fees amount for each university for the given
year 


ALTER TABLE `university management`.`semester_fee` 
ADD COLUMN `balance_amount` DOUBLE NOT NULL AFTER `paid_status`;

SELECT university.`university_name`
      ,college.`name`
      ,semester_fee.`semester`
      ,SUM(amount) AS 'collected_fees' 
      ,semester_fee.paid_year
FROM semester_fee
    ,university
    ,student
    ,college
WHERE semester_fee.stud_id = student.id
 AND student.college_id = college.id
 AND college.univ_code = university.univ_code
 AND university_name = 'anna'
 AND paid_year = '2005'
 AND semester = '12'
 AND paid_status = 'notpaid';

b) Collected semester fees amount for each university for the given 
    year  

SELECT university.`university_name`
      ,SUM(amount) AS 'collected_fees' 
      ,semester_fee.paid_year
FROM semester_fee
    ,university
    ,student
    ,college
WHERE semester_fee.stud_id = student.id
 AND student.college_id = college.id
 AND college.univ_code = university.univ_code
 AND university.university_name = 'anna'
 AND paid_status = 'paid'
 AND paid_year = '2001'


