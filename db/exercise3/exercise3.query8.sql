
8. Find employee vacancy position in all the departments from all the
colleges and across the universities. Result should be populated with
following information.. Designation, rank, college, department and
university details.

SELECT designation.name 
  ,designation.rank
  ,university.univ_code
  ,college.name AS college_name
  ,department.depart_name
  ,university.university_name
  ,college.city
  ,college.state
  ,college.year_opened
FROM university 
  ,college 
  ,department 
  ,designation 
  ,college_department 
  ,employee  
WHERE college.univ_code = university.univ_code 
AND university.univ_code = department.univ_code 
AND college_department.college_id = college.id 
AND college_department.udept_code = department.dept_code
AND employee.cdept_id = college_department.cdept_id 
AND employee.desig_id = designation.id 
ORDER BY designation.rank;