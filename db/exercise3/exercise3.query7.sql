7. Update PAID_STATUS and PAID_YEAR in SEMESTER_FEE table who
has done the payment. Entries should be updated based on student
ROLL_NUMBER.
a) Single Update - Update one student entry
b) Bulk Update - Update multiple students entries
c) PAID_STATUS value as ‘Paid’
d) PAID_YEAR value as ‘year format’



UPDATE semester_fee  
SET paid_year = (2017)
WHERE stud_id = 13;
UPDATE semester_fee  
SET paid_status = 'notpaid'
WHERE stud_id = 14; 
UPDATE semester_fee
SET paid_status =
  (case stud_id when 11 then 'paid'
                when 12 then 'notpaid'
                when 15 then 'notpaid'
                when 14 then 'notpaid'
    end)
WHERE stud_id in(11,12,15,14);
UPDATE semester_fee
SET paid_year =
  (case stud_id when 11 then (2001)
                when 12 then (2005)
                
    end)
WHERE stud_id in(11,12); 
 
SELECT * FROM semester_fee;
