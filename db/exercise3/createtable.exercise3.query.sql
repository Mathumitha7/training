create schema institution;
CREATE TABLE `institution`.`university` (
  `univ_code` CHAR(4) NOT NULL,
  `university_name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`univ_code`));

CREATE TABLE `institution`.`designation` (
  `id` INT NOT NULL,
  `name` VARCHAR(30) NOT NULL,
  `rank` CHAR(1) NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `institution`.`college` (
  `id` INT NOT NULL,
  `code` CHAR(4) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `univ_code` CHAR(4) NULL,
  `city` VARCHAR(50) NOT NULL,
  `state` VARCHAR(50) NOT NULL,
  `year_opened` YEAR(4) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `univ_code_institution_idx` (`univ_code` ASC) VISIBLE,
  CONSTRAINT `univ_code_institution`
    FOREIGN KEY (`univ_code`)
    REFERENCES `institution`.`university` (`univ_code`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT);
	
	CREATE TABLE `institution`.`department` (
  `dept_code` CHAR(4) NOT NULL,
  `depart_name` VARCHAR(50) NOT NULL,
  `univ_code` CHAR(4) NOT NULL,
  PRIMARY KEY (`dept_code`),
  INDEX `univ_code_college_idx` (`univ_code` ASC) VISIBLE,
  CONSTRAINT `univ_code_college`
    FOREIGN KEY (`univ_code`)
    REFERENCES `institution`.`university` (`univ_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
CREATE TABLE `institution`.`college_department` (
  `cdept_id` INT NOT NULL,
  `udept_code` CHAR(4) NOT NULL,
  `college_id` INT NOT NULL,
  PRIMARY KEY (`cdept_id`),
  INDEX `udept_code_department_idx` (`udept_code` ASC) VISIBLE,
  CONSTRAINT `udept_code_department`
    FOREIGN KEY (`udept_code`)
    REFERENCES `institution`.`department` (`dept_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
	
	ALTER TABLE `institution`.`college_department` 
ADD INDEX `college_id_college_idx` (`college_id` ASC) VISIBLE;
;
ALTER TABLE `institution`.`college_department` 
ADD CONSTRAINT `college_id_college`
  FOREIGN KEY (`college_id`)
  REFERENCES `institution`.`college` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

CREATE TABLE `institution`.`employee` (
  `id` INT NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `dob` DATE NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `phone` VARCHAR(45) NOT NULL,
  `college_id` INT NOT NULL,
  `cdept_id` INT NOT NULL,
  `desig_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `college_id_college_department_idx` (`college_id` ASC) VISIBLE,
  CONSTRAINT `college_id_college_department`
    FOREIGN KEY (`college_id`)
    REFERENCES `institution`.`college_department` (`cdept_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

ALTER TABLE `institution`.`employee` 
ADD INDEX `cdept_id_college_department_idx` (`cdept_id` ASC) VISIBLE;
;
ALTER TABLE `institution`.`employee` 
ADD CONSTRAINT `cdept_id_college_department`
  FOREIGN KEY (`cdept_id`)
  REFERENCES `institution`.`college_department` (`cdept_id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

ALTER TABLE `institution`.`employee` 
ADD INDEX `desig_id_designation_idx` (`desig_id` ASC) VISIBLE;
;
ALTER TABLE `institution`.`employee` 
ADD CONSTRAINT `desig_id_designation`
  FOREIGN KEY (`desig_id`)
  REFERENCES `institution`.`designation` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

CREATE TABLE `institution`.`syllabus` (
  `id` INT NOT NULL,
  `syallabus_code` CHAR(4) NOT NULL,
  `syllabus_name` VARCHAR(100) NOT NULL,
  `cdeptid` CHAR(4) NOT NULL,
  PRIMARY KEY (`id`, `cdeptid`),
  INDEX `cdeptid-college_department_idx` (`cdeptid` ASC) VISIBLE,
  CONSTRAINT `cdeptid_college_department`
    FOREIGN KEY (`cdeptid`)
    REFERENCES `institution`.`college_department` (`udept_code`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
	
	CREATE TABLE `institution`.`professer_syllabus` (
  `emp_id` INT NULL,
  `semester` TINYINT(30) NULL,
  `syllabus_id` INT NOT NULL,
  CONSTRAINT `emp_id_employee`
    FOREIGN KEY (`emp_id`)
    REFERENCES `institution`.`employee` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
	
	ALTER TABLE `institution`.`professer_syllabus` 
CHANGE COLUMN `semester` `semester` TINYINT NOT NULL ,
CHANGE COLUMN `syllabus_id` `syllabus_id` INT NULL ,
ADD INDEX `syllabus_id_syllabus_idx` (`syllabus_id` ASC) VISIBLE;
;
ALTER TABLE `institution`.`professer_syllabus` 
ADD CONSTRAINT `syllabus_id_syllabus`
  FOREIGN KEY (`syllabus_id`)
  REFERENCES `institution`.`syllabus` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

CREATE TABLE `institution`.`student` (
  `roll_number` CHAR(8) NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `dob` DATE NOT NULL,
  `gender` CHAR(1) NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `phone` BIGINT(20) NOT NULL,
  `address` VARCHAR(200) NOT NULL,
  `academic_year` YEAR(4) NOT NULL,
  `cdept_id` INT NOT NULL,
  `college_id` INT NOT NULL,
  `id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `cdept_id_college_department_idx` (`cdept_id` ASC) VISIBLE,
  CONSTRAINT `cdeptid_college_student`
    FOREIGN KEY (`cdept_id`)
    REFERENCES `institution`.`college_department` (`cdept_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

ALTER TABLE `institution`.`student` 
ADD INDEX `college_id_student_idx` (`college_id` ASC) VISIBLE;
;
ALTER TABLE `institution`.`student` 
ADD CONSTRAINT `college_id_student`
  FOREIGN KEY (`college_id`)
  REFERENCES `institution`.`student` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

CREATE TABLE `institution`.`semester_fee` (
  `cdept_id` INT NOT NULL,
  `stud_id` INT NOT NULL,
  `semester` TINYINT(20) NOT NULL,
  `amount` DOUBLE NOT NULL,
  `paid_year` YEAR(4) NOT NULL,
  `paid_status` VARCHAR(10) NOT NULL,
  INDEX `cdept_id_semester_fee_idx` (`cdept_id` ASC) VISIBLE,
  CONSTRAINT `cdept_id_semester_fee`
    FOREIGN KEY (`cdept_id`)
    REFERENCES `institution`.`college_department` (`cdept_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

ALTER TABLE `institution`.`semester_fee` 
ADD INDEX `stud_id_semester_fee_idx` (`stud_id` ASC) VISIBLE;
;
ALTER TABLE `institution`.`semester_fee` 
ADD CONSTRAINT `stud_id_semester_fee`
  FOREIGN KEY (`stud_id`)
  REFERENCES `institution`.`student` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;

CREATE TABLE `institution`.`semester_result` (
  `stud_id` INT NOT NULL,
  `syllabus_id` INT NOT NULL,
  `semester` TINYINT(20) NOT NULL,
  `grade` VARCHAR(2) NOT NULL,
  `credits` FLOAT NOT NULL,
  `result_date` DATE NOT NULL,
  INDEX `stud_id_semester_result_idx` (`stud_id` ASC) VISIBLE,
  CONSTRAINT `stud_id_semester_result`
    FOREIGN KEY (`stud_id`)
    REFERENCES `institution`.`student` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

ALTER TABLE `institution`.`semester_result` 
ADD INDEX `syllabus_id_semester_result_idx` (`syllabus_id` ASC) VISIBLE;
;
ALTER TABLE `institution`.`semester_result` 
ADD CONSTRAINT `syllabus_id_semester_result`
  FOREIGN KEY (`syllabus_id`)
  REFERENCES `institution`.`syllabus` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;
