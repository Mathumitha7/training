6.Create new entries in SEMESTER_FEE table for each student from all the colleges and across all the universities. These entries should be
created whenever new semester starts.. Each entry should have below default values;
a) AMOUNT - Semester fees
b) PAID_YEAR - Null
c) PAID_STATUS - Unpaid


ALTER TABLE `institution`.`semester_fee` 
ADD COLUMN `receipt_no` INT NOT NULL AFTER `paid_status`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`amount`, `receipt_no`);
;ALTER TABLE `institution`.`semester_fee` 
CHANGE COLUMN `amount` `amount` DOUBLE NULL DEFAULT 40000.00 ,
CHANGE COLUMN `paid_year` `paid_year` YEAR NULL ,
CHANGE COLUMN `paid_status` `paid_status` VARCHAR(10) NULL DEFAULT 'notpaid' ;
