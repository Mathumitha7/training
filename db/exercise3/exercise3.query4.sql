
4. List Employees details from a particular university along with their
college and department details. Details should be sorted by rank and
college name

SELECT employee.id
	  ,employee.name
      ,employee.dob
      ,employee.desig_id
      ,designation.name
      ,designation.rank
      ,college.name
      ,university.univ_code
      ,university.university_name
FROM employee,college,university,designation
WHERE college.id = employee.college_id
   AND college.univ_code = university.univ_code
   AND designation.id = employee.desig_id
   AND university.university_name = 'Anna'
   ORDER BY college.name
	   
	   
	   
	   