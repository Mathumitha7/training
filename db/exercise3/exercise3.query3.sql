3. Select students details who are studying under a particular university
and selected cities alone. Fetch 20 records for every run.
ROLL_NUMBER, NAME, GENDER, DOB, EMAIL, PHONE, ADDRESS,
COLLEGE_NAME, DEPARTMENT_NAME, HOD_NAME


SELECT student.`roll_number`
	  ,student.`name`
      ,student.`gender`
      ,student.`dob`
      ,student.`email`
      ,student.`phone`
      ,student.`address`
      ,college.`name`
      ,department.`depart_name`
      ,employee.`name`

FROM department,student,employee,college_department,college,university
WHERE university.univ_code = college.univ_code
   AND student.college_id = college.id 
   AND employee.college_id = college.id
   AND employee.desig_id = 1000
   AND student.cdept_id = college_department.cdept_id
   AND college_department.udept_code = department.dept_code
   AND university.university_name = 'anna'
   AND college.city = 'tripur'
   LIMIT 0,20;