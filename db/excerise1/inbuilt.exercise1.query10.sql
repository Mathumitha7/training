10.USE inbuilt Functions - now,MAX, MIN, AVG, COUNT, FIRST, LAST, SUM

select patient_id,patient_name,max(fees) from department;
select patient_id,patient_name,min(fees) from department;

select avg(fees) from department;

select count(patient_name) from department;

select sum(fees) as total from department;
