12.Read Records by Join using tables FOREIGN KEY relationships (CROSS, INNER, LEFT, RIGHT)

select department.patient_name,doctor.doctor_name from doctor inner join department on department.location=doctor.location;
select department.patient_name,doctor.doctor_name from doctor cross join department on department.location=doctor.location;
select department.patient_name,doctor.doctor_name from doctor left join department on department.location=doctor.location;
select department.patient_name,doctor.doctor_name from doctor right join department on department.location=doctor.location;
