9.
9.USE Where to filter records using AND,OR,NOT,LIKE,IN,ANY, wildcards ( % _ )

select patient_id
      ,patient_name
    	from department 
		where gender="F" and location="madurai";

select patient_id
       ,patient_name 
	      from department 
		  where gender="M" and location="vellore";


select patient_id
       ,patient_name 
	     from department
		 where gender="M" or location="annur";

select patient_id
       ,patient_name
     	   from department 
		   where not location="tripur";
		   
select patient_id
      ,patient_name 
	    from department 
		where not location="annur";		   

select patient_id
      ,patient_name 
	    from department 
		where gender like "M";	

select  patient_id
      ,patient_name 
	    from department 
		where location in ("annur","madurai");

select patient_name from department where patient_name=any(select location from department where gender='M');

select patient_id
       ,patient_name
	   ,location 
	    from department 
	    where location like "%i";		   
		
	   		
	   