13.At-least use 3 tables for Join
Union 2 Different table with same Column name (use Distinct,ALL)

CREATE TABLE `hospital`.`college` (
  `staff_name` VARCHAR(45) NOT NULL,
  `college_id` INT NOT NULL,
  PRIMARY KEY (`college_id`));
INSERT INTO `hospital`.`college` (`staff_name`, `college_id`) VALUES ('pooja', '23');
INSERT INTO `hospital`.`college` (`staff_name`, `college_id`) VALUES ('menaga', '34');

CREATE TABLE `hospital`.`student` (
  `student_id` INT NOT NULL,
  `college_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`student_id`));

INSERT INTO `hospital`.`student` (`student_id`, `college_name`) VALUES ('21', 'karpagam');
INSERT INTO `hospital`.`student` (`student_id`, `college_name`) VALUES ('30', 'johnson');

select student_id,college_name from student union select staff_name,college_id from college ;

select college.college_id,college.staff_name,department.patient_id,department.patient_name,student.student_id,student.college_name from college 
 inner join department on department.location=college.location
 inner join student on department.location=student.location
 order by college.location asc;
 