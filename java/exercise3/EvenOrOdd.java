/*
Requirements:
   - Convert the following anonymous class into lambda expression.
Entities:
   The class name called EvenOrOdd
   - CheckNumber (Interface)
Function Declaration:
   - int numbers(int number1,int number2);
   - public static void main(String[] args)
Jobs to be done:
   1.Create a class as EvenOrOdd with interface as CheckNumber
   2.Inside the declaring the single method with integer variable value parameters
   3.In the class main creating interface object and assigning with passing int value of statement if condition to check 
value mod of 2 is equal to 0 . if true return true, outside if block return false.
   4.Print statement invoking the interface single method with integer value and finally return the value using assigned
interface object lambda expression statements.
Psuedocode:
   
   //create an interface
    interface CheckNumber {
         public boolean isEven(int value);
     
    public class EvenOrOdd {
	    public static void main(String[] args) {
	    
	    //Convert into Java Lambda Expression
          CheckNumber number = (value) -> { 
            if (value % 2 == 0) {
                return true;
            } else return false;
        };
        
        //print the result
             System.out.println(number.isEven(11));

 */
program:

package com.java.training.core.lambda;
interface CheckNumber {
    public boolean isEven(int value);
}
public class EvenOrOdd {

	public static void main(String[] args) {
		CheckNumber number = (value) -> { // Convert into Java Lambda Expression 
            if (value % 2 == 0) {
                return true;
            } else return false;
        };
    System.out.println(number.isEven(11));
		

	}

}