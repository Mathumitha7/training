/*
Requirements:
which one of these is a valid lambda expression? and why?:
        (int x, int y) -> x+y; or (x, y) -> x + y;
*/

Explanation:
Both of them are valid lambda expressions if used in a correct context.

In first one (int x, int y) -> x+y; we know that the parameters must be of type int.

In second one (x, y) -> x + y; if used in a correct context type can be inferred from the context in which the lambda expression is executed.