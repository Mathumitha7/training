/*
Requirements:
Given path,check if path is file or directory
Entity:
The class named as CheckFileOrDirectory
Methodsignature:
  public static void main(String[] args)
Jobstobedone:
1.create a class called CheckFileOrDirectory
2.Create a object as file,dir,notExists and set the path name for all
 2.1 isFile() method ,check the given pathname is in file  
 2.2 If it is in file return true orelse false
 2.3 isDirectory() method ,check the given pathname is in directory
 2.4 If it is in directory return true orelse false
pseudocode:
public class CheckFileOrDirectory{

    public static void main(String[] args) {
        File file = new File("C:\\data\\char");
        File dir = new File("/Users/pankaj");
        File notExists = new File("C:\\data\\byteoutput1.txt");
        
        System.out.println("C:\\data\\char "+file.isFile());
        System.out.println("C:\\data\\char "+file.isDirectory());
        
        System.out.println("/Users/pankaj "+dir.isFile());
        System.out.println("/Users/pankaj "+dir.isDirectory());
        
        System.out.println("C:\\data\\byteoutput1.txt "+notExists.isFile());
        System.out.println("C:\\data\\byteoutput1.txt "+notExists.isDirectory());
    }

}
*/
package com.java.training.core.nio;
import java.io.File;

public class CheckFileOrDirectory{

    public static void main(String[] args) {
        File file = new File("C:\\data\\char");
        File dir = new File("/Users/pankaj");
        File notExists = new File("C:\\data\\byteoutput1.txt");
        
        System.out.println("C:\\data\\char "+file.isFile());
        System.out.println("C:\\data\\char "+file.isDirectory());
        
        System.out.println("/Users/pankaj "+dir.isFile());
        System.out.println("/Users/pankaj "+dir.isDirectory());
        
        System.out.println("C:\\data\\byteoutput1.txt "+notExists.isFile());
        System.out.println("C:\\data\\byteoutput1.txt "+notExists.isDirectory());
    }

}

