/*
Requirement:
     - Program to rounding the value to maximum of 3 digits by setting maximum and minimum Fraction digits.
     
Entity:
     - The class named as FractionDigits
     
Function Declaration:
  	 - public static void main(String[] args)
  	 
Jobs To Be Done:
  	1.create a object as numberformat,Invoke NumberFormat class getInstance() method and store it in numberFormat.
  	2.Set Minimun and Maximun fraction digits using setMinimumFractionDigits and setMaximumFractionDigits method.
        3.Invoke format() method with float value to print. 
    
PseudoCode:
public class FractionDigits  {
	public static void main(String[] args) {
		NumberFormat numberFormat = NumberFormat.getInstance();
		//Set Minimun fraction digits
		numberFormat.setMinimumFractionDigits(3);
		//Set Maximun fraction digits
		numberFormat.setMaximumFractionDigits(5);
		System.out.println(numberFormat.format(54.6432f));
	}
}

*/
package com.java.training.core.internalization;
import java.text.NumberFormat;

public class FractionDigits  {
	public static void main(String[] args) {
		NumberFormat numberFormat = NumberFormat.getInstance();
		
		numberFormat.setMinimumFractionDigits(3);
		
		numberFormat.setMaximumFractionDigits(5);
		System.out.println(numberFormat.format(54.6432f));
	}
}

