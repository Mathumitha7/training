/*
Requirements:
Write a Java program to calculate the revenue from a sale based on the unit price and quantity of a product input by the user.
The discount rate is 10% for the quantity purchased between 100 and 120 units, and 15% for the quantity purchased greater than 120 units. If the quantity purchased is less than 100 units, the discount rate is 0%. See the example output as shown below:
Enter unit price: 25
Enter quantity: 110
The revenue from sale: 2475.0$
After discount: 275.0$(10.0%)

Entities:
The class named as Product

FunctionDeclaration:
static void calculateSale()
public static void main(String[] args

Jobs to be done:
1.create a package and import util package
2.create a class called Product
3.In the main function call the function calculateSale()
4.Create a function calculateSale and initialize values of unitprice,quantity,revenue,discount_rate and discount_amount.
5.User gives the values for unitprice and quantity 
6.if the quantity is less than 100 calculate revenue as product of unitprice and quantity.
7.if the quantity lies between 100 and 120 calculate discount_rate ,revenue,discount_amount
  7.1. Calculate discount_rate as (float)10/100
  7.2. revenue as product of unitprice and quantity
  7.3. discount_amount as product of revenue and discount_rate
  7.4. revenue as difference of revenue and discount_amount

8.if the quantity is greater than 120 
  8.1. Calculate discount_rate as (float)15/100
  8.2. revenue as product of unitprice and quantity
  8.3. discount_amount as product of revenue and discount_rate
  8.4. revenue as difference of revenue and discount_amount

9.Print the revenue from sale and after discount amount

pseudocode:
public class Product {

	public static void main(String[] args) {
	//function call
	 calculateSale();
	 
	//function declaration
	 static void calculateSale() {
	    float unitprice=0f;
	    int quantity=0;
	    float revenue=0f;
	    float discount_rate=0f, discount_amount=0f;
	    Scanner sc=new Scanner(System.in);
		
	     //input unitprice and quantity
	     System.out.print("Enter unit price:");
	     unitprice=sc.nextFloat();
		 System.out.print("Enter quantity:");
		 quantity=sc.nextInt();
		 
		 //Using if condition
		  if(quantity<100) {
			revenue=unitprice*quantity;
			
			//Using else if 
		 else if(quantity>=100 && quantity<=120) {
			discount_rate=(float)10/100;
			revenue=unitprice*quantity;
			discount_amount=revenue*discount_rate;
			revenue-=discount_amount;
       
       //Using else if
        else if(quantity>120) {
			discount_rate=(float)15/100;
			revenue=unitprice*quantity;
			discount_amount=revenue*discount_rate;
			revenue-=discount_amount;
			
	    System.out.println("The revenue from sale:"+revenue+"$");
		System.out.println("After discount:"+discount_amount+"$("+discount_rate*100+"%)");
 */



program:

package com.java.training.core.logicaloperators;
import java.util.*;
 public class Product
{
public static void main(String[] args)
{
calculateSale();
}

static void calculateSale(){

float unitprice=0f;
int quantity=0;
float revenue=0f;
float discount_rate=0f, discount_amount=0f;

Scanner sc=new Scanner(System.in);
System.out.print("Enter unit price:");
unitprice=sc.nextFloat();
System.out.print("Enter quantity:");
quantity=sc.nextInt();

if(quantity<100)
revenue=unitprice*quantity;
else if(quantity>=100 && quantity<=120)
{
discount_rate=(float)10/100;
revenue=unitprice*quantity;
discount_amount=revenue*discount_rate;
revenue-=discount_amount;
}

else if(quantity>120)
{
discount_rate=(float)15/100;
revenue=unitprice*quantity;
discount_amount=revenue*discount_rate;
revenue-=discount_amount;
}

System.out.println("The revenue from sale:"+revenue+"$");
System.out.println("After discount:"+discount_amount+"$("+discount_rate*100+"%)");
}

}

