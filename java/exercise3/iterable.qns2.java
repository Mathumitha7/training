/*
Requirements:
Why except iterator() method in Iterable interface, are not necessary to define in the implemented class?
*/
Explanation:	
	The Iterator method is necessary to be overridden from the Iterable interface to the implementing Class because the iterator 
method makes the Class or the custom data type iterable but the rest of the methods such as spliterator does not makes the class iterable.
  	