Requirement:
To Create a list
   -Convert the list to a set.
   -Convert the list to a array.

Entities:
The class named ConversionOfList is used.

FunctionDeclaration:
 There is no function is declared 

Jobs to be done:
1.Create a package.
2.Import util package.
3.Declare the class ConversionOfList. 
4.Create an empty list and add the elements to it.
5.Use addAll() function,add the element of list a to list.
6.convert list to array and convert list to set.
7.Display the output.

program:

package com.java.training.core.list;
import java.util.*;
public class ConversionOfList {

 public static void main(String[] args) {
		 ArrayList<String> list=new ArrayList<String>();   
	      list.add("India");   
	      list.add("Pakistan");    
	      list.add("England");    
	      list.add("Thailand");    
	      list.add("Germany");
	      list.add("Nepal");
	      list.add("Iran");
	      list.add("Iraq");
	      list.add("Russia");
	      list.add("Malaysia");
	      list.add("Spain");
	      ArrayList<String> a = new ArrayList<String>();
	      a.add("Japan");
	      a.add("China");
	      list.addAll(a);
	     String[] myArray=new String[a.size()];
	     a.toArray(myArray);
	     for(int i=0;i<myArray.length;i++)
	     {
	    	 System.out.println("Element at index "+myArray[i]);
			
	     }
	     Set<String> s= new HashSet<String>(list);
	        System.out.println("Set is :" + s);
	     
 }
}