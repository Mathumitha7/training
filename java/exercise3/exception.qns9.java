/*
Requirements:
Difference between throw and throws with example.
*/
explanation:
throw:
Java throw keyword is used to explicitly throw an exception.	
Checked exception cannot be propagated using throw only.	
Throw is followed by an instance.
Throw is used within the method.	
You cannot throw multiple exceptions.	 
throws:
Java throws keyword is used to declare an exception.
Checked exception can be propagated with throws.
Throws is followed by class.
Throws is used with the method signature.
declare multiple exceptions e.g.
public void method()throws IOException,SQLException.

eg:
void m()throws ArithmeticException{  
throw new ArithmeticException("sorry");  
}  