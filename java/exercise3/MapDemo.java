Requirements:
To demonstrate java program to working of map interface using put(), remove(), booleanContainsValue(), replace() ) . If possible try remaining methods.

Entities:
The class named as MapDemo.

FunctionDeclaration:
public static void main(String[] args

Jobstobedone:
1.Create a package.
2.Import util package.
3.create a class callled MapDemo.
4.create a new Map named as map and add the elements to it
5.use remove() method to remove the element in map.
6.use boolean hasValue()method to check the value in that Map.
7.use boolean hasKey()method to check the Key in that Map.
8.use clear() method to clear all the elements in that Map.
9.print the result

program:

package com.java.training.core.setsandmaps;
import java.util.*;
public class MapDemo {

	public static void main(String[] args) {
		Map<String, String> map = new HashMap<>();
		map.put("1","one");
		map.put("2","two");
		map.put("3","three");
		map.put("4","four");
		map.put("5","five");
        System.out.println(""+map);
	    String p = map.remove("2");
	    System.out.println("The removed element is "+p);
		boolean hasValue = map.containsValue("five");
		System.out.println(""+hasValue);
		boolean hasKey = map.containsKey("2");
	     System.out.println(""+hasKey);
         map.clear();
		System.out.println("After cleared "+map);
		
	}

}
