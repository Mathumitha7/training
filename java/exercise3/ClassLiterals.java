/*
Requirement:
    - Write a program to demonstrate generics - class objects as type literals.
Entity:
     class named as ClassLiterals

Function Declaration:
    - public static void main(String[] args)
    - public void sound()
    - public static <T> boolean checkInterface(Class<?> theClass)
    
Jobs to be Done:
    1.Create a class ClassLiterals and implenting Animal interafce with  sound method().
    2.Declare a method public static <T> boolean checkInterface(Class<?> theClass) returning theClass.isInterface()
    3.In the sound() method print as "Cute" 
    4.In mainmethod,create a Integer class  and Animal interface 
    5.Declare integer class and printing boolean , getClass() method for getting class and getName() method for getting type
    6.Using TryCatch exception handling Checking class , getClass and getName presenr
    7.Print the result
*/
program:
interface Animal {
	public void sound();
}

public class ClassLiterals implements Animal {

    public static <T> boolean checkInterface(Class<?> theClass) {
        return theClass.isInterface();
    }

    public void sound() {
        System.out.println("Cute");
    }

    public static void main(String[] args) {
        Class<Integer> intClass = int.class;            
        boolean boolean1 = checkInterface(intClass);
        System.out.println(boolean1);                   
        System.out.println(intClass.getClass());        
        System.out.println(intClass.getName());         

        boolean boolean2 = checkInterface(ClassLiterals.class);
        System.out.println(boolean2);                   
        System.out.println(ClassLiterals.class.getClass());       
        System.out.println(ClassLiterals.class.getName());        

        boolean boolean3 = checkInterface(Animal.class);
        System.out.println(boolean3);                   
        System.out.println(Animal.class.getClass());    
        System.out.println(Animal.class.getName());     

        try {
            Class<?> errClass = Class.forName("Dog");
            System.out.println(errClass.getClass());
            System.out.println(errClass.getName());
        } catch (ClassNotFoundException ex) {
            System.out.println(ex.toString());
        }
    }
}
