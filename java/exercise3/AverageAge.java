/*
Requirements: 
    - Write a program to find the average age of all the Person in the person List
    
Entities:
    - The class named as AverageAge
    
Function Declaration:
    - public static void main(String[] args)
 
Job to be done:
    - Create class called AverageAge
    - Declare and define main method
    - Invoke the createRoster() method and store the returned value in a list-persons
    - Invoke streams method and map each element to Integer with mapToInt() method and invoke average() method.
    - Print the resultant value
pseudocode:
public class AverageAge {

    public static void main(String[] args) {
    	List<Person> persons = Person.createRoster();
    	System.out.println(persons.stream().mapToInt((person) -> person.getAge()).average().getAsDouble());
    }
}
 
*/
package com.java.training.core.Streams;
import java.util.List;
import java.util.stream.*;

public class AverageAge {

    public static void main(String[] args) {
    	List<Person> persons = Person.createRoster();
    	System.out.println(persons.stream().mapToInt((person) -> person.getAge()).average().getAsDouble());
    }
}

