/*
Requirements:
  	To write a program to print employees name list by implementing iterable interface.
Entity:
  	  - MyIterable
  	  -Employee
Function Declaration:
  	  - public MyIterable(T[] t),
  	  -	public Iterator<T> iterator().
  
Jobs To Be Done:
 1.Creating the MyIterable class that implements the Iterable.
 2.Creating the local variable list as generic type.
 3.Creating a method MyIterable of generic type  and assigning already defined String values from the main class.
 4.Creating the main class as Employee.
   4.1.Declaring the  values in the type of string.
   4.2 Creating the object List for the variables to be iterated.
   4.3After assigning the variables in the list by iterator, print the result.
pseudocode:
class MyIterable<T> implements Iterable<T> {
	private List<T> list;
	public MyIterable(T[] t) {
		list = Arrays.asList(t);
	}
	public Iterator<T> iterator() {
		return list.iterator();
	}
}
public class Employee {

	public static void main(String[] args) {
		String[] names= {"arun","abishek","deepa","varun","deepan"};
		MyIterable<String> a = new MyIterable<>(names);
		for (String string : a) {
			System.out.println(string);
		}
	}
}
*/
program:

package com.java.training.core.Generics;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

class MyIterable<T> implements Iterable<T> {
	private List<T> list;
	public MyIterable(T[] t) {
		list = Arrays.asList(t);
	}
	public Iterator<T> iterator() {
		return list.iterator();
	}
}
public class Employee {

	public static void main(String[] args) {
		String[] names= {"arun","abishek","deepa","varun","deepan"};
		MyIterable<String> a = new MyIterable<>(names);
		for (String string : a) {
			System.out.println(string);
		}
	}
}





