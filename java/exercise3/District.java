Requirements:
8 districts are shown below Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy   to be converted to UPPERCASE.

Entities:
The class named as District.

FunctionDeclaration:
public static void main(String[] args)


Jobs to be done:
1.create a package.
2.import a package.
3.Create a class called District
4.create a newlist and add the elements in that list
5.Elements in the list are converted to uppercase 
6.print the result.

program:

package com.java.training.core.Collections;
import java.util.*;
public class District {

	public static void main(String[] args) {
		List<String> list=new ArrayList<String>();
		list.add("Madurai");
		list.add("Coimbatore");
		list.add("Theni");
		list.add("Chennai");
		list.add("karur");
		list.add("Salem");
		list.add("Erode");
		list.add("Trichy");
		list.stream().map(district-> district.toUpperCase()).forEach(district -> System.out.print(district + ","));

	}

}
