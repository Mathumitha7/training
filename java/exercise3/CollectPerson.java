/*
Requirement:
    - Write a program to collect the minimal person with name and email address from the Person class using java.util.Stream<T> API as List
Entity:
    - The class named as CollectPerson

Function Declaration
    - public static void main(String[] args)

Jobs to be done:
     1.Create class called CollectPerson
     2.In mainmethod,Invoke the createRoster method using the Person class and store it in roster list.
     3.Get the person's email using Person class getEmailAddress and collect as list using asList method store it in emailRoster list.
     4.Check if the get email is present in roster list.
              4.1)Print the person's name and email.
     
Pseudo Code:
public class CollectPerson{

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        // creating a list and storing the returned list

        List<String> emailRoster = roster.stream()
            .map(Person::getEmailAddress)
            .collect(Collectors.toList());
        // created a list of emails from objects of person class using pipelined

        for (int i = 0; i < emailRoster.size(); i++) {
            for (int j = 0; j < roster.size(); j++) {
                if (roster.get(j).emailAddress == emailRoster.get(i)) {
                    System.out.println(
                        "Person name: " +
                        roster.get(j).name + ", mail Id: " +
                        emailRoster.get(i)
                    );
                }
            }
        }
        // using email to match the email in roster list to
        // get the name of the person.
    }
}

*/
package com.java.training.core.Streams;
import java.util.List;
import java.util.stream.Collectors;

public class CollectPerson{

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
       
        List<String> emailRoster = roster.stream()
            .map(Person::getEmailAddress)
            .collect(Collectors.toList());
       
        for (int i = 0; i < emailRoster.size(); i++) {
            for (int j = 0; j < roster.size(); j++) {
                if (roster.get(j).emailAddress == emailRoster.get(i)) {
                    System.out.println(
                        "Person name: " +
                        roster.get(j).name + ", mail Id: " +
                        emailRoster.get(i)
                    );
                }
            }
        }
       
    }
}


