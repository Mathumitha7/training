Requirements:
Write a generic method to exchange the positions of two different elements in an array.

Entities:
The class named as Algorithm

Functiondeclaration:
 public static <T> void swap(T[] a, int i, int j)

program:
package com.java.training.core.Generics;
import java.util.*;

	public final class Algorithm {
	    public static <T> void swap(T[] a, int i, int j) {
	        T temp = a[i];
	        a[i] = a[j];
	        a[j] = temp;
	    }
	}

