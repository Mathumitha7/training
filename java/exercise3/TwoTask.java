Requirements:
To Write a program of performing two tasks by two threads that implements Runnable interface.

Entities:
The class named as TwoTask

FunctionDeclaration:
public static void main(String[] args)
public void run()


Jobstobedone:
1.create a package
2.Import a util package
3.create a class called TwoTask
4.create runnable interface r1 which has function run() 
5.create runnable interface r2 which has function run()
6.create a new threads named as t1,t2
7.use start() method,print the output.

program:

package com.java.training.core.Concurrency;
import java.util.*;
public class TwoTask {

	public static void main(String[] args) {
		Runnable r1=new Runnable(){  
		    public void run(){  
		      System.out.println("task one");  
		    }  
		  };  
		  
		  Runnable r2=new Runnable(){  
		    public void run(){  
		      System.out.println("task two");  
		    }  
		  };  
		      
		  Thread t1=new Thread(r1);  
		  Thread t2=new Thread(r2);  
		  
		  t1.start();  
		  t2.start();  
		 }  
		}  

			  