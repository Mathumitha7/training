Requirement:
To demonstrate program explaining basic add and traversal operation of linked hash set

Entities:
The class named as LinkedHashSetDemo.

FunctionDeclaration:
public static void main(String[] args

Jobstobedone:
1.Create a package.
2.Import util package.
3.create a new linkedhashsetand add the elements to it
4.Iterate the elements in k.
5.print the output.

program:
package com.java.training.core.setsandmaps;

import java.util.*;


public class LinkedHashSetDemo {

	public static void main(String[] args) {
		Set<String> k=new LinkedHashSet<String>();
		k.add("A");
		k.add("B");
		k.add("C");
		k.add("D");
		k.add("E");
		System.out.println(""+k);
		Iterator<String> i = k.iterator();
		while(i.hasNext()) {
			System.out.println(""+i.next());
		}
	}

}
