/*
Requirement:
    To Write an example that, for a given year, reports the length of each month within that particular year.
    
Entity:
   The class named as MonthsInYear

Function Declaration:
    public static void main(String[] args);
Jobs To be Done:    
1.creta a package and import.
2.create a class called MonthsInYear
3.In main method,Scanner() is used Get the year from user
 3.1.invoke the class Year and pass the user input year as parameter
 3.2Get the month from user
 3.3invoke the Month  and pass the user input month as parameter 
4.invoke max length method and find the length of the month
5.Print the result.

pseudo code:

    public class LengthOfMonth {
    
        public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);//get input from user
        int year = scanner.nextInt();
        Year thisYear = Year.of(year);
        int month = scanner.nextInt();
        Month thismonth = Month.of(month);
        System.out.println(thismonth.maxLength());
    }
}

 */

program:
package com.java.training.core.dataandtime;
import java.time.Month;
import java.time.Year;
import java.util.Scanner;

public class MonthsInYear {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the year \n");
        int year = scanner.nextInt();
        Year thisYear = Year.of(year);
        System.out.print("Enter the month \n");
        int month = scanner.nextInt();
        Month thismonth = Month.of(month);
        System.out.println(thismonth.maxLength());
        scanner.close();
    }
}