Requirement:
  To  write a Java program to get the portion of a map whose keys range from a given key to another key.
     
Entity:
     class named as Key

Function Declaration:
    public static void main(String[] args)

Jobs to be Done:
    1. create a package com.java.training.core.Map
    2. Import the util packages.
    3. Declare the class Key
    4. Create an object for TreeMap and SortedMap as treeMap and subTreeMap.
    5. Now store the elements in TreeMap.
    6. Now print the range of keys from 60 to 90 in subTreeMap.

program:
package com.java.training.core.Map;
import java.util.*;

public class Key {

	public static void main(String[] args) {
	            TreeMap<Integer, String> treeMap = new TreeMap<>();
		        SortedMap<Integer, String> subTreeMap = new TreeMap<>();
		        treeMap.put(50, "Mango");
		        treeMap.put(60, "Orange");
		        treeMap.put(70, "Blueberry");
		        treeMap.put(80, "cherry");
		        treeMap.put(90, "Apple");
		        System.out.println("the treemap is " + treeMap);
		        subTreeMap = treeMap.subMap(60, 90);
		        System.out.println("the range is " + subTreeMap);
		    }

		}
	
