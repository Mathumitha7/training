/*
Requirements:
   - TO Write a Program to print difference of two numbers using lambda expression and the single method interface
Entities:
   The class named as Difference
   - Different (Interface)
Function Declaration:
   - int numbers(int number1,int number2);
   - public static void main(String[] args)
Jobs to be done:
   1.Create a class as Difference with interface as Different
   2.Inside the declaring the single method with two integer parameters.
   3.In the class main creating interface object and assigning the lambda expression to return two integers difference.
   4.Print statement invoking the interface single method with integer values and finally return the value using assigned interface object lambda expression 
Psuedocode:
  interface Different {
	int numbers(int num1,int num2);


     public class Difference{

	     public static void main(String[] args) {
	     
	     //create an object for interface different
	      Different different = (num1,num2) -> num1 - num2;
		System.out.println(different.numbers(30,20));
 */
program:

package com.java.training.core.lambda;
interface Different {
	int numbers(int num1,int num2);
}
public class Difference {

	public static void main(String[] args) {
		
		
					Different different = (num1,num2) -> num1 - num2;
					System.out.println(different.numbers(30,20));
				
			}

		}
