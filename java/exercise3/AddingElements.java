Requirement:
To  write a java program to demonstrate adding elements,displaying,removing,iterating in hashset.

Entities:
The class named AddingElements is used.

FunctionDeclaration:
 public static void main(String[] args)

Jobs to be done:
1.Create a package.
2.Import HashSet,Iterator packages.
3.Declare the class AddingElements. 
4.Create an empty hashset and add the elements to it.
5.Use remove() function,to remove particular element.
6.Iterate  the elements in hashset.
7.Display the output.

program:

package com.java.training.core.setsandmaps;

import java.util.HashSet;
import java.util.Iterator;

public class AddingElements {

	public static void main(String[] args) {
		HashSet<String> hash = new HashSet<String>();
		hash.add("black");
		hash.add("brown");
		hash.add("pink");
		hash.add("red");
		hash.add("blue");
		System.out.println(""+hash);
		hash.remove("red");
		System.out.println(""+hash);
		Iterator<String> i = hash.iterator();
		while(i.hasNext()) {
			System.out.println(""+i.next());
		}
	}

}
