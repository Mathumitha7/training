Requirement:
To Explain About contains(),sublist(),retainAll()

Entities:
The class named as ListDemo

FunctionDeclaration:
public static void main(String[] args)

Jobs to be done:
1.Create a package com.java.training.core.list.
2.Import util packages.
3.Declare the class ListDemo.
4.Create a new list and add the values in it.
5.use contains() method to check the element is present or not.
6.create another list , add the values and retainAll() method is used.
7.Create a list and Use substring() method.
8.Display the output.

Program:
package com.java.training.core.list;
import java.util.*;
import java.util.ArrayList;
import java.util.List;
public class ListDemo {
    public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(11);
		list.add(12);
		list.add(30);
		if(list.contains(20)==true) {
			System.out.println("element is Present");
		}
		else {
			System.out.println("element is Not Present");
		}
		ArrayList<Integer> m =new ArrayList<>();
		m.add(30);
		m.add(20);
		m.add(25);
		list.retainAll(m);
		System.out.println(" "+ list);
		ArrayList<String> string=new ArrayList<>();
		string.add("Welcome");
		string.add("to");
		string.add("Java");
		string.add("Programming");
		string.add("2020");
		System.out.println(" " +string);
		List<String> substring=new ArrayList<>();
		substring=string.subList(2,4);
		System.out.println(" "+substring);

	}

}
