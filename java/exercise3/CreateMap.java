Requirements:
To write a java program to copy all of the mappings from specified map to another map.

Entities:
class named as CreateMap

Functiondeclaration:
There is no function is declared.

Jobs to be done:
1.Create a package
2.Import a package util.
3.create class called CreateMap
4.create emptymap and add the values in that map and also create another map like that.
5.copy all the mappings from specified map to another map.
6.display the output.

program:

package com.java.training.core.Map;
import java.util.*;
public class CreateMap {

	public static void main(String[] args) {
		Map<Integer,String> m = new HashMap<Integer,String>();
		  m.put(1, "C#");
		  m.put(2, "C++");
		  m.put(3, "Java");
		  System.out.println(" "+m);
		  Map<Integer,String> b = new HashMap<Integer,String>();
		  b.put(4, "Python");
		  b.put(5, "Ruby");
		  System.out.println(" "+b);

		  b.putAll(m);
		  System.out.println("\nValues in a map are: " +b);
		
	}

}
