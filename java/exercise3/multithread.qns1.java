/* 
 Requirement:
 What are the arguments passed for the sleep() method?
*/
Explanation:
The arguments passed for the sleep() method:
      Prototype: public static void sleep (long millis) throws InterruptedException.
      Parameters: millis => the duration of time in milliseconds for which the thread sleeps.
      Return Value: void.
