/*
Requirements:
To read a file using java.io.file
Entities:
The class named as ReadFile
Functiondeclaration:
   public static void main(String[] args)
Jobs to be done:
1.create and import the package.
2.create class named as ReadFile
3.In main method,try and catch is used
  3.1 Set the path of the file and use scanner() to get the input
  3.2 In while loop,read the file and print
4.Use close() method,to close the readingfile.
Pseudocode:
public class ReadFile {
  public static void main(String[] args) {
    try {
      File myObj = new File("C:\\data\\byteoutput1.txt");//set the path to read the file
      Scanner myReader = new Scanner(myObj);//scanner() method
      while (myReader.hasNextLine()) {
        String data = myReader.nextLine();
        System.out.println(data);
      }
      myReader.close();
    } catch(Exception e){
        System.out.println(e);
  }
    }
  }

 */

program:
package com.java.training.core.IO;
import java.io.File; 
import java.util.Scanner;

public class ReadFile {
  public static void main(String[] args) {
    try {
      File myObj = new File("C:\\data\\byteoutput1.txt");
      Scanner myReader = new Scanner(myObj);
      while (myReader.hasNextLine()) {
        String data = myReader.nextLine();
        System.out.println(data);
      }
      myReader.close();
    } catch(Exception e){
        System.out.println(e);
  }
    }
  }

 