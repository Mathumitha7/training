/*
Requirements:
   - Write a program for each types with suitable comments.
Entities:
    The class named as  MethodReference
     -Addition
Interfaces:
   - StaticMethod
   - ParameterMethod
   - InstanceMethod
   - ConstructorReference
   
Function Declaration:
   - public static void display()
   - public void printer()
   - public int find(String string1, String string2)
   - public void print(String string)
   - Addition printAdd(int number1,int number2)
   - Addition(int number1,int number2)(Constructor)
   - public static void main(String[] args)
Jobs to be done:
   1.Create a class as MethodReference with method reference types 
          1.StaticMethod Interface
          2.ParameterMethod Interface
          3.InstanceMethod Interface
          4.ConstructorReference Interface
   2.Inside
         1.StaticMethod Interface declaring the single printer() method.
         2.ParameterMethod Interface declaring the single find() method with two String parameters.
         3.InstanceMethod Interface declaring the single print() method with a String parameters.
         4.ConstructorReference Interface declaring the single printAdd() method type of className with two integer parameters.
   3.In the class main creating object four interface and 
         1.Reference display() method using MethodReference class
         2.Reference indexOf() method inside String class method.
         3.Reference println() method inside instance(out) of System class.
         4.Reference new keyword using Addition constructor.
   4.Prints all invoking the interface single method.

Psuedocode:
      //create an interface
       interface StaticMethod {
	       public void printer();
	 
	 //create an interface with two String parameters
	  interface ParameterMethod {
	       public int find(String string1, String string2);
      
     //create an interface with a String parameter
      interface InstanceMethod {
	      public void print(String string);
	 
	//create an interface declaring the single printAdd() method type of className with two integer parameters
	 interface ConstructorReference {  

	     Addition printAdd(int number1,int number2);
	     
	 //function declaration
	  class Addition{   
        //Constructor of Addition class
	     Addition(int number1,int number2) {  
             System.out.print(number1 + number2);  
    }
    
      public class MethodReference {
	       public static void display(){  
               System.out.println("Static Method Reference");  
           public static void main(String[] args) {
    	       // Referring static method  
    	       StaticMethod printStatement = MethodReference::display;  
               // Calling interface method
    	       System.out.println("Static Method Reference");
    	       printStatement.printer();
               
               //Reference indexOf() method inside String class method
                 ParameterMethod finder = String::indexOf;
                 System.out.println("**Parameter Method Reference**");
                 System.out.println(finder.find("Parameter Method Reference","Reference"));
        
              //Reference println() method inside instance(out) of System class

               InstanceMethod myPrinter = System.out::println;
               System.out.println("**Instance Method Reference**");
               myPrinter.print("Instance Method Reference");
        
              //Reference new keyword using Addition constructor.

              ConstructorReference constructorReference = Addition::new;
              System.out.println("Constructor Reference");
              constructorReference.printAdd(5,9);
     
 */
program:

package com.java.training.core.lambda;

interface StaticMethod {
	public void printer(); 
}

interface ParameterMethod {
	public int find(String string1, String string2);
}

interface InstanceMethod {
	public void print(String string);
}

interface ConstructorReference {  

	Addition printAdd(int number1,int number2);  
    /*The printAdd() method of this interface matches the signature of one of the constructors 
in the Message class.*/
}

class Addition{   
    //Constructor of Addition class
	Addition(int number1,int number2) {  
        System.out.print(number1 + number2);  
    }  
}  
public class MethodReference {
	public static void display(){  
        System.out.println("Static Method Reference");  
    }  
    public static void main(String[] args) {
    	// Referring static method  
    	StaticMethod printStatement = MethodReference::display;  
        // Calling interface method
    	System.out.println("Static Method Reference");
    	printStatement.printer();
        
    	
        ParameterMethod finder = String::indexOf;
        System.out.println("**Parameter Method Reference**");
        System.out.println(finder.find("Parameter Method Reference","Reference"));
        
        
        InstanceMethod myPrinter = System.out::println;
        System.out.println("**Instance Method Reference**");
        myPrinter.print("Instance Method Reference");
        
        
        ConstructorReference constructorReference = Addition::new;
        System.out.println("Constructor Reference");
        constructorReference.printAdd(5,9);
	}

}