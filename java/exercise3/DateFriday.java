/*
Requirement:
    - To Given a random date, how would you find the date of the previous Friday
Entity:
    -The class named as DateFriday
Method Signature:
    - public static void main(String[] args)
Jobs to be Done:
    1.create a class called DateFriday
    2.Invoke the LocalDate class and get current time using now method.
    3.Using LocalDate class's object get the Previous Friday's date and Print it .
    
Pseudo Code:
public class DateFriday {
    
    public static void main(String[] args) {
    	LocalDate date = LocalDate.now();
    	System.out.printf("The previous Friday is: %s%n",
    	          date.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY)));
    }
}

*/
package com.java.training.core.dataandtime;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
public class DateFriday {
    
    public static void main(String[] args) {
    	LocalDate date = LocalDate.now();
    	System.out.printf("The previous Friday is: %s%n",
    	          date.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY)));
    }
}

