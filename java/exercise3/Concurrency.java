Requirements:
Explain in words Concurrency and Parallelism( in two sentences). 

explanation:
Concurrency is when two or more tasks can start, run, and complete in overlapping time periods. It doesn't necessarily mean they will ever both be running at the same instant. Parallelism is when tasks literally run at the same time.