Requirement:
 To create a list and to add 10 elements 
    1. create another list and perform addAll() method
    2. Use indexOf() and lastIndexOf() methods
    3. To print the values in list
       - for loops 
       - for each
       - iterator 
       - stream API
Entity:
    public class named  List is used.
    
Function Declaration:
   There is no function is declared.

Jobs to be done:
    1. Create a package com.java.training.core.list.
    2. Import util packages.
    3. Declare the class List.
    4. Create an empty list and add the elements to it and print it.
    5. Use addAll() function add the element of list a to list.
    6. use indexOf() and lastIndexOf() method to find the index of the the elements.
    7. Using for loop, Enhanced for loop iterator and stream to print the elements of the list.
Program:

package com.java.training.core.list;
import java.util.*;
public class List {

 public static void main(String[] args) {
		 ArrayList<String> list=new ArrayList<String>();   
	      list.add("India");   
	      list.add("Pakistan");    
	      list.add("England");    
	      list.add("Thailand");    
	      list.add("Germany");
	      list.add("Nepal");
	      list.add("Iran");
	      list.add("Iraq");
	      list.add("Russia");
	      list.add("Malaysia");
	      list.add("Spain");
	      System.out.println(""+ list);
	      ArrayList<String> a = new ArrayList<String>();
	      a.add("Japan");
	      a.add("China");
	      list.addAll(a);
	     System.out.println(""+list);
	     int i=list.indexOf("Russia");//using indexOf()
	     System.out.println(i);
	     int y=list.lastIndexOf("India");//using lastIndexOf()
	     System.out.println(y);
	     for (int z= 0; z < a.size(); z++) {
	            System.out.println("The elements are"+a);//using for loop
	        }
	     Iterator<String> iteratorList = list.iterator();//using iterator
	        while (iteratorList.hasNext()) {
	            System.out.println("the elements are" + iteratorList.next());
	        }
	        list.stream().forEach ((element) -> System.out.println(element));
	        	      
     }
 }
	

	         

    
	
	
		     
		        