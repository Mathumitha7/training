/*
Requirements:
    -  Write a program to filter the Person, who are male and age greater than 21
Entities:
    The class named as FilterPerson 
Function Declaration:
    - public static void main(String[] args)
    - createRoster()
Job to be done:
    - Create a class FilterPerson
    - Declare and define main method
    - Invoke the createRoster() method of Person class and store the returned value in a list-persons
    - Check whether each element of list is having 
        i) age greater than 21
        ii) gender is male
      Collect all the elements and add them in a list and store in a list-filteredPersons
    - Print the elements in filteredPersons
pseudocode:
public class FilterPerson {
    
	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		List<Person> filteredPersons = persons.stream()
				                              .filter(person -> person.getGender() == Person.Sex.MALE )
				                              .filter(person -> person.getAge() > 21)
				                              .collect(Collectors.toList());
		for(Person person : filteredPersons) {
			System.out.println(person.getName());
		}
	}
}
 
 */
package com.java.training.core.Streams;
import java.util.List;
import java.util.stream.*;

public class FilterPerson {
    
	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		List<Person> filteredPersons = persons.stream()
				                              .filter(person -> person.getGender() == Person.Sex.MALE )
				                              .filter(person -> person.getAge() > 21)
				                              .collect(Collectors.toList());
		for(Person person : filteredPersons) {
			System.out.println(person.getName());
		}
	}
}

