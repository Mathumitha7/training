/*
Requirement:
 Write a program for email-validation
Entity:
The class named as EmailValidation
Method Signature:
-public static void main(String[] args)
Jobs to be done:
1) create a package.
2)In mainmethod,Get the input from user using Scanner.
3) Create a pattern.
4) Create a matcher that tries to match input and pattern
5) If matches
    5.1) Display the input is a valid email otherwise invalid.
Pseudo Code:
public class EmailValidation {

    public static void main(String[] args) {

        // getting the input from user
        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your email address: ");
        String inputMail = scanner.nextLine();

        // creating a pattern to validate user input
        Pattern pattern = Pattern.compile("^[a-zA-Z\\d._]{6,}+@[a-z.]+\\.[a-z]{2,}$");

        // checking matcher matches the input
        Matcher matcher = pattern.matcher(inputMail);

        // if matches
        if (matcher.find()) {
            System.out.format("'%s'" + " is a valid email.", matcher.group());
        } else {
            System.out.println("Invalid email.");
        }
    }

}

*/
program:
package com.java.training.core.quantifier;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidation {

    public static void main(String[] args) {

        // getting the input from user
        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter your email address: ");
        String inputMail = scanner.nextLine();

        
        Pattern pattern = Pattern.compile("^[a-zA-Z\\d._]{6,}+@[a-z.]+\\.[a-z]{2,}$");

        
        Matcher matcher = pattern.matcher(inputMail);

       
        if (matcher.find()) {
            System.out.format("'%s'" + " is a valid email.", matcher.group());
        } else {
            System.out.println("Invalid email.");
        }
    }

}

