/*
Requirement:
    To Which class would you use to store your birthday in years, months, days, seconds, and nanoseconds

Entity:
    LocalDateTime,ZonedDateTime

Jobs To be Done:
    To Find Which class would you use to store your birthday in years,months,days,seconds, and nanoseconds
*/
Explanation:
    Most likely you would use the LocalDateTime class. To take a particular time zone into account,
    you would use the ZonedDateTime class. Both classes track date and time to nanosecond precision and both classes,
    when used in conjunction with Period, give a result using a combination of human-based units, 
    such as years, months, and days.
    
 