/*
Requirements:
1)Demonstrate the different types of groups in regex.
2)Demonstrate all the fields of the pattern class
*/
Explanation:
-        1.Capturing group
         2.Non-capturing group
         3.Backreference
         4.Relative Backreference
         5.Failed backreference
         6.Invalid backreference
         7.Nested backreference
         8.Forward reference

-The Pattern class defines an alternate compile method that accepts a set of flags affecting the way the pattern is matched. 
The flags parameter is a bit mask that may include any of the following public static fields: 
Pattern. 
    CANON_EQ Enables canonical equivalence.