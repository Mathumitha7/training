/*
Requirements:
    -  Consider the following code snippet:
            List<Person> newRoster = new ArrayList<>();
            newRoster.add(
                new Person(
                "John",
                IsoChronology.INSTANCE.date(1980, 6, 20),
                Person.Sex.MALE,
                "john@example.com"));
            newRoster.add(
                new Person(
                "Jade",
                IsoChronology.INSTANCE.date(1990, 7, 15),
                Person.Sex.FEMALE, "jade@example.com"));
            newRoster.add(
                new Person(
                "Donald",
                IsoChronology.INSTANCE.date(1991, 8, 13),
                Person.Sex.MALE, "donald@example.com"));
            newRoster.add(
                new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
        - Create the roster from the Person class and add each person in the newRoster to the existing list and print the new roster List.
        - Print the number of persons in roster List after the above addition.
        - Remove the all the person in the roster list
        
Entities:
    -The class named as NewRoster
     
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class called NewRoster
    - Declare and define main method
    - Invoke createRoster() method and store the returned value in a list-roster
    - Add elements to the list-newRoster
    - Add newRoster to the roster list
    - Print the elements from list-roster
    - Remove all elements from roster and print its size
 
*/
package com.java.training.core.Streams;
import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;

public class NewRoaster {

    public static void main(String[] args) {
    	List<Person> roster = Person.createRoster();
    	List<Person> newRoster = new ArrayList<>();
    	newRoster.add(new Person(
            "John",
            IsoChronology.INSTANCE.date(1980, 6, 20),
            Person.Sex.MALE,
            "john@example.com"));
        newRoster.add(new Person(
            "Jade",
            IsoChronology.INSTANCE.date(1990, 7, 15),
            Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add(new Person(
            "Donald",
            IsoChronology.INSTANCE.date(1991, 8, 13),
            Person.Sex.MALE, "donald@example.com"));
        newRoster.add(new Person(
            "Bob",
            IsoChronology.INSTANCE.date(2000, 9, 12),
            Person.Sex.MALE, "bob@example.com"));
        roster.addAll(newRoster);
        for(Person person : roster) {
        	System.out.println(person.getName());
        }
        roster.clear();
        System.out.println("roster is cleared - roster size : " + roster.size());
    }
}


