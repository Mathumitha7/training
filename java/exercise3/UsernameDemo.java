/*
Requirement:
Create a username for login website which contains
  -> 8-12 characters in length
  -> Must have at least one uppercase letter
  -> Must have at least one lower case letter
  -> Must have at least one digit
Entity:
The class named as UsernameDemo
Method Signature:
-public static void main(String[] args)
Jobs to be done:
1) Get the input from user using Scanner.
2) Create a pattern that should have
    2.1) pattern length lies between 8-12 characters
    2.2) pattern must have at least one upper case letter
    2.3) pattern must have at least one lower case letter
    2.4) pattern must have at least one digit
3) Create a matcher that tries to match input and pattern
4) If matches
    4.1) Display the input is a valid email otherwise invalid username.

Pseudo Code:
public class UsernameDemo {

    public static void main(String[] args) {

        System.out.print("Enter the username: ");
        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);
        String usernameInput = scanner.nextLine();

        // creating a pattern to validate user input
        Pattern pattern = Pattern
                .compile("^(?=.[a-z])(?=.[A-Z])(?=.*\\d).{8,12}$");

        // checking matcher matches the input
        Matcher matcher = pattern.matcher(usernameInput);
        if (matcher.find()) {
            System.out.println("Valid username");
        } else System.out.println("Invalid username");
    }

}
*/
program:
package com.java.training.core.quantifier;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UsernameDemo {

    public static void main(String[] args) {

        System.out.print("Enter the username: ");
        @SuppressWarnings("resource")
        Scanner scanner = new Scanner(System.in);
        String usernameInput = scanner.nextLine();

        
        Pattern pattern = Pattern
                .compile("^(?=.[a-z])(?=.[A-Z])(?=.*\\d).{8,12}$");

       
        Matcher matcher = pattern.matcher(usernameInput);
        if (matcher.find()) {
            System.out.println("Valid username");
        } else System.out.println("Invalid username");
    }

}
