1.What is class object?
Anwser:-
        Class Object is the entry point for all the reflection operations. 
    For every type of object, JVM instantiates an immutable instance of java.lang.Class 
that provides methods to examine the runtime properties of the object and create new objects, 
invoke its method and get/set object fields.

We can get a Class object via reflection using three methods
Anwser:- 
    *Through static variable class
    *getClass() method of object 
    *java.lang.Class.forName(String fullyClassifiedClassName)
    
2.How do you access the parent class of a class?
Anwser:-
      Access the parent class of a class using the getSuperclass() method to obtain a reference 
to a Class object that represents a superclass type of the object.