Requirements:
Create a queue using generic type and in both implementation Priority Queue,Linked list and complete following  
  -> add atleast 5 elements
  -> remove the front element
  -> search a element in stack using contains key word and print boolean value value
  -> print the size of stack
  -> print the elements using Stream
Entities:
The class named as UsingQueue

FunctionDeclaration:
public void linkedList()
public void priority()
public static void main(String[] args)

Jobs to be done:
1.create a package and Import a package
2.Create a class called UsingQueue,create a function called linkedList() and invoking two methods
3.In a linkedList() method creating queue and add elements in that.
4.use size() method, to find the size of the elements and use poll() method ,to remove the front element.
5.Use contains() to check the specific element in that queue and Printing the queue values using stream and for each value.
6.In a priority() method creating queue and add() method is used to adding the values.
7.Use size()method,to print size of element in queue and poll() used to remove the front element.
8.For finding specified value using contains() method and printing the PriorityQueue values using stream and for each
  
program:

package com.java.training.core.queue;
import java.util.Queue;
import java.util.stream.Stream;
import java.util.LinkedList;
import java.util.PriorityQueue;
public class UsingQueue {
   public void linkedList() {
	
		 Queue<Integer> q = new LinkedList<Integer>(); 
		 q.add(10);
		 q.add(20);
		 q.add(30);
		 q.add(40);
		 q.add(50);
		 System.out.println(""+q); 
		 int e=q.size();
		 System.out.println("size of queue "+e);
		 int o=q.poll();
		 System.out.println(q);
		 System.out.println(q.contains(10));
		 Stream m = q.stream();
	     m.forEach((element) -> {
		 System.out.println(element);  
			});
	}
  public void priority() {
	Queue<Integer> p = new PriorityQueue<Integer>();
	p.add(1);
	p.add(2);
	p.add(3);
	p.add(4);
	p.add(5);
	System.out.println(""+p);
	int h=p.size();
	System.out.println("size of queue "+h);
	int a=p.poll();
	System.out.println(a);
	System.out.println(p.contains("maths"));
	Stream z = p.stream();
    z.forEach((element) -> {
	 System.out.println(element);  
		});
	
}
  public static void main(String[] args) {
	UsingQueue queue = new UsingQueue();
	queue.linkedList();
	queue.priority();
}

}
