/*
Requirements:
Write some String content using outputstream
Entities:
The class named as OutputStream1
Functiondeclaration:
   public static void main(String[] args)
Jobs to be done:
1.create and import the package.
2.create class named as OutputStream1
3.In main method,throws IOException is used
  3.1 Set the path of the file and create odject as fos
  3.2create string named as s
  3.3 Get the string in byte format and write to the file
4.Use close() method,to close the file.
5.Print the output
pseudocode:
public class OutputStream1 {

	public static void main(String[] args) throws IOException {
		 OutputStream fos = new FileOutputStream("C:\\data\\ex.text");//set the path of file
			String s = " Welcome To Java Class";
			byte[] b = s.getBytes();//converting string into byte array    
			
	                fos.write(b);
			fos.close();
			System.out.println("Success");
	        }
	        
	 }
*/
program:

package com.java.training.core.IO;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class OutputStream1 {

	public static void main(String[] args) throws IOException {
		 OutputStream fos = new FileOutputStream("C:\\data\\ex.text");
			String s = " Welcome To Java Class";
			byte[] b = s.getBytes();//converting string into byte array    
			
	                fos.write(b);
			fos.close();
			System.out.println("Success");
	        }
	        
	 }

	 