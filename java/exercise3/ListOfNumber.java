/*
Requirement:
   To Write a program ListOfNumbers (using try and catch block).
 
Entity:
   The class named as  ListOfNumber
    Main
 
Method Signature:
    public void writeList()
    public static void main(String[] args)

Jobs to be done:
1.create a package.
2.Create a class called ListOfNumber
3.Declare an array with size 10 
4.Declare the method writeList()
    4.1 Assign arrayOfNumbers inside the try block
    4.2 Execute catch block for NumberFormatException and Print the exception
    4.3 Execute another catch block for IndexOutOfBoundsException and  Print the exception
    4.4 Inside the main class create an object for ListOfNumber and call the writeList() method.
5.Finally,display the output.

pseudocode:
public class ListOfNumber {
	
	    public int[] arrayOfNumbers = new int[10];
	    public void writeList() {
	        try {
	            arrayOfNumbers[10] = 11;
	      } catch (NumberFormatException e1) {
	            System.out.println("NumberFormatException => " + e1.getMessage());
	      } catch (IndexOutOfBoundsException e2) {
	            System.out.println("IndexOutOfBoundsException => " + e2.getMessage());
	    }
	  }
	}
   //create another class
	class Main {
	  public static void main(String[] args) {
	    ListOfNumber list = new ListOfNumber();
	    list.writeList();
	  }
	  
}


 */


program:
package com.java.training.core.ExceptionHandling;

public class ListOfNumber {
	
	    public int[] arrayOfNumbers = new int[10];
	    public void writeList() {
	        try {
	            arrayOfNumbers[10] = 11;
	      } catch (NumberFormatException e1) {
	            System.out.println("NumberFormatException => " + e1.getMessage());
	      } catch (IndexOutOfBoundsException e2) {
	            System.out.println("IndexOutOfBoundsException => " + e2.getMessage());
	    }
	  }
	}

	class Main {
	  public static void main(String[] args) {
	    ListOfNumber list = new ListOfNumber();
	    list.writeList();
	  }
	  
}
