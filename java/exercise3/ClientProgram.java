Requirements:
    - Build a client side program using ServerSocket class for Networking.
2.Entities:
    - The class named as ClientProgram
3.Methodsignature:
    - public static void main(String[] args)
4.Jobs to be done:
    1.create a class called ClientProgram 
    2.In mainmethod,Try and catch is used
        2.1)Create a Socket class with localhost.
        2.2)Pass argument of DataOutputStream class with getOutputStream method.
        2.3)Write to server using writeUTF method
        2.4)Close and flush the dataOutput.
    3.print the result

Pseudo Code:
public class ClientProgram {
	public static void main(String[] args) {
		try {
			Socket socket = new Socket("localhost", 6666);
			DataOutputStream dataOutput = new DataOutputStream(socket.getOutputStream());
			dataOutput.writeUTF("Client Server");
			dataOutput.flush();
			dataOutput.close();
			socket.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
*/
package com.java.training.core.networking;
import java.io.DataOutputStream;
import java.net.Socket;

public class ClientProgram{
	public static void main(String[] args) {
		try {
			Socket socket = new Socket("localhost", 6666);
			DataOutputStream dataOutput = new DataOutputStream(socket.getOutputStream());
			dataOutput.writeUTF("Client Server");
			dataOutput.flush();
			dataOutput.close();
			socket.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}

