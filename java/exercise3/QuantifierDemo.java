/*
Requirement:
Write a program for java regex quantifier
Entity:
The class named as RegexQuantifiers
Method Signature:
-public static void main(String[] args)
Jobs to be done:
1) In mainmethod,Get the input string.
2) Create a pattern 
3) Create a matcher that tries to match input and pattern
4) Find the word is present
    4.1) If present display the position of pattern

Pseudo Code:
public class QuantifierDemo{

	    public static void main(String[] args) {
	        String sentence = "Life is full of sorrows ";
	        // creating a pattern
	        Pattern pattern = Pattern.compile("is+");

	        // checking the pattern conditions matches the string
	        Matcher matcher = pattern.matcher(sentence);

	        // find the occurrance of the pattern
	        while (matcher.find()) {
	            System.out.println("'" + matcher.group() + "'" + " found at " +
	                    matcher.start());
	        }
	    }

	}

*/
program:
package com.java.training.core.quantifier;


	import java.util.regex.Matcher;
	import java.util.regex.Pattern;

	public class QuantifierDemo{

	    public static void main(String[] args) {
	        String sentence = "Life is full of sorrows ";
	       
	        Pattern pattern = Pattern.compile("is+");

	        
	        Matcher matcher = pattern.matcher(sentence);

	        while (matcher.find()) {
	            System.out.println("'" + matcher.group() + "'" + " found at " +
	                    matcher.start());
	        }
	    }

	}

