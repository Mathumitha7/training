/*
Requirement:
    - Remove the only person who are in the newRoster from the roster list.

Entity:
    -The class named as  RemovePerson
Function declaration
	- public static void main(String[] args)
Jobs to be done:
    1.Invoke Person class createRoster method and store it in roster List.
    2.Create new ArrayList and store new person details it in.
    3.Remove all newRoster person in roster using remove method.
    4.Remove one newRoster person in roster using filter and remove method.
    5.Print person roster contains or not.
pseudocode:
public class RemovePerson {
	@SuppressWarnings("unlikely-arg-type")
	public static void main(String[] args) {
	        List<Person> persons = Person.createRoster();
	        List<Person> newRoster = new ArrayList<>();
	        newRoster.add(
	            new Person(
	            "John",
	            IsoChronology.INSTANCE.date(1980, 6, 20),
	            Person.Sex.MALE,
	            "john@example.com"));
	        newRoster.add(
	            new Person(
	            "Jade",
	            IsoChronology.INSTANCE.date(1990, 7, 15),
	            Person.Sex.FEMALE, "jade@example.com"));
	        newRoster.add(
	            new Person(
	            "Donald",
	            IsoChronology.INSTANCE.date(1991, 8, 13),
	            Person.Sex.MALE, "donald@example.com"));
	        newRoster.add(
	            new Person(
	            "Bob",
	            IsoChronology.INSTANCE.date(2000, 9, 12),
	            Person.Sex.MALE, "bob@example.com"));
	        
	        persons.removeAll(newRoster);
			
			persons.stream()
				   .filter(person -> person.name == "Bob" ? persons.remove(person) : null);
			
			System.out.println("Roster contains 'Bob' is "+persons.contains("Bob"));
	}    
}
*/
package com.java.training.core.Streams;
import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;

public class RemovePerson {
	@SuppressWarnings("unlikely-arg-type")
	public static void main(String[] args) {
	        List<Person> persons = Person.createRoster();
	        List<Person> newRoster = new ArrayList<>();
	        newRoster.add(
	            new Person(
	            "John",
	            IsoChronology.INSTANCE.date(1980, 6, 20),
	            Person.Sex.MALE,
	            "john@example.com"));
	        newRoster.add(
	            new Person(
	            "Jade",
	            IsoChronology.INSTANCE.date(1990, 7, 15),
	            Person.Sex.FEMALE, "jade@example.com"));
	        newRoster.add(
	            new Person(
	            "Donald",
	            IsoChronology.INSTANCE.date(1991, 8, 13),
	            Person.Sex.MALE, "donald@example.com"));
	        newRoster.add(
	            new Person(
	            "Bob",
	            IsoChronology.INSTANCE.date(2000, 9, 12),
	            Person.Sex.MALE, "bob@example.com"));
	        
	        persons.removeAll(newRoster);
			
			persons.stream()
				   .filter(person -> person.name == "Bob" ? persons.remove(person) : null);
			
			System.out.println("Roster contains 'Bob' is "+persons.contains("Bob"));
	}    
}

