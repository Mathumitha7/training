Requirement:
To Create a set
   -add 10 values
   -perform addAll() and removeAll() to the set.
   -iterate the set using 
      -Iterator
      -for-each
   -contains(),isEmpty()

Entities:
The class named CreateSet is used.

FunctionDeclaration:
 There is no function is declared 

Jobs to be done:
1.Create a package.
2.Import util package.
3.Declare the class CreateSet. 
4.Create an empty set and add the elements to it.
5.Use addAll() function,add the element of set b to set.
6.Use removeAll() method, to remove all the elements in set.
7.perform iterator and foreach.
8.check isEmpty()and contains() function in the set
8.Display the output.

program:

package com.java.training.core.Set;
import java.util.*;

public class CreateSet {

	public static void main(String[] args) {
		
		        Set<String> s = new HashSet<String>(); 		        
		        s.add("blue"); 
		        s.add("yellow"); 
		        s.add("green"); 
		        s.add("violet"); 
		        s.add("white"); 
		        s.add("orange");
		        s.add("red");
		        s.add("pink");
		        s.add("black");
		        s.add("brown");		         
		        System.out.println("Set: " + s);
		        Set<String> b = new HashSet<String>(); 
		        b.add("golden");
		        b.add("silver");
		        s.addAll(b);
		        System.out.println(""+s);
		        s.removeAll(b);
		        Iterator<String> iterator = s.iterator();
                   while(iterator.hasNext()) {
		               String element = iterator.next();
		               System.out.println("the iterate elements are"+element);
		        }
                   for(String i : s) {
                	    System.out.println(i);
                	}
                   boolean isEmpty = (s.size() == 0);
                   System.out.println(isEmpty);
                   String z = "This is a String contains Example";
                   System.out.println("Contains sequence : " + z.contains("pen"));
		        
	}

}
