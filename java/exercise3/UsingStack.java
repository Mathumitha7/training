Requirements:
Create a stack using generic type and implement
  -> Push atleast 5 elements
  -> Pop the peek element
  -> search a element in stack and print index value
  -> print the size of stack
  -> print the elements using Stream


Entities:
The class named as UsingStack 

FunctionDeclaration:
public static void main(String[] args)

Jobs to be done:
1.create a package
2.Import a package.
3.create a class called UsingStack
4.creata stack and add the elements in that and print
5.use peek() method,to pop the first element in stack and print
6.use search() method,to search the element in that stack
7.use size() method,to find the size of element in stack
8.To print the elements using stream
9.print the result.



program:

package com.java.training.core.Stack;
import java.util.Stack;
import java.util.stream.Stream;
public class UsingStack {  
	public static void main(String[] args) {
	  Stack<String> z = new Stack<String>(); 
	   z.push("this"); 
        z.push("is"); 
        z.push("java"); 
        z.push("training"); 
        z.push("session"); 
        System.out.println(" " +z); 
        String topElement = z.peek();
        System.out.println("Peek element is: " +topElement); 
        int index = z.search("is");
        System.out.println("search the element: "+index);
        int size = z.size();
        System.out.println("size of  the stack "+z.size());
        Stream m = z.stream();
        m.forEach((element) -> {
		System.out.println(element);  
		});
        
        
	}

}
