/*
Requirement:
Explain the below program.
      try{
          dao.readPerson();
        } catch (SQLException sqlException) {
        throw new MyException("wrong", sqlException);
      }
*/	  
Explanation:
The method dao.readPerson() can throw an SQLException. If it does, the SQLException is caught and wrapped in a MyException. Notice how the SQLException (the sqlException variable) is passed to the MyException's constructor as the last parameter.
Exception wrapping is a standard feature in Java since JDK 1.4. Most (if not all) of Java's built-in exceptions has constructors that can take a "cause" parameter. They also have a getCause() method that will return the wrapped exception.