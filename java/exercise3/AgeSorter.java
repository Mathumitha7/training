/*
Requirements:
    -  sort the roster list based on the person's age in descending order using comparator
Entities:
    -The class named as AgeSorter
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create a class called AgeSorter
    - Declare and define main function
    - Invoke createRoster() method of class-Person and assign the returned value to a list-persons
    - Use stream method and its sorted method to sort them in descending order and store it in a list-orderedPersons
    - use forloop, iterate the values
    -Print the results 
pseudocode: 
public class AgeSorter {

    public static void main(String[] args) {
    	List<Person> persons = Person.createRoster();
		List<Person> orderedPersons = persons.stream()
				                             .sorted(Comparator.comparing(Person::getAge).reversed())
				                             .collect(Collectors.toList());
		for(Person person : orderedPersons) {
			System.out.println(person.getName() + " - " + person.getAge());
		}
    }
}

*/
program:
package com.java.training.core.Streams;
import java.util.Comparator;
import java.util.List;
import java.util.stream.*;


public class AgeSorter {

    public static void main(String[] args) {
    	List<Person> persons = Person.createRoster();
		List<Person> orderedPersons = persons.stream()
				                             .sorted(Comparator.comparing(Person::getAge).reversed())
				                             .collect(Collectors.toList());
		for(Person person : orderedPersons) {
			System.out.println(person.getName() + " - " + person.getAge());
		}
    }
}

