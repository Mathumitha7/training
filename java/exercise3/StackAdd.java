Requirement:
To write a code for add and remove the elements in stack.
Entities:
The class named as StackAdd.

FunctionDeclaration:

public static void main(String[] args)


Jobs to be done:
1.create a package
2.Import a package Stack.
3.Create a class called StackAdd.
4.create a new stack and add the elements in that stack.
5.Use remove() method,to remove the particular elements in stack.
6.print the result.

program:

package com.java.training.core.Collections;
import java.util.Stack;
public class StackAdd {

	public static void main(String[] args) {
        Stack<String> stack = new Stack<String>(); 
        stack.add("This"); 
        stack.add("is"); 
        stack.add("tutorial"); 
        stack.add("class"); 
        stack.add("for");
        stack.add("java");
        System.out.println("Stack: " + stack); 
        String k = stack.remove(2); 
       
        System.out.println(" After removed " + stack);
        
	}

}
