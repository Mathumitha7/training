Requirements:
What is the difference between remove() and poll() method of queue interface?

Entities:
There is no class in this program.

FunctionDeclaration:
There is no function is declared.

Explanation:

The remove() and poll() methods remove and return the head of the queue. 
The remove() and poll() methods differ only in their behavior when the queue is empty: the remove() method throws an exception, while the poll() method returns null.



