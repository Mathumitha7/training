/*
Requirement:
 mWhat will be the output of the following program?
 public class UseGenerics {
	
	public static void main(String args[]){  
		/*
        MyGen<Integer> m = new MyGen<Integer>();  
        m.set("merit");
        System.out.println(m.get());*/
    }
}

class MyGen<T>
{
    T var;
    void  set(T var)
    {
        this.var = var;
    }
    T get()
    {
        return var;
    }
}
Entities:
 -UseGenerics
 -MyGen
Functiondeclaration:
 void set(T var);
 T get();
 */
Answer:
 Thi s Program will not execute .since ,the Generic object created for MyGen class is of Integer type   and the set method of MyGen Class takes the Integer type data, but the input gave is of String type, so in the Compile Time ,the Compiler shows an error 
 

