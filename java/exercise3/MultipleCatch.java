Requirement:
To Demonstrate the catching multiple exception with example.

Entities:
The class named as MultipleCatch

FunctionDeclaration:
public static void main(String[] args)

Jobs To be done:
1.Create a package as java.training.core.ExceptionHandling.
2.Import a package util.
3.create class called MultipleCatch
4.create a array and add values in that array.
5.Use multiple catch block
6.print the output

program:
package com.java.training.core.ExceptionHandling;
import java.util.*;
public class MultipleCatch {

	public static void main(String[] args) {
		int[] arr=new int[] {3,4,5,1,2};
		for(int i=0;i<5;i++) {
			try {
				System.out.println(2/arr[i]);
			}
			catch(ArrayIndexOutOfBoundsException e){
				System.out.println("Array index out of range");
			}
			catch(Exception e) {
				System.out.println("Exception is handled");
			}
		}

	}

}
				

	
