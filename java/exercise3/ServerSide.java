/*
Requirements:
    - Build a client side program using ServerSocket class for Networking.
Entities:
    -The class named as  ServerSide
3.Methodsignature:
    - public static void main(String[] args)
4.Jobs to be done:
    1.In mainmethod,Try and catch block is used
    2.Create a ServerSocket class with localhost.
        2.1)Accept serverSocket using accept mathod.
        2.2)Pass argument of DataOutputStream class with getInputStream method.
        2.3)Read to client passed string using readUTF method
        2.4)Print and close the dataOutput.
Pseudo Code:
public class MyServer {
	public static void main(String[] args) {
		try {
			ServerSocket serverSocket = new ServerSocket(6666);
			Socket socket = serverSocket.accept();// establishes connection
			DataInputStream dataInput = new DataInputStream(socket.getInputStream());
			String string = (String) dataInput.readUTF();
			System.out.println(string);
			serverSocket.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
*/
package com.java.training.core.networking;
import java.io.DataInputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerSide{
	public static void main(String[] args) {
		try {
			ServerSocket serverSocket = new ServerSocket(6666);
			Socket socket = serverSocket.accept();// establishes connection
			DataInputStream dataInput = new DataInputStream(socket.getInputStream());
			String string = (String) dataInput.readUTF();
			System.out.println(string);
			serverSocket.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}

