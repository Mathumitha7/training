\*Requirement:
To write a program to display the following using escape sequence in java
      A.My favorite book is "Twilight" by Stephanie Meyer
      B.She walks in beauty, like the night, 
        Of cloudless climes and starry skies 
        And all that's best of dark and bright 
        Meet in her aspect and her eyes…
      C."Escaping characters", © 2019 Java
 
Entities:
EscapeSequence class is used.

Function Declaration:
public static void main(String[] args)

Jobs to be done:
1. Create a class EscapeSequence
2. Create a string book and print the string using escape sequences
3. Create a string k and print the k  string using escape sequences
4. Print the output. 

Pseudocode:
public class  EscapeSequence{

	public static void main(String[] args) {
	//create a String 
	 String book = new String ("My favorite book is \"Twilight\" by Stephanie Meyer");
	 System.out.println(myFavoriteBook);
	//create another String
     String k = new String ("She walks in beauty, like the night, \nOf cloudless climes and starry skies\nAnd all that's best of dark and bright\nMeet in her aspect and her eyes...");
     System.out.println(k);
     System.out.println("\"Escaping characters\", \u00A9 2019 Java");
 */
program:
package com.java.training.core.logicaloperators;
import java.util.*;
public class EscapeSequence {

	public static void main(String[] args) {
		String book = new String ("My favorite book is \"Twilight\" by Stephanie Meyer");
	    System.out.println(book);
	    String k = new String ("She walks in beauty, like the night, \nOf cloudless climes and starry skies\nAnd all that's best of dark and bright\nMeet in her aspect and her eyes...");
	    System.out.println(k);
	    
	   System.out.println("\"Escaping characters\", \u00A9 2019 Java");

	}

}
