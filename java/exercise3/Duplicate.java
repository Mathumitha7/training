/*
Requirements:
    -  Consider a following code snippet:
            List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
       - Get the non-duplicate values from the above list using java.util.Stream API
       
Entities:
    The class named as Duplicate
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class called Duplicate
    - Declare and define main function
    - Declare a list-Numbers and assign values to it
    - Use Stream API and invoke distinct() method to extract unique values and add them to a list.
    - Store that new list to a list-uniqueValues
    - Print the elements in list-uniqueValues
pseudocode:
public class Duplicate {
	

	    public static void main(String[] args) {
	    	List<Integer> Numbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
	    	List<Integer> uniqueNumbers = Numbers.stream().distinct().collect(Collectors.toList());
	    	System.out.println("Unique values are : ");
	    	uniqueNumbers.stream().forEach((value) -> System.out.println(value));
 */
package com.java.training.core.Streams;
import java.util.List;
import java.util.stream.*;
import java.util.Arrays;

public class Duplicate {
	

	    public static void main(String[] args) {
	    	List<Integer> Numbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
	    	List<Integer> uniqueNumbers = Numbers.stream().distinct().collect(Collectors.toList());
	    	System.out.println("Unique values are : ");
	    	uniqueNumbers.stream().forEach((value) -> System.out.println(value));
	    }
	}


