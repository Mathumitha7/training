/*
Requirements:
 Write a program for java string regex Method
Entities:
The class named as StringRegex
FunctionDeclaration:
public static void main(String[] args)
Jobs to be done:
1.create a package.
2.create class called StringRegex
3.In main method,create a string and assign the value for it 
 3.1 Use matches(),check  the givenstring it matches,and print result in boolen type
 3.2 use splitmethod,to split that  words in string
 3.3use replaceAll()method, to replace all the words in string
 3.4 use replaceFirst() method to give the replacement word, then this method replaced that word 
4.Print the result
Pseudocode:
public class StringRegex {
	public static void main(String[] args) {
			String text = "one two three two one ";

			boolean matches = text.matches(".*five.*");
			System.out.println(""+matches);
			String[] twos = text.split("two");
			System.out.println(""+twos);
			String t = text.replaceAll("two", "five");
			System.out.println(""+t);
			String s = text.replaceFirst("two", "2");
			System.out.println(""+s);

*/
program:
package com.java.training.core.regex;

public class StringRegex {
	public static void main(String[] args) {
			String text = "one two three two one ";

			boolean matches = text.matches(".*five.*");
			System.out.println(""+matches);
			String[] twos = text.split("two");
			System.out.println(""+twos);
			String t = text.replaceAll("two", "five");
			System.out.println(""+t);
			String s = text.replaceFirst("two", "2");
			System.out.println(""+s);
			
			}

	}

