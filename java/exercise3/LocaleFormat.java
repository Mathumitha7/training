/*
Requirement:
     - Program to rounding the value to maximum of 3 digits by setting maximum and minimum Fraction digits.
     
Entities:
     - The class named as LocaleFormat
     
3.Function Declaration:
  	 - public static void main(String[] args)
  	 
4.Jobs To Be Done:
  	1.create a class called  LocaleFormat  
        2.Create Date class to get current and print.
  	3.Create Locale class to get a time in specified laguage and country.
  	4.Using DateFormat class getTimeInstance method pass parameters 
  	      4.1)DEFAULT with locale object.
  	      4.2)MEDIUM with locale object.
  	      4.3)LONG with locale object.
  	      4.4)SHORT with locale object.
  	      4.5)FULL with locale object.
        5.Print the all Date format. 	      
    
PseudoCode:
public class LocaleFormat {
   public static void main(String args[]){
	   
	   //Current Date
       Date currentDate = new Date();  
       System.out.println("Current date is: "+currentDate); 
       
       //Default time
       Locale locale = new Locale("English", "IN");
       String timeDefault = DateFormat.getTimeInstance(DateFormat.DEFAULT, locale).format(currentDate);  
       System.out.println("Formatting the Time using DateFormat.DEFAULT: "+timeDefault);   
 
       //Medium date
       String dateMedium = DateFormat.getDateInstance(DateFormat.MEDIUM, locale).format(currentDate);  
       System.out.println("Formatting the Date using DateFormat.MEDIUM: "+dateMedium);  
         
       //Long date
       String dateLong = DateFormat.getDateInstance(DateFormat.LONG, locale).format(currentDate);  
       System.out.println("Formatting the Date using DateFormat.LONG: "+dateLong); 
       
       //Short date
       String dateShort = DateFormat.getDateInstance(DateFormat.SHORT, locale).format(currentDate);  
       System.out.println("Formatting the Date using DateFormat.SHORT: "+dateShort); 
 
       //Full time
       String timeFull = DateFormat.getTimeInstance(DateFormat.FULL, locale).format(currentDate);  
       System.out.println("Formatting the Time using DateFormat.FULL: "+timeFull);  
         
       
   }
}
*/
package com.java.training.core.internalization;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale; 
public class LocaleFormat {
   public static void main(String args[]){
	   
	  
       Date currentDate = new Date();  
       System.out.println("Current date is: "+currentDate); 
       
       
       Locale locale = new Locale("English", "IN");
       String timeDefault = DateFormat.getTimeInstance(DateFormat.DEFAULT, locale).format(currentDate);  
       System.out.println("Formatting the Time using DateFormat.DEFAULT: "+timeDefault);   
 
       
       String dateMedium = DateFormat.getDateInstance(DateFormat.MEDIUM, locale).format(currentDate);  
       System.out.println("Formatting the Date using DateFormat.MEDIUM: "+dateMedium);  
         
       
       String dateLong = DateFormat.getDateInstance(DateFormat.LONG, locale).format(currentDate);  
       System.out.println("Formatting the Date using DateFormat.LONG: "+dateLong); 
       
      
       String dateShort = DateFormat.getDateInstance(DateFormat.SHORT, locale).format(currentDate);  
       System.out.println("Formatting the Date using DateFormat.SHORT: "+dateShort); 
 
       
       String timeFull = DateFormat.getTimeInstance(DateFormat.FULL, locale).format(currentDate);  
       System.out.println("Formatting the Time using DateFormat.FULL: "+timeFull);  
         
       
   }
}
