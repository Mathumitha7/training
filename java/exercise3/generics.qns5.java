/*Requirement:
What will be the output of the given program?
public class UseGenerics {
	
	public static void main(String args[]){  
		/*
        MyGen<Integer> m = new MyGen<Integer>();  
        m.set("merit");
        System.out.println(m.get());*/
    }
}

class MyGen<T>
{
    T var;
    void  set(T var)
    {
        this.var = var;
    }
    T get()
    {
        return var;
    }
}
Entities:
The class named as MyGen,UseGenerics
*/
Explanation:
No,This Program will not execute and shows Compilationerror
since the Generic object created for MyGen class is of Integer type   and the set method of MyGen Class takes the Integer type data, but the input gave is of String type, so the Compiler shows   error in the Compile Time.
