/*
Requirements:
Write a string content using Writer
Entities:
The class named as WriterDemo
Functiondeclaration:
   public static void main(String[] args)
Jobs to be done:
1.create and import the package.
2.create class named as WriterDemo
3.In main method,try and catch block is used
  3.1 Set the path of the file and create a string named as content
  3.2 write the content to the file. 
  3.3Use close() method,to close the file.
4.print the output.
Pseudocode:
public class WriterDemo {
   public static void main(String[] args) {
//create try and catch block
		 try {  
	            Writer w = new FileWriter("C://data//byteoutput2.txt"); //set the path  
	            String content = "I love my country";  //create a string
	            w.write(content);  
	            w.close();  
	            System.out.println("Done");  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        }  
	}

}


*/
program:

package com.java.training.core.IO;
import java.io.Writer;
import java.io.FileWriter;
public class WriterDemo {
   public static void main(String[] args) {
		 try {  
	            Writer w = new FileWriter("C://data//byteoutput2.txt");  
	            String content = "I love my country";  
	            w.write(content);  
	            w.close();  
	            System.out.println("Done");  
	        } catch (Exception e) {  
	            e.printStackTrace();  
	        }  
	}

}
