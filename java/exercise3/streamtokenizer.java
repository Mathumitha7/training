/*
Requirement:
1)What is stream tokenizer?
2)Why do we use stream tokenizer?  
3)Mention any five methods of stream tokenizer?
Explanation:
  -- The Java. io. StreamTokenizer class takes an input stream and parses it into "tokens", allowing 
the tokens to be read one at a time. 
   The stream tokenizer can recognize identifiers, numbers, quoted strings, and various comment 
styles
   ---Java.io.StreamTokenizer class parses input stream into “tokens”.It allows to read one token 
at a time. 
   Stream Tokenizer can recognize numbers, quoted strings, and various comment styles.


  --* void commentChar(int ch)
        => Specified that the character argument starts a single-line comment.
   * void eolIsSignificant(boolean flag)
        => This method determines whether or not ends of line are treated as tokens.
   * int lineno()
        => This method returns the current line number.
   * void lowerCaseMode(boolean fl)
        => This method determines whether or not word token are automatically lowercased.
   * int nextToken()
        => This method parses the next token from the input stream of this tokenizer.
                        
 */