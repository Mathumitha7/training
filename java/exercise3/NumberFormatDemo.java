/*
Requirement:
     - Program to change the number format to Denmark number format.
Entity:
     -The class named as  NumberFormat
     
Function Declaration:
  	 - public static void main(String[] args)
  	 
Jobs To Be Done:
  	1.Invoke the NumberFormat class getInstance() method as parameter , create Locale class with argument Denmark number format.
        2.Print the Denmark number format.
pseudocode:
 public class NumberFormatDemo{
	public static void main(String[] args) {
		System.out.println("Denmark Number Foramt:-");
		System.out.println(NumberFormat.getInstance(new Locale("Denmark"))
				                       .format(12349));

	}
}

*/   
package com.java.training.core.internalization;
import java.text.NumberFormat;
import java.util.Locale;

public class NumberFormatDemo{
	public static void main(String[] args) {
		System.out.println("Denmark Number Foramt:-");
		System.out.println(NumberFormat.getInstance(new Locale("Denmark"))
				                       .format(12349));

	}
}

