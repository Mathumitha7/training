Requirements:
To count the size of mappings in a map
Entities:
class named as Size

Functiondeclaration:
There is no function is declared.

Jobs to be done:
1.Create a package
2.Import a package HashMap and Map.
3.create class called Size
4.create emptymap and add the values in that map .
5.Find the size of map and display the output.

program:

package com.java.training.core.Map;

import java.util.HashMap;
import java.util.Map;

public class Size {

	public static void main(String[] args) {
		Map<String,String> m= new HashMap<String,String>();
		 m.put("1","fast");
		 m.put("2","slow");
		 m.put("3","medium");
		 System.out.println(""+m);
		 System.out.println("size of elements "+m.size());

	}

}
