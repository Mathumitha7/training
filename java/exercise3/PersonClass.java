/*
Requirements:
     Consider the following Person:
            new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
       - Check if the above person is in the roster list obtained from Person class.
Entities:
    - The class named as PersonClass 
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class called PersonClass
    - Declare and define main method
    - Invoke createRoster() method and store the returned value in a list of type Person
    - Use stream and anyMatch() method ,used to find whether the element is present
    -Check the condition,print the result
 
pseudocode:
public class PersonClass {

    public static void main(String[] args) {
    	List<Person> persons = Person.createRoster();
    	Person checker = new Person("Bob", 
    			                    IsoChronology.INSTANCE.date(2000, 9, 12),
                                    Person.Sex.MALE, 
                                    "bob@example.com");
    	boolean checkPerson = persons.stream()
    			                     .anyMatch((person) -> 
    	                             person.getName().equals(checker.getName()) & 
    	                             person.getAge() == checker.getAge() & 
    	                             person.getGender().equals(checker.getGender()) &
    	                             person.getEmailAddress().equals(checker.getEmailAddress())
    	    );
    	if(checkPerson) {
    		System.out.println(checker.getName() + " is present.");
    	} else {
    		System.out.println(checker.getName() + " is not present.");
    	}
    }
}
*/
package com.java.training.core.Streams;
import java.time.chrono.IsoChronology;
import java.util.List;
import java.util.stream.*;

public class PersonClass {

    public static void main(String[] args) {
    	List<Person> persons = Person.createRoster();
    	Person checker = new Person("Bob", 
    			                    IsoChronology.INSTANCE.date(2000, 9, 12),
                                    Person.Sex.MALE, 
                                    "bob@example.com");
    	boolean checkPerson = persons.stream()
    			                     .anyMatch((person) -> 
    	                             person.getName().equals(checker.getName()) & 
    	                             person.getAge() == checker.getAge() & 
    	                             person.getGender().equals(checker.getGender()) &
    	                             person.getEmailAddress().equals(checker.getEmailAddress())
    	    );
    	if(checkPerson) {
    		System.out.println(checker.getName() + " is present.");
    	} else {
    		System.out.println(checker.getName() + " is not present.");
    	}
    }
}

