/*Requirements:
Compare the Checked and UnCheckedException
*/

Explanation:
checked: 
       checkedckedException are occur at compiletime.
       The compiler checks a checkedexception.
	   These types of exception can be handled at the time of compilation.
	   JVM  needs the exception to catch and handle.
	     eg;SQLException, IOException, ClassNotFoundException.
Unchecked:
       Uncheckedexceptions occur at runtime.
	   The compiler does not check these types of exceptions.
	   These types of exception cannot be catch or  handle at the time of compilation.
	   JVM  doesnot require the exception to catch and handle.
          eg;ArrayIndexOutOfBoundsException,ArithmeticException, nullpointerexception.
			