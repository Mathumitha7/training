Requirement:
To code for sorting vector list in descending order.

Entities:
The class named as Sorting.

FunctionDeclaration:

public static void main(String[] args)


Jobs to be done:
1.create a package
2.Import a package Vector,Collections.
3.Create a class called Sorting
4.Create a new vector and add the elements in that
5.use Collections.reverseOrder() method,to print the elements in reverse order.
6.print the output.

program:

package com.java.training.core.Collections;
import java.util.Vector;
import java.util.Collections;
public class Sorting {

	public static void main(String[] args) {
        Vector < String > vec = new Vector < String > ();  
        vec.add("White");  
        vec.add("Green");  
        vec.add("Black");  
        vec.add("Orange");  
        System.out.println("The vector elements are: "+vec); 
        Collections.sort(vec,Collections.reverseOrder());
        for(String num:vec) {
        	System.out.println(num+=" ");
        }
	}

}
