/*
Requirements:
  create a program for url Connection class in networking
Entities:
  The class named as UrlConnection
Methodsignature:
  public static void main(String[] args)
Jobstobedone:
1.Create a class called UrlConnection
2.In mainmethod,try catch block is used.
3.create a url path,and set the  path name.
 3.1 openConnection() method,used to open the connection
 3.2 getInputStream(),used to get the inputstream  function
 3.3 declare a variable i
 3.4 Using while loop,to iterate the loop until statisfies the condition
4.print the results
pseudocode:
 public class UrlConnection {
	
	
	public static void main(String[] args){  
	try{  
	URL url=new URL("https://bitbucket.org" );  
	URLConnection urlcon=url.openConnection();  
	InputStream stream=urlcon.getInputStream();  
	int i;  
	while((i=stream.read())!=-1){  
	System.out.print((char)i);  
	}  
	}catch(Exception e){System.out.println(e);}  
	}  
	}  
*/
program:
package com.java.training.core.networking;
import java.io.*;  
import java.net.*;  
public class UrlConnection {
	
	
	public static void main(String[] args){  
	try{  
	URL url=new URL("https://bitbucket.org" );  
	URLConnection urlcon=url.openConnection();  
	InputStream stream=urlcon.getInputStream();  
	int i;  
	while((i=stream.read())!=-1){  
	System.out.print((char)i);  
	}  
	}catch(Exception e){System.out.println(e);}  
	}  
	}  
