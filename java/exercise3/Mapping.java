Requirement:
    To write a Java program to test if a map contains a mapping for the specified key.

Entity:
    class  named as Mapping.
    
Function Declaration:
    public static void main(String[] args)

Jobs to be Done:
    1. Create a package com.java.training.core.Map.
    2. import the util package.
    3. Declare the class Mapping.
    4. Create an object for a Map  and Store the values for m.
    5. Now create a variable called b of type boolean and check the keys is present in the m
    6. Print the result stored in the value.

program:
package com.java.training.core.Map;
import java.util.*;
public class Mapping {

	public static void main(String[] args) {
		
		        Map<Integer, String> m = new HashMap<>();
		        m.put(10, "rose");
		        m.put(25, "lotus");
		        m.put(20, "jasmine");
		        m.put(76, "tulip");
		        m.put(12, "lily");
		        boolean b =m.containsKey(12);
		        System.out.println(" "+b);
		        
		        boolean n = m.containsKey(20);
		        System.out.println(" "+n);
		    }

		}


