Requirements:
To use addFirst(),addLast(),removeFirst(),removeLast(),peekFirst(),peekLast(),pollFirst(),pollLast()methods to store and retrieve elements in Arraydequeue.

Entities:
the class named as DequeDemo.

FunctionDeclaration:
public static void main(String[] args) 

Jobs to be done:
1.Create a package.
2.import the package Deque,ArrayDeque.
3.create a class called DequeueDemo.
4.create new deque, add elements in that  
5.use addFirst(),addLast() method to display the first and last element in deque 
6.use peekFirst(),peekLast() method to display the beginning and last element in deque.
7.use pollFirst(),pollLast() method to remove first and last element in deque.
8.print the output

program:

package com.java.training.core.Collections;
import java.util.*;
public class DequeueDemo {
   public static void main(String[] args) {
		Deque<String> deque = new ArrayDeque<>(); 
		deque.add("Blue");
		deque.addFirst("green");
		deque.add("yellow");
		deque.add("silver");
		deque.add("red");
		deque.add("black");
		deque.add("orange");
		deque.add("apple");
		deque.addLast("Mango");
		System.out.println(""+deque);
		String r = deque.removeFirst();
	    System.out.println(""+deque);
	    String t = deque.removeLast();
		System.out.println(""+deque);
		String poll = deque.pollFirst();
		System.out.println(""+deque);
		String y = deque.pollLast();
		System.out.println(""+deque);
		String firstElement = deque.peekFirst();
		System.out.println(""+ firstElement);
		String m = deque.peekLast();
		System.out.println(""+m);
	}

}
