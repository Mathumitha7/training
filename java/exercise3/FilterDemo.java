/*
Requirements:
    Write a program to filter the Person, who are male and
        - find the first person from the filtered persons
        - find the last person from the filtered persons
        - find random person from the filtered persons 
Entities:
    - The class named as FilterDemo
Function Declaration:
    - public static void main(String[] args)
Job to be done:
    - Create a class called  FilterDemo
    - Declare and define main function
    - Invoke createRoster() function and store the returned value in a List of type Person
    - Check each element using filter method whether gender is MALE or not. If MALE, add them in a List-filteredPersons
    - print the first person
    - print the last person
    - print a random person
pseudocode:
public class FilterDemo {

	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		List<Person> selectedPersons = persons.stream()
				                              .filter(person -> person.gender == Person.Sex.MALE )
				                              .collect(Collectors.toList());
		Random r = new Random();
		for(Person person : selectedPersons) {
			System.out.print(person.getName() + ", ");
		}
		System.out.println("\nFirst Element: " + selectedPersons.stream().findFirst().get().getName());
		System.out.println("Random Element: " + selectedPersons.stream().findAny().get().getName());
		System.out.println("Last Element: " + selectedPersons.stream().reduce((first, second) -> second).get().getName());
	}
}


*/
package com.java.training.core.Streams;
import java.util.List;
import java.util.Random;
import java.util.stream.*;

public class FilterDemo {

	public static void main(String[] args) {
		List<Person> persons = Person.createRoster();
		List<Person> selectedPersons = persons.stream()
				                              .filter(person -> person.gender == Person.Sex.MALE )
				                              .collect(Collectors.toList());
		Random r = new Random();
		for(Person person : selectedPersons) {
			System.out.print(person.getName() + ", ");
		}
		System.out.println("\nFirst Element: " + selectedPersons.stream().findFirst().get().getName());
		System.out.println("Random Element: " + selectedPersons.stream().findAny().get().getName());
		System.out.println("Last Element: " + selectedPersons.stream().reduce((first, second) -> second).get().getName());
	}
}

