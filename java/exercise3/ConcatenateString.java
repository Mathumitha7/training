/*Requirements:
To write a Lambda expression Program with a single method interface to concatenate two strings

Entities: 
The class named as ConcatenateString

Functiondeclaration:
interface singleMethodInterface
public static void main(String[] args

Jobs to be done:
1.Create and import package
2.create a class called ConcatenateString with interface as SingleMethodInterface
3.Inside the declaring the single method with two string parameters
4.In the class main creating interface object and assigning the lambda expression to return two strings concatenate 
5.Print statement invoking the interface single method with string values and finally return the value which is assigned

Psuedocode:
   interface singleMethodInterface {
	String string(String string1, String string2);

    public class StringConcatenateDemo {

	   public static void main(String[] args) {
	   //create SingleMethodInterface
	    SingleMethodInterface singleInterface = (string1,string2) -> string1.concat(string2);
	    
	    //display the result
	     System.out.println(singleInterface.string("Hello! ", "MathuMitha "));
 */

program:

package com.java.training.core.lambda;
import java.util.*;
interface singleMethodInterface {
	String string(String string1, String string2);
}

public class ConcatenateString {

	public static void main(String[] args) {
		       SingleMethodInterface singleInterface = (string1,string2) -> string1.concat(string2);
						
			   System.out.println(singleInterface.string("Hello! ", "MathuMitha "));

			}

		}

