Requirement:
create an array list with 7 elements, and create an empty linked list add all elements of the array list to linked list ,traverse the elements and display the result

Entities:
The class named as Traverse is used.

FunctionDeclaration:
public static void main(String[] args)

Jobs to be done:
1.create a package called java.training.core.Collections
2.import a package
3.create a class called Traverse
4.create a empty list ,add the elements in that.
5.create a LinkedList,addAll() method is used.
6.add all elements of a to list.
7.use hasNext() method to traverse the elements of a and print result.


program:

package com.java.training.core.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Iterator;
public class Traverse {

	public static void main(String[] args) {
		List<Integer> a = new ArrayList<>();
		a.add(1);
		a.add(2);
		a.add(3);
		a.add(4);
		a.add(5);
		a.add(6);
		a.add(7);
		System.out.println(" "+a);
		LinkedList<Integer> list = new LinkedList<>();
		list.addAll(a);
		System.out.println(" "+list);
		Iterator i =a .iterator();
		while(i.hasNext()) {
			System.out.println(i.next());
	}

}
}