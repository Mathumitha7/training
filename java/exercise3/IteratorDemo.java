/*
Requirements:
    -  Iterate the roster list in Persons class and and print the person without using forLoop/Stream  
Entities:
    - The class named as IteratorDemo
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create classcalled IteratorDemo
    - Declare and define main method
    - create a list
    - Invoke the createRoster() method and store the returned value in a list
    - Use Iterator to print the element
pseudocode:
public class IteratorDemo{

    public static void main(String[] args) {
         //createRoster() method
    	List<Person> persons = Person.createRoster();
    	Iterator<Person> iterator = persons.iterator();
    	while(iterator.hasNext()) {
    		System.out.println(iterator.next().getName());
    	}
    }
}
 
 */
program:
package com.java.training.core.Streams;
import java.util.Iterator;
import java.util.List;

public class IteratorDemo{

    public static void main(String[] args) {
    	List<Person> persons = Person.createRoster();
    	Iterator<Person> iterator = persons.iterator();
    	while(iterator.hasNext()) {
    		System.out.println(iterator.next().getName());
    	}
    }
}
