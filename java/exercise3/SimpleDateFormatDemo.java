/*
Requirement:
     - Program to print the following pattern of the Date and Time using SimpleDateFormat.
           "yyyy-MM-dd HH:mm:ssZ"
     
Entity:
     - The class named as SimpleDateFormatDemo 
     
Function Declaration:
  	 - public static void main(String[] args)
  	 
JobsToBeDone:
  	1.create a mainmethod
        2.Store date pattern in pattern String.
  	3.Create SimpleDateFormat class pass argument pattern.
  	4.Get pattern format using invoke format() method.
  	5.Print the format date.      
PseudoCode:
public class SimpleDateFormatDemo {
	public static void main(String[] args) {
		String pattern = "yyyy-MM-dd HH:mm:ssZ";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		String date = simpleDateFormat.format(new Date(0));
		System.out.println(date);
	}
}

*/
package com.java.training.core.internalization;
import java.sql.Date;
import java.text.SimpleDateFormat;

public class SimpleDateFormatDemo {
	public static void main(String[] args) {
		String pattern = "yyyy-MM-dd HH:mm:ssZ";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		String date = simpleDateFormat.format(new Date(0));
		System.out.println(date);
	}
}

