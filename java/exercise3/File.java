/*
Requirements:
To read the file using inputstream

Entities:
The class named as File

FunctionDeclaration:
public static void main(String[] args)

Jobs to be done:
1.cretae and import a package.
2.create a class called File 
3.In main method,try and catch block is used.
 3.1 In First,Set the path to read the file and initialize i=0 which type is int
 3.2 In while condition,reading the file until it gets false and print the result
4.After read the file,Use close()method is used to close the file.

Pseudocode:
public class File {

	public static void main(String[] args) {
                //create try and catch block
		 try {
		        FileInputStream fis = new FileInputStream("C:\\data\\char");//Fix the path to read the file 
		        int i = 0;
				while((i=fis.read())!= -1){

				System.out.println((char)i);}
		        fis.close();
		        }catch(Exception e){
		              System.out.println(e);
		        }
		  }
}
*/
program:

package com.java.training.core.IO;

import java.io.FileInputStream;

public class File {
	

	public static void main(String[] args) {
		 try {
		        FileInputStream fis = new FileInputStream("C:\\data\\char");
		        int i = 0;
				while((i=fis.read())!= -1){

				System.out.println((char)i);}
		        fis.close();
		        }catch(Exception e){
		              System.out.println(e);
		        }
		  }
}