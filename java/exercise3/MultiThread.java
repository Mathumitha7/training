/*
Requirement:
   To Write a program which implement multithreading using sleep(), setPriority(), getPriorty(), Name(), getId() methods.

Entity:
   The class named as MultiThread
Function declaration:
   - public void run()
   - public static void main(String[] args)
Jobs to be done:
   1.Create a class as MultiThread 
   2.Declare main method and create run() method to run thread.
   3.Using for loop run getName() and getId() method.
   4.Print the method slow down using sleep() method.
   5.In the main method create Thread and and invoke start method
   6.Set setPriority() method and currentThread() and getPriority() method
   7.print the result.
pseudocode:
public class MultiThread extends Thread  {
    Thread t;
      public void run() {
	  for (int i = 5; i < 13; i++) {
		                                           //Using getName() method
	    System.out.println(Thread.currentThread().getName() + "  " + i);
             try {
		        	 // Displaying the thread that is running 
		  System.out.println ("Thread " + Thread.currentThread().getId() + " is running");  //getId() method
		            // thread to sleep for 1000 milliseconds
		    Thread.sleep(500);
		     } catch (Exception e) {
		         System.out.println(e);
		         }
		 }
	   }

	         public static void main(String[] args) throws Exception {
		     Thread thread = new Thread(new MultiThread());
		      // this will call run() function
		      thread.start();

		    Thread.currentThread().setPriority(6); //setPriority() method 

		     System.out.println("main thread priority : " + Thread.currentThread().getPriority()); 
		   }
	} 
*/

program:
package com.java.training.core.multithreading;
  public class MultiThread extends Thread  {
    Thread t;
      public void run() {
	  for (int i = 5; i < 13; i++) {
		                                                  
	    System.out.println(Thread.currentThread().getName() + "  " + i);
             try {
		        	  
		  System.out.println ("Thread " + Thread.currentThread().getId() + " is running");  
		            
		    Thread.sleep(500);
		     } catch (Exception e) {
		         System.out.println(e);
		         }
		 }
	   }

	         public static void main(String[] args) throws Exception {
		     Thread thread = new Thread(new MultiThread());
		      
		      thread.start();

		    Thread.currentThread().setPriority(6); 
		     System.out.println("main thread priority : " + Thread.currentThread().getPriority()); 
		   }
	} 
		 


