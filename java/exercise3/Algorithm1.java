Requirements:
Write a generic method to find the maximal element in the range [begin, end) of a list

Entities:
public final class Algorithm1

Functiondeclaration:
public static <T extends Object & Comparable<? super T>>
        T max(List <? extends T> list, int begin, int end)

Jobs to be done:
1.create and inport package.
2.create class called public final class Algorithm1
3.Find maximal element using begin()and end() of list

program:

package com.java.training.core.Generics;
import java.util.*;
public final class Algorithm1 {
    public static <T extends Object & Comparable<? super T>>
        T max(List <? extends T> list, int begin, int end) {

        T maxElem = list.get(begin);

        for (++begin; begin < end; ++begin)
            if (maxElem.compareTo(list.get(begin)) < 0)
                maxElem = list.get(begin);
        return maxElem;
    }
}
