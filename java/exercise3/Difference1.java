/*
Requirement:
    demonstrate establish the difference between lookingAt() and matches().
Entity:
   The class named as Difference
Function Declaration:
    public static void main(String[] args)
Jobs to be done :
1.create and import the package.
2.create a class called as Difference.
3.Declare two strings name as w and g and assign values for both 
4.By using lookingAt() returns true if, and only if, a prefix of the input sequence matches this matcher's pattern.
5.Matches() This method returns true if, and only if,this string matches the given regular expression.
6.Print the boolean values
pseudo code:
public class Difference {
    
    public static String w = "happy";
    public static String g = "sad";
    public static void main(String[] args) {
        pattern pattern
        Matcher matcher
        System.out.println("lookingAt(): "+ matcher.lookingAt());
        System.out.println("matches(): "+ matcher.matches());
        
    }

}
*/
program:
package com.java.training.core.regex;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Difference {
            public static String w = "happy";
	    public static String g = "sad";
	    public static Pattern pattern;
	    public static Matcher matcher;
	    public static void main(String[] args) {
	        pattern = Pattern.compile(w);
	        matcher = pattern.matcher(g);
	        System.out.println("lookingAt(): "+ matcher.lookingAt());
	        System.out.println("matches(): "+ matcher.matches());
	    }

	}


