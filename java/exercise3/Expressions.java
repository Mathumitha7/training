Requirements:
To perform addition,subtraction,multiplication,division concepts are achieved using la,bda expressions and function interface.

Entities:
the class named as Expressions.

FunctionDeclaration:
public static void main(String[] args) 
The function declared as Arithmetic


Jobs to be done:
1.Create a package.
2.import the package.
3.create a class called Expressions
4.perform addition,subtraction,multiplication,division concepts.
5.print the output

program:

package com.java.training.core.Collections;
import java.util.*;
interface Arithmetic {
	int operation(int a, int b);
}

public class Expressions {

  public static void main(String[] args) {
		        Arithmetic addition = (int a, int b) -> (a + b);
			System.out.println("Addition = " + addition.operation(5, 6));
			Arithmetic subtraction = (int a, int b) -> (a - b);
			System.out.println("Subtraction = " + subtraction.operation(5, 3));
                        Arithmetic multiplication = (int a, int b) -> (a * b);
                        System.out.println("Multiplication = " + multiplication.operation(4, 6));
                        Arithmetic division = (int a, int b) -> (a / b);
			System.out.println("Division = " + division.operation(12, 6));
				
			}
		}

