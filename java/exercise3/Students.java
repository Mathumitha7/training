/*
Requirement:
 LIST CONTAINS 10 STUDENT NAMES
    krishnan, abishek, arun,vignesh, kiruthiga, murugan,
    adhithya,balaji,vicky, priya and display only names starting with 'A'.
Entities:
The class named as  Students 
Function Declaration:
   public static void main(String[] args) 
Jobs to be done:
1.create and import package
2.Create a class called Students
3.create a list named as student and add 10 values in list
4.use startsWith() method ,to print the letter A of starting values
5.print the output
pseudocode:
public class Students {

	public static void main(String[] args) {
		 List<String> student = new ArrayList<String>();//create a list
			student.add("krishnan"); 	
			student.add("abishek");
			student.add("arun");
			student.add("vignesh");
			student.add("kiruthiga");
			student.add("murugan");
			student.add("adhithya");
			student.add("balaji");
			student.add("vicky");
			student.add("priya");
			for(String m : student) {
			    if(m.startsWith("a")) 
			    	System.out.println(m);
			    }
		    }
 }
*/   
program:
package com.java.training.core.Collections;
import java.util.ArrayList;
import java.util.List;
public class Students {

	public static void main(String[] args) {
		 List<String> student = new ArrayList<String>();
			student.add("krishnan"); 	
			student.add("abishek");
			student.add("arun");
			student.add("vignesh");
			student.add("kiruthiga");
			student.add("murugan");
			student.add("adhithya");
			student.add("balaji");
			student.add("vicky");
			student.add("priya");
			for(String m : student) {
			    if(m.startsWith("a")) 
			    	System.out.println(m);
			    }
		    }
 }




