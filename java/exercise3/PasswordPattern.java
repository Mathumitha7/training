/*
Requirement:
Create a pattern for password which contains 
8 to 15 characters in length
Must have at least one uppercase letter
Must have at least one lowercase letter
must have atleast one digit.
Entities:
The class named as PasswordPattern
FunctionDeclaration:
public static boolean validPassword(String password)
public static void main(String[] args)
Jobs to be done:
1.create and import the package.
2.create class called PasswordPattern 
3.Create a  string inside the method and check condition using if loop.
  3.1 create a matcher as object and return the value.
4.In main method,scanner() is used to get the input from user.
5.print the result.
pseudocode:
public class PasswordPattern {
	
	
	    public static boolean validPassword(String password) {
	        String word =  "^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])" + "(?=\\S+$).{8,15}$";
	     
	        Pattern pattern = Pattern.compile(word);
	        if (password == null) {
	            return false;
	        }
	    
	        Matcher matcher = pattern.matcher(password);
	        return matcher.matches();
	    }

	    public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);
	        System.out.println("Enter the Password ");
	        String password = scanner.next();
	        System.out.println(validPassword(password));
	        scanner.close();
	    }

	    
	}
	 
*/
program:
package com.java.training.core.regex;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordPattern {
	
	
	    public static boolean validPassword(String password) {
	        String word =  "^(?=.*[0-9])" + "(?=.*[a-z])(?=.*[A-Z])" + "(?=\\S+$).{8,15}$";
	     
	        Pattern pattern = Pattern.compile(word);
	        if (password == null) {
	            return false;
	        }
	    
	        Matcher matcher = pattern.matcher(password);
	        return matcher.matches();
	    }

	    public static void main(String[] args) {
	        Scanner scanner = new Scanner(System.in);
	        System.out.println("Enter the Password ");
	        String password = scanner.next();
	        System.out.println(validPassword(password));
	        scanner.close();
	    }

	    
	}
	 

