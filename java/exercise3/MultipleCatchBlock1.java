/*
Requirement:
Explain multiple catchblock with example.

Entities:
The class named as MultipleCatchBlock1

Functiondeclaration:
public static void main(String[] args)

Jobs to be done:
1.Create a package.
2.create a class called MultipleCatchBlock1
3.In main method, try block and multiple catch block is used.
4.Using multiple catch blocks are ArithmeticException(),ArrayIndexOutOfBoundsException(),Exception()
5.print the output.

Pseudocode:
 public class MultipleCatchBlock1 {

	public static void main(String[] args) {
		  //create try block
		           try{    
		                int a[]=new int[5];    
		                a[5]=30/0;    
		               }  
                 // create multiple catch block  
		               catch(ArithmeticException e)  
		                  {  
		                   System.out.println("Arithmetic Exception occurs");  
		                  }    
		               catch(ArrayIndexOutOfBoundsException e)  
		                  {  
		                   System.out.println("ArrayIndexOutOfBounds Exception occurs");  
		                  }    
		               catch(Exception e)  
		                  {  
		                   System.out.println("Parent Exception occurs");  
		                  }             
		               System.out.println("rest of the code");    
		    }  
}  
*/

program:

package com.java.training.core.ExceptionHandling;

public class MultipleCatchBlock1 {

	public static void main(String[] args) {
		
		           try{    
		                int a[]=new int[5];    
		                a[5]=30/0;    
		               }    
		               catch(ArithmeticException e)  
		                  {  
		                   System.out.println("Arithmetic Exception occurs");  
		                  }    
		               catch(ArrayIndexOutOfBoundsException e)  
		                  {  
		                   System.out.println("ArrayIndexOutOfBounds Exception occurs");  
		                  }    
		               catch(Exception e)  
		                  {  
		                   System.out.println("Parent Exception occurs");  
		                  }             
		               System.out.println("rest of the code");    
		    }  
}  

