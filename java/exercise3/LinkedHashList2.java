Requirement:
To demonstrate linked hash set to array() method in java
Entities:
The class named as LinkedHashSet2.

FunctionDeclaration:
public static void main(String[] args

Jobstobedone:
1.Create a package.
2.Import util package.
3.create a class callled LinkedHashSet2.
4.create a new linkedhashset and add the elements to it
5.create a array and iterate the for loop of the elementsin hash.
6.print the output.

program:
package com.java.training.core.setsandmaps;
import java.util.*;
public class LinkedHashSet2 {

	public static void main(String[] args) {
		Set<String> hash = new LinkedHashSet<String>();
		hash.add("chennai");
		hash.add("bangalore");
		hash.add("mumbai");
		hash.add("delhi");
		hash.add("italy");
		System.out.println(""+hash);
		Object[] b=hash.toArray();
		for(int i=0;i<b.length;i++) {
			System.out.println(""+b[i]);
			
		}
		
	}

}
