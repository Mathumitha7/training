/*
Requirements:
For the following code use the split()method and print the sentence
String website="https-www-google-com";
Entities:
The class named as SplitMethod
FunctionDeclaration:
public static void main(String[] args)
Jobs to be done:
1.create a package.
2.create class called SplitMethod
3.In main method,create a string and assign the value for it 
 3.1 use splitmethod,to split the worrds in string
 3.2use forloop,and print the result 
Pseudocode:
public class SplitMethod {
	public static void main(String[] args) {
			
			        String website = "http-www-google-com"; 
			        String[] arrOfStr = website.split("-",4); 
			  
			        for (String a : arrOfStr) 
			            System.out.println(a); 
	
*/
program:
package com.java.training.core.regex;

public class SplitMethod {
	public static void main(String[] args) {
			
			        String website = "http-www-google-com"; 
			        String[] arrOfStr = website.split("-",4); 
			  
			        for (String a : arrOfStr) 
			            System.out.println(a); 
		} 
}
