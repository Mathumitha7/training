/*
Requirements:
   To write a Program to print the volume of a Rectangle using lambda expression.
Entities:
   The class named as VolumeOfRectangle
   - Values (Interface)
Function Declaration:
   - int Values(int base,int length,int height);
   - public static void main(String[] args)
Jobs to be done:
   1.Create a class as VolumeOfRectangle with interface as Values.
   2.Inside the declaring the single method with integer parameters.
   3.In the class main creating interface object and assigning with passing  values.
   4.Print statement invoking the interface single method with multiple integer value and finally return value using assigned interface object lambda expression.
Psuedocode:
    
       //create an interface
         interface Values{
	         int values(int base,int length,int height);
	         
	     public class VolumeOfRectangle {

	          public static void main(String[] args) {    
	          
	    //call the interface with an object and perform the operation
	     Rectvalues rectvalues=(base,length,height) -> base*length*height;
	     
	    //print statement invoking the interface with multiple integer value
 */

program:
package com.java.training.core.lambda;
interface Values{
	int values(int base,int length,int height);
}
public class VolumeOfRectangle {

	public static void main(String[] args) {
		
		
				Values v=(base,length,height) -> base*length*height;
				System.out.println(v.values(5,5,5));
		      
			}

		}
