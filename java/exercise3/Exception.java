/*
Requirement:
    To handle and give the reason for the exception in the following code

Entity:
  the class named as Exception
	
Functiondeclaration:
	public static void main(String[] args)
	
Jobs to be done:
1.create a package.
2.create a class called Exception 
3.Create an array inside the try block
        3.1 Print arr[7]
        3.2 If the index is not there in created array,
        3.3.Print the catch block
pseudocode:
    public class Exception {

	public static void main(String[] args) {
		      //create try and catch block
			    try {
			        int arr[] ={1,2,3,4,5};
			            System.out.println(arr[7]);
			}   catch(ArrayIndexOutOfBoundsException e) {
			        System.out.println("The specified index does not exist " + "in array");
			}
		   
	}

} 
*/ 
program:
package com.java.training.core.ExceptionHandling;

public class Exception {

	public static void main(String[] args) {
		
			    try {
			        int arr[] ={1,2,3,4,5};
			            System.out.println(arr[7]);
			}   catch(ArrayIndexOutOfBoundsException e) {
			        System.out.println("The specified index does not exist " + "in array");
			}
		   
	}

}
