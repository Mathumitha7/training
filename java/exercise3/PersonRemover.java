/*
Requirements:
    - Consider the following code snippet:
            List<Person> newRoster = new ArrayList<>();
            newRoster.add(
                new Person(
                "John",
                IsoChronology.INSTANCE.date(1980, 6, 20),
                Person.Sex.MALE,
                "john@example.com"));
            newRoster.add(
                new Person(
                "Jade",
                IsoChronology.INSTANCE.date(1990, 7, 15),
                Person.Sex.FEMALE, "jade@example.com"));
            newRoster.add(
                new Person(
                "Donald",
                IsoChronology.INSTANCE.date(1991, 8, 13),
                Person.Sex.MALE, "donald@example.com"));
            newRoster.add(
                new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
       - Remove the only person who are in the newRoster from the roster list.
       - Remove the following person from the roster List:
            new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
Entities:
    - PersonRemover
    
Function Declaration:
    - public static void main(String[] args)
    
Job to be done:
    - Create class called  PersonRemover
    - Declare and define main method
    - Invoke createRoster() method from Person-class and store the returned value in a list-roster
    - Create a new list-newRoster and add the values given
    - Remove elements from roster if it is in newRoster 
    -print the result
*/
package com.java.training.core.Streams;
import java.util.List;
import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.stream.*;

public class PersonRemover {

    public static void main(String[] args) {
    	List<Person> roster = Person.createRoster();
    	List<Person> newRoster = new ArrayList<>();
    	newRoster.add(
                new Person(
                "John",
                IsoChronology.INSTANCE.date(1980, 6, 20),
                Person.Sex.MALE,
                "john@example.com"));
        newRoster.add(
                new Person(
                "Jade",
                IsoChronology.INSTANCE.date(1990, 7, 15),
                Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add(
                new Person(
                "Donald",
                IsoChronology.INSTANCE.date(1991, 8, 13),
                Person.Sex.MALE, "donald@example.com"));
        newRoster.add(
                new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
        roster.removeIf((person) -> 
        	newRoster.stream().anyMatch((checker) ->  person.getName().equals(checker.getName()) & 
    	                             person.getAge() == checker.getAge() & 
    	                             person.getGender().equals(checker.getGender()) &
    	                             person.getEmailAddress().equals(checker.getEmailAddress()))
        	
        );
        for(Person person : roster) {
        	System.out.println(person.getName());
        }
    }
}

