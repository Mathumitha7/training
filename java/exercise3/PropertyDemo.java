/*
Requirement:
  - Program to perform the following operations in Properties.
    i) add some elements to the properties file.
    ii) print all the elements in the properties file using iterator.
    iii) print all the elements in the properties file using list method.
Entity:
  - The class named as PropertyDemo
Method Signature:
  - public static void main(String[] args)
Jobs to be done:
    1.Create an object properties for Properties class.
    2.Set some properties with key and values to properties object.
    3.Display all property using Iterator object with keySet.
    4.Using Stream to display all property in entrySet.
    5.print the result
Pseudo Code:
public class PropertyDemo {

    public static void main(String[] args) {

        // Creating an object of Properties class
        Properties properties = new Properties();

        properties.setProperty("username", "mathumitha");
        properties.setProperty("email", "mathumitha123@gmail.com");
        properties.setProperty("password", "mathu001");

        // Using Iterator to display key and values on properties
        Iterator<?> propertiesIterator = properties.keySet().iterator();
        while (propertiesIterator.hasNext()) {
            String key = (String) propertiesIterator.next();
            System.out.println(key + " : " + properties.get(key));
        }

        
        properties.entrySet().stream().forEach(System.out::println);
    }

}
*/
package com.java.training.core.properties;
import java.util.Iterator;
import java.util.Properties;

public class PropertyDemo {

    public static void main(String[] args) {

        
        Properties properties = new Properties();

        properties.setProperty("username", "mathumitha");
        properties.setProperty("email", "mathumitha123@gmail.com");
        properties.setProperty("password", "mathu001");

       
        Iterator<?> propertiesIterator = properties.keySet().iterator();
        while (propertiesIterator.hasNext()) {
            String key = (String) propertiesIterator.next();
            System.out.println(key + " : " + properties.get(key));
        }

        
        properties.entrySet().stream().forEach(System.out::println);
    }

}

