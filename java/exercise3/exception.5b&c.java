/*
Requirement:
1)It is possible to have morethan one try block?
2)To write the difference between catching multiple exceptions and Mutiple catch blocks.
*/
Explanation:
1)It cannot have multiple try blocks with a single catch block. Each try block must be followed by catch or finally. Still ,if try to have single catch block for multiple try blocks a compile time error is generated.
2)Multiple catch block:
For catching different exceptions need to write different catch blocks.

eg:
try{
 //code 
}catch(IOException ex1){
 //code mcb
} catch(SQLException ex2){
 //code 
}   

catching multiple exceptions:
This is also known as multi catch,We could separate different exceptions using pipe ( | ).

eg:
try{
 //code  
}catch(IOException | SQLException ex) //syntax {
 //code  
}