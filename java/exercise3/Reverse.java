Requirement:
To Reverse List Using Stack with minimum 7 elements in list.

Entities:
The class named as Reverse

FunctionDeclaration:
public static void main(String[] args)

Jobs to be done:
1.create and import a package.
2.Create a class called Reverse
3.create a list and adding the elements using add() method.
4.To reverse a stack create a new stack removing the values using remove() method and pushing to new created 
stack using push() method 
5.Removing elements from stack adding to list using add() method.
6.print the result.
 
program:

package com.java.training.core.Stack;
import java.util.*;
public class Reverse {

	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		list.add("x");
		list.add("y");
		list.add("z");
		System.out.println(list);

		Stack<String> stack = new Stack<String>();
		while(list.size() > 0) {
		    stack.push(list.remove(0));
		}

		while(stack.size() > 0){
		    list.add(stack.pop());
		    
		}

		System.out.println(list);
	}

}
