/*
Requirements:
   - Convert the following anonymous class into lambda expression.
   What's wrong with the following program? And fix it using Type Reference
interface BiFunction{
    int print(int number1, int number2);
}

public class TypeInferenceExercise {
    public static void main(String[] args) {

        BiFunction function = (int number1, int number2) ->  { 
        return number1 + number2;
        };
        
        int print = function.print(int 23,int 32);
        
        System.out.println(print);
    }
}
Entities:
   -The class named as  TypeInferenceExercise
   - BiFunction (Interface)
Function Declaration:
   - int print(int number1, int number2)
   - public static void main(String[] args)
Jobs to be done:
   1.Create a class as TypeInferenceExercise with interface as BiFunction.
   2.Inside the declaring the single method with two integer parameters.
   3.In the class main creating interface object and assigning with passing integer values returning the addition of two values.
   4.Print statement invoking the interface single method with integer value and finally return the value using assignedinterface object lambda expression.
   5.print the output
Pseudocode:
//create a interface
interface BiFunction{
    int print(int number1, int number2);
}
public class TypeInferenceExercise {

	public static void main(String[] args) {
                         //call the interface with an object and perform the operation
		         BiFunction function = (number1, number2) -> number1 + number2; 
			     int print = function.print(23,32); 
			     System.out.println(print);

 */

program:
package com.java.training.core.lambda;
interface BiFunction{
    int print(int number1, int number2);
}
public class TypeInferenceExercise {

	public static void main(String[] args) {
		         BiFunction function = (number1, number2) -> number1 + number2; 
			     int print = function.print(23,32); 
			     System.out.println(print);

		    }
}

	
