/*
Requirements:
Create two paths and test whether they represent same path
Entities:
The class named as CreatePath
Functiondeclaration:
public static void main(String[] args)
 private static void testSameFile(Path path1, Path path2)
Jobstobedone:
1.create package and import it.
2.create a class called CreatePath
3.In main method,create a path and set the pathname
 3.1In testSameFile,passthe parameter
4.create a method called testSameFile and try&catch block is used
  4.1 Using if condition,check all the paths in same path and print the results.
pseudocode:
public class CreatePath {
	 public static void main(String[] args) {
	    Path path1 = Paths.get("C://data//char");
	    Path path2 = Paths.get("C://data//byteoutput1.txt");
	    Path path3 = Paths.get("C://data//byteoutput2.txt");

	    testSameFile(path1, path2);
	    testSameFile(path1, path3);
	  }
	  private static void testSameFile(Path path1, Path path2) {
	    try {
	      if (Files.isSameFile(path1, path2)) {
	        System.out.println("same file");
	      } else {
	        System.out.println("NOT the same file");
	      }
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	  }
	}

*/
program:
package com.java.training.core.nio;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CreatePath {
	 public static void main(String[] args) {
	    Path path1 = Paths.get("C://data//char");
	    Path path2 = Paths.get("C://data//byteoutput1.txt");
	    Path path3 = Paths.get("C://data//byteoutput2.txt");

	    testSameFile(path1, path2);
	    testSameFile(path1, path3);
	  }
	  private static void testSameFile(Path path1, Path path2) {
	    try {
	      if (Files.isSameFile(path1, path2)) {
	        System.out.println("same file");
	      } else {
	        System.out.println("NOT the same file");
	      }
	    } catch (IOException e) {
	      e.printStackTrace();
	    }
	  }
	}

