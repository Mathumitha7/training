Requirement:
To write a java program to demonstrate insertions and string buffer in tree set.

Entities:
The class named as StringBufferSort implements Comparator<StringBuffer>

FunctionDeclaration:
public static void main(String[] args)

Jobstobedone:
1.create a package.
2.Import Comparator,TreeSet packages.
3.create a class called StringBufferSort implements Comparator<StringBuffer>
4.create a new TreeSet namedas tset and add the elements in it.
5.print the tset.
6.create another TreeSet namedas tset1 and add the elements using StringBuffer() method.
7.print the tset1

program:

package com.java.training.core.setsandmaps;

import java.util.Comparator;
import java.util.TreeSet;
public class StringBufferSort implements Comparator<StringBuffer>{
   @Override
   public int compare(StringBuffer strB1, StringBuffer strB2) {
      return strB1.toString().compareTo(strB2.toString());
   }
   public static void main(String[] args) {
      TreeSet<Integer> tset = new TreeSet<>();
      tset.add(1);
      tset.add(2);
      tset.add(3);
      tset.add(4);
      tset.add(5);
      System.out.println(tset);
      TreeSet<StringBuffer> tset1 = new TreeSet<>(new StringBufferSort());	
      tset1.add(new StringBuffer("Brown"));
      tset1.add(new StringBuffer("Yellow"));
      tset1.add(new StringBuffer("Red"));
      tset1.add(new StringBuffer("Grey"));
      tset1.add(new StringBuffer("White"));
      System.out.println(tset1);
   }
}
