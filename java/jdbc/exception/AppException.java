/*
 Problem Statement:
 1.Create a AppException
 Entity:
 1.AppException
 Method Signature:
 1.public AppException();
 2.public AppException(ErrorCode code);
 Jobs to be Done:
 1.Create a empty parameter constructor
 2.Create a Constructor with parameter of ErrorCode as code
    2.1)return the super errorCode and code messgae
 3.Create another constructor with ErrorCode and Exception as parameter
    3.1)return the super with errorCode and error message and StackTrace 
 Pseudo Code:
 public class AppException extends RuntimeException{
	
	public AppException() {
		super();
	}
	
	public AppException(ErrorCode code) {
		super("Error:" + code + " " + code.getMessage());
	}
	
	public AppException(ErrorCode code, Exception e) {
		super(code + " " + code.getMessage(), e);
	}
}
*/
package com.kpr.training.exception;

@SuppressWarnings("serial")
public class AppException extends RuntimeException{
	
	public AppException() {
		super();
	}
	
	public AppException(ErrorCode code) {
		super("Error:" + code + " " + code.getMessage());
	}
	
	public AppException(ErrorCode code, Exception e) {
		super(code + " " + code.getMessage(), e);
	}
}
