/*
Requirement:
    To create a connection between the database and java application.
    
Entity:
    JdbcConnection
    
Function declaration:
    public void init() 
    public void release() 
    
Jobs to be done:
    
    1. Declare Connection con.
    2. Declare init method.
    	2.1 Load db.properties file to a properties object.
        2.2 Establish the connection using sql driver and store it as con.
    3. Declare release method.
        3.1 Close the Connection of the server.
        3.2 Close the PreparedStatement of the query.
    4. Declare commit method.
        4.1 Commit the changes made after execution of query.
    5. Declare rollback method.
        5.1 Rollback the changes made by executing the query.

Pseudo code:
class ConnectionService {
    
    public Connection con;

    public void init() {

        Properties properties = new Properties();
        
        try {
            InputStream resourceAsStream =
                    ConnectionService.class.getClassLoader().getResourceAsStream("db.properties");
            if (resourceAsStream != null) {
                properties.load(resourceAsStream);
            }

        } catch (IOException e) {
            throw new AppException(ErrorCode.IO_EXCEPTION);
        }

        try {
            con = DriverManager.getConnection(properties.getProperty("URL"),
                    properties.getProperty("User Name"), properties.getProperty("Password"));
            con.setAutoCommit(false);

        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }

    }

    public void release() {
        try {
            con.close();
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }
    }
    
    public void commit() {
        try {
            con.commit();
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }
    }

    public void rollback() {
        try {
            con.rollback();
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }
    }
}

*/

package com.kpr.training.jdbc.service;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCode;

@SuppressWarnings("unused")
public class ConnectionService{

    public Connection con;

    public void init() {

        Properties properties = new Properties();

        try {
            InputStream resourceAsStream =
                    ConnectionService.class.getClassLoader().getResourceAsStream("db.properties");
            if (resourceAsStream != null) {
                properties.load(resourceAsStream);
            }

        } catch (IOException e) {
            throw new AppException(ErrorCode.IO_EXCEPTION);
        }

        try {
            con = DriverManager.getConnection(properties.getProperty("URL"),
                    properties.getProperty("User Name"), properties.getProperty("Password"));
            con.setAutoCommit(false);

        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }

    }

    public void release() {
        try {
            con.close();
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }
    }

    public void commit() {
        try {
            con.commit();
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }
    }

    public void rollback() {
        try {
            con.rollback();
        } catch (SQLException e) {
            throw new AppException(ErrorCode.SQLException);
        }
    }
}