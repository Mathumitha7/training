package com.kpr.training.model;

import java.sql.Timestamp;
import java.sql.Date;

public class Person {

	public long id;
	public String firstName;
	public String lastName;
	public String email;
	public Date birthDate;
	public Date createdDate;
	public Address address;
  
	public Person(long id, String firstName, String lastName, String email, long addressId, Date birthDate, Date createdDate, Address address) {
        this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.birthDate = birthDate;
		this.address = address;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) { //email
		this.email = email;
	}
	
	public Date getBirthDate() {
		return birthDate;
	}
	
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public long getAddressId() {
		return address.getId();
	}
	public void setAddressId(long id) { // id
		address.setId(id); 
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Address getAddress() {
		return address;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		
		return new StringBuilder("Person [id =")
						.append(id)
						.append(", firstName =")
						.append(firstName)
						.append(", lastName =")
						.append(lastName)
						.append(", email =")
						.append(email)
						.append(", addressId =")
						.append(address.getId())
						.append(", birthDate=")
						.append(birthDate)
						.append(", createdDate=")
						.append(createdDate)
						.append(", address=")
						.append(address)
						.append("]").toString();
	}
	
}
