/*
Problem Statement:
 1.POJO Class for Address
Entity:
 1.Address
Method Signature:
  1.public Address(String street, String city, long postal_code);
  2.public String getStreet();
  3.public String getCity();
  4.public long getPostal_code();
Jobs to be Done:
1.Create a id field of long type
2.Create a street field of String type
3.Create a city field of String type
4.Create a postal_code of long type
5.Create a public constructor take all the Address fields
6.Create a Street getter of String return type
7.Create a city getter of String return type
8.Create a postal_code getter of long type
Pseudo Code:
 class Address {

	private long id;
	private String street;
	private String city;
	private long postal_code;
	
	public Address(String street, String city, long postal_code) {
		this.street = street;
		this.city = city;
		this.postal_code = postal_code;
	}
	
	public Address(long id, String street, String city, long postal_code) {
		this.id = id;
		this.street = street;
		this.city = city;
		this.postal_code = postal_code;
	}
	
	public Address(long id) {
		this.id = id;
	}
	
	public long getId() {
		return id;
	}
	
	public String getStreet() {
		return street;
	}
	
	public String getCity() {
		return city;
	}
	
	public long getpostal_code() {
		return postal_code;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public void setpostal_code(long postal_code) {
		this.postal_code = postal_code;
	}
	
	
	public String toString() {
		return new StringBuilder("Address [id=")
						.append(id)
						.append(", street=")
						.append(street)
						.append(", city =")
						.append(city)
						.append(", postal_code =")
						.append(postal_code)
						.append("]")
						.toString();
	}
	
}
*/
package com.kpr.training.model;

public class Address {

	private long id;
	private String street;
	private String city;
	private long postal_code;
	
	public Address(String street, String city, long postal_code) {
		this.street = street;
		this.city = city;
		this.postal_code = postal_code;
	}
	
	public Address(long id, String street, String city, long postal_code) {
		this.id = id;
		this.street = street;
		this.city = city;
		this.postal_code = postal_code;
	}
	
	public Address(long id) {
		this.id = id;
	}
	
	public long getId() {
		return id;
	}
	
	public String getStreet() {
		return street;
	}
	
	public String getCity() {
		return city;
	}
	
	public long getpostal_code() {
		return postal_code;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public void setpostal_code(long postal_code) {
		this.postal_code = postal_code;
	}
	
	
	public String toString() {
		return new StringBuilder("Address [id=")
						.append(id)
						.append(", street=")
						.append(street)
						.append(", city =")
						.append(city)
						.append(", postal_code =")
						.append(postal_code)
						.append("]")
						.toString();
	}
	
}

