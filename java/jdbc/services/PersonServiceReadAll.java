/*
Requirement:
 To Perform Read All Person for Person Service
Entity:
  1.Person
  2.PersonServiceReadAll
Method Signature:
 -public List<Person> readAllPerson()
Jobs to be Done:
  1.Create a Person list called allPerson
  2.Create a person of Person type
  3.Establish the Connection with getConnection method from the Connections Class
  4.Check whether the connection is equal to null then throw AppException "the connection was failed"
  5.Prepare the PreparedStatmenet called statement with the readAllPersonQuery.
  6. execute the Query and Store the statement in  result
  7. In whileloop,check  the the result has next 
  		7.1)Store the id to person.id
  		7.2)Store the name to person.name
  		7.3)Store the email to person.email
  		7.4)Store the address_id to person.address_id
 		7.5)Store the birth_date to person.birth_date
                7.6)Store the created_date to person.created_date
8.Add the person to addPerson List
9.Check whether the addPerson is equal to null,then throw AppException
10.close the connection and return allPerson
pseudocode:
public class PersonServiceReadAll {
public List<Person> readAllPerson() throws AppException {
		
		List<Person> allPerson = new ArrayList<>();
		
		Person person = null;
		
		Connection connection = Connections.getConnection();
		
		if(connection == null) {
			throw new AppException("the connection was failed");
		}
		
		try {
			PreparedStatement statement = connection.prepareStatement(QueryStatement.readAllPersonQuery);
			
			ResultSet result = statement.executeQuery();
			
			while(result.next()) {
				person.id = result.getLong("id");
				person.name = result.getString("name");
				person.email = result.getString("email");
				person.address_id = result.getLong("address_id");
				person.birth_date = result.getDate("birth_date");
				person.created_date = result.getDate("created_date");
				
				allPerson.add(person);
			}
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(allPerson == null) {
			throw new AppException("person read failure");
		}
		return allPerson;
	}
	
}

*/
package com.kpr.training.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.kpr.training.connection.Connections;
import com.kpr.training.exception.AppException;
import com.kpr.training.model.Person;
import com.kpr.training.queryStatement.QueryStatement;

public class PersonServiceReadAll {
public List<Person> readAllPerson() throws AppException {
		
		List<Person> allPerson = new ArrayList<>();
		
		Person person = null;
		
		Connection connection = Connections.getConnection();
		
		if(connection == null) {
			throw new AppException("the connection was failed");
		}
		
		try {
			PreparedStatement statement = connection.prepareStatement(QueryStatement.readAllPersonQuery);
			
			ResultSet result = statement.executeQuery();
			
			while(result.next()) {
				person.id = result.getLong("id");
				person.name = result.getString("name");
				person.email = result.getString("email");
				person.address_id = result.getLong("address_id");
				person.birth_date = result.getDate("birth_date");
				person.created_date = result.getDate("created_date");
				
				allPerson.add(person);
			}
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(allPerson == null) {
			throw new AppException("person read failure");
		}
		return allPerson;
	}
	
}
