/*
Requirement:
 To Perform insert Person for Person Service
Entity:
-The class named as PersonServiceInsert
-person
Methodsignature:
public long insert(Person person)
Jobs to be Done:
1.create a class called PersonServiceInsert
2.Check whether the name, email, birth date and address_id is equal to null ,then throw new AppException 
3.Establish a Connection with getConnection of Connections Class and store it in connections
4.Prepare the preparedStatement of insertPersonQuery in QueryStatement Class and store it in statement of PrepareStatment type
5..Set Person values in statement.
6.Execute the statement and store the person id to personId
7.Check whether the personId is 0,then throw AppException "person is not be empty"
8.close the connection and return personid
Pseudo Code:
public class PersonServiceInsert {
	
	public long insert(Person person) throws AppException {
			
		if(person.getName() == null && person.getEmail() == null && person.getBirthDate() == null ) {
			throw new AppException("person creation is failure");
		}
			
		Connection connection = Connections.getConnection();
		try {
			PreparedStatement statement = connection.prepareStatement(QueryStatement.insertPersonQuery);
				
			statement.setString(1, person.getName());
			statement.setString(2, person.getEmail());
			statement.setLong(3, person.getAddressId());
			statement.setDate(4, person.getBirthDate());
				
			person.id = statement.executeUpdate();
				
			if(person.id == 0) {
				throw new AppException("personid is not be empty");
			}
			connection.close();
				
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return person.id;
	}
}
*/
package com.kpr.training.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import com.kpr.training.connection.Connections;
import com.kpr.training.exception.AppException;
import com.kpr.training.model.Person;
import com.kpr.training.queryStatement.QueryStatement;
public class PersonServiceInsert {
	
	public long insert(Person person) throws AppException {
			
		if(person.getName() == null && person.getEmail() == null && person.getBirthDate() == null ) {
			throw new AppException("person creation is failure");
		}
			
		Connection connection = Connections.getConnection();
		try {
			PreparedStatement statement = connection.prepareStatement(QueryStatement.insertPersonQuery);
				
			statement.setString(1, person.getName());
			statement.setString(2, person.getEmail());
			statement.setLong(3, person.getAddressId());
			statement.setDate(4, person.getBirthDate());
				
			person.id = statement.executeUpdate();
				
			if(person.id == 0) {
				throw new AppException("personid is not be empty");
			}
			connection.close();
				
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return person.id;
	}
}
