/*
Requirement
  To Perform Read All Address for AddressService
Entity:
-The class named as AddressServiceReadAll
-Address
Jobs to be Done:
 1.create a class called AddressServiceReadAll and create a address of Addresstype
 2.Create a List of Address type called addressList
 3.Establish a Connection with getConnection method on Connections class
 4.Check whether the connection is null then throw AppExcepion 101
 5.Prepare the Statement with readAllAddressQuery query from QueryStatement Class
 6.execute the Query and store the result to the Result Set called result
 7.To get the statement and store to the address of Address type and Add to the List addressList 
 8.close() method,is used to close connection and return the addressList.
Pseudo Code:
public class AddressServiceReadAll {
public List<Address> readAllAddress() throws AppException {
		
		Address address = null;
		List<Address> addressList = new ArrayList<>();
		
		Connection connection = Connections.getConnection();
		
		if(connection == null) {
			throw new AppException("101");
		}
		
		try {
			PreparedStatement statement = connection.prepareStatement(QueryStatement.readAllAddressQuery);
			
			ResultSet result = statement.executeQuery();
			
			while(result.next()) {
				address.street = result.getString("street");
				address.city = result.getString("city");
				address.postal_code = result.getLong("postal_code");
				
				addressList.add(address);
			}
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return addressList;
	}
	

}
*/
package com.kpr.training.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.kpr.training.connection.Connections;
import com.kpr.training.exception.AppException;
import com.kpr.training.model.Address;
import com.kpr.training.queryStatement.QueryStatement;

public class AddressServiceReadAll {
public List<Address> readAllAddress() throws AppException {
		
		Address address = null;
		List<Address> addressList = new ArrayList<>();
		
		Connection connection = Connections.getConnection();
		
		if(connection == null) {
			throw new AppException("101");
		}
		
		try {
			PreparedStatement statement = connection.prepareStatement(QueryStatement.readAllAddressQuery);
			
			ResultSet result = statement.executeQuery();
			
			while(result.next()) {
				address.street = result.getString("street");
				address.city = result.getString("city");
				address.postal_code = result.getLong("postal_code");
				
				addressList.add(address);
			}
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return addressList;
	}
	

}
