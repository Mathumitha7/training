/*
 Problem Statement:
  To Perform Update Address in Address Service
Entity:
-The class named as AddressServiceUpdate
-Address
Method Signature
  public int update(Address address, long id);
Jobs to be Done:
 1.Create a class called AddressServiceUpdate and declare count=0 of type int
 2.Check whether the postalCode equalto 0 then, Throw AppException 
 3.Establish Connection  and store it in connection of Connection type
 4.Get the update query from QueryStatement and store it in updateQuery of String type
 5.Prepare preparedStatement as statement using connection and updateQuery
 6.To get the statement and store to the address of Address type 
 7.execute the Query and store the result to the Result Set called result
 8.Check whether the count is 0 then,throw AppException 
 9.close() method,to close the connection and return count
pseudocode:
public class AddressServiceUpdate {
	public int update(Address address, long id) throws SQLException {
		int count = 0;
		try {
			
			if(address.getPostalCode() == 0) {
				throw new AppException("100");
			}
			Connection connection = Connections.getConnection();
			
			PreparedStatement statement = connection.prepareStatement(QueryStatement.updateAddressQuery);
			
			statement.setString(1,address.getStreet());
			statement.setString(2,address.getCity());
			statement.setLong(3,address.getPostalCode());
			statement.setLong(4,id);
			
			count = statement.executeUpdate();
			
			if(count == 0) {
				throw new AppException("103");
			}
			connection.close();
		} catch(AppException e) {
			e.printStackTrace();
		}
		
		return count;
	}
		
}

*/
package com.kpr.training.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.kpr.training.connection.Connections;
import com.kpr.training.exception.AppException;
import com.kpr.training.model.Address;
import com.kpr.training.queryStatement.QueryStatement;

public class AddressServiceUpdate {
	public int update(Address address, long id) throws SQLException {
		int count = 0;
		try {
			
			if(address.getPostalCode() == 0) {
				throw new AppException("100");
			}
			Connection connection = Connections.getConnection();
			
			PreparedStatement statement = connection.prepareStatement(QueryStatement.updateAddressQuery);
			
			statement.setString(1,address.getStreet());
			statement.setString(2,address.getCity());
			statement.setLong(3,address.getPostalCode());
			statement.setLong(4,id);
			
			count = statement.executeUpdate();
			
			if(count == 0) {
				throw new AppException("103");
			}
			connection.close();
		} catch(AppException e) {
			e.printStackTrace();
		}
		
		return count;
	}
		
}
