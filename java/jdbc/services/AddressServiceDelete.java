/* Requirement:
    To Perform Delete Address for AddressService
 Entity:
 - Address
 -The class named as AddressServiceDelete
 Method Signature
 -public long delete(long id);
 Jobs to be Done:
 1.create a class called AddressServiceDelete
 2.Create a deletedCount of long type
 3.Check whether the id is equal to 0 then throw new AppException 104
 4.Establish the Connection by getConnection method of Connections Class and store it to connection of Connection type
 5.Prepare the preparedStatement with deleteAddressQuery query and store it to statement of preparedStatement
 6.Set the id to the statement
 7.To execute,executeupdate() method  and store it to deletedCount
 8.Check whether the deletedCount is equal 0 then throw new AppException 105
 9.close the connection using Close() method,and return the deletedCount.
 Pseudo Code:
public class AddressServiceDelete {
	public long deletedCount;
	public long deleteAddress(long id) throws AppException {
		
		long deletedCount = 0;
		try {
			if(id == 0) {
				throw new AppException("104");
			}
			
			Connection connection = Connections.getConnection();
				
			PreparedStatement statement = connection.prepareStatement(QueryStatement.deleteAddressQuery);
				
			statement.setLong(1, id);
				
	
			deletedCount = statement.executeUpdate();
				
			if(deletedCount == 0) {
				throw new AppException("105");
			}
			connection.close();
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return deletedCount;
	}
}
*/
package com.kpr.training.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.kpr.training.connection.Connections;
import com.kpr.training.exception.AppException;
import com.kpr.training.queryStatement.QueryStatement;

public class AddressServiceDelete {
	public long deletedCount;
	public long deleteAddress(long id) throws AppException {
		
		long deletedCount = 0;
		try {
			if(id == 0) {
				throw new AppException("104");
			}
			
			Connection connection = Connections.getConnection();
				
			PreparedStatement statement = connection.prepareStatement(QueryStatement.deleteAddressQuery);
				
			statement.setLong(1, id);
				
	
			deletedCount = statement.executeUpdate();
				
			if(deletedCount == 0) {
				throw new AppException("105");
			}
			connection.close();
		} catch(SQLException e) {
			e.printStackTrace();
		}
		return deletedCount;
	}
}


