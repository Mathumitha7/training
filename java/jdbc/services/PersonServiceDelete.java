/*
Problem Statement:
 To Perform Delete Person for PersonService
Entity:
-The class named as PersonServiceDelete
-Person
Method Signature:
 -public long delete(long id);
Jobs to be Done:
1.create a class called PersonServiceDelete
2.Declare changes equal to 0 of type long
3.Check whether the id equal to 0 then, throw new AppException 
4.Establish the Connection by getConnection in Connections class
5.Check weather the connection is equal to null then,throw new AppException
6.Prepare a preparedStatement called statement with deletePersonQuery from QueryStatement
7.execute the statement with executeUpdate and store the result in changes of long type
8.Check Whether the changes is equal to 0 then throw new AppException as changes is occur
9.close()method,and return the changes
Pseudo Code:
public class PersonServiceDelete {
public long delete(long id) throws AppException {

		
		long changes = 0;
			
		if(id == 0) {personid is not be empty");
			throw new AppException(" ");
		}
			
		Connection connection = Connections.getConnection();
			
		if(connection == null) {
			throw new AppException("the connection was failed");
		}
			
		try {
			PreparedStatement statement = connection.prepareStatement(QueryStatement.deletePerson);
			
			statement.setLong(1, id);
				
			changes = statement.executeUpdate(); 
			
			connection.close();
				
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		if(changes == 0) {
			throw new AppException("the changes is occur");
		}
		return changes;
	}
}
*/
package com.kpr.training.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.kpr.training.connection.Connections;
import com.kpr.training.exception.AppException;
import com.kpr.training.queryStatement.QueryStatement;

public class PersonServiceDelete {
public long delete(long id) throws AppException {

		
		long changes = 0;
			
		if(id == 0) {
			throw new AppException("display personid ");
		}
			
		Connection connection = Connections.getConnection();
			
		if(connection == null) {
			throw new AppException("the connection was failed");
		}
			
		try {
			PreparedStatement statement = connection.prepareStatement(QueryStatement.deletePerson);
			
			statement.setLong(1, id);
				
			changes = statement.executeUpdate(); 
			
			connection.close();
				
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		if(changes == 0) {
			throw new AppException("the changes is occur");
		}
		return changes;
	}
}
