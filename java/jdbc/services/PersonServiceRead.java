/*
Requirement:
 TO Perform Read Person of PersonService
Entity:
  -Person
  -PersonServiceRead
Methodsignature:
-public Person readPerson(long id)
Jobs to be Done:
 1.Check whether the person id is equal to 0 then,throw new AppException "person id cant be empty'
 2.Establish the Connection with getConnection of Connections Class and store it in connection of Connection type.
 3.Prepare the PreparedStatment called statement and called the readPersonQuery from QueryStatement Class
 4.set id which type is long  
 5.Execute the statement and store the person values to result
 6.To store the id, name, email, address_id, birth_date, created_date while result has next
 7.Check whether the result is empty then throw new AppException  "person read failure"
 8.close connection and return the person
  
Pseudo Code:
public class PersonServiceRead{
 public Person readPerson(long id) throws AppException {
	
	Person person = null;
	if(id == 0) {
		throw new AppException("person id cant be empty");
	}
	
	AddressService address = new AddressService();
	
	Address addressGot = address.readAddress(id);
	
	Connection connection = Connections.getConnection();
	
	try {
		PreparedStatement statement = connection.prepareStatement(QueryStatement.readPersonQuery);
		
		statement.setLong(1, id);
		
		ResultSet result = statement.executeQuery();
		
		while(result.next()) {
			person.id = result.getLong("id");
			person.name = result.getString("name");
			person.email = result.getString("email");
			person.address_id = addressGot.getId();
			person.birth_date = result.getDate("birth_date");
			person.created_date = result.getDate("created_date");
		}
		
		if(person == null) {
			throw new AppException("person read failure");
		}
		connection.close();
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	return person;
}

}
*/
package com.kpr.training.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.kpr.training.connection.Connections;
import com.kpr.training.exception.AppException;
import com.kpr.training.model.Address;
import com.kpr.training.model.Person;
import com.kpr.training.queryStatement.QueryStatement;
public class PersonServiceRead{
 public Person readPerson(long id) throws AppException {
	
	Person person = null;
	if(id == 0) {
		throw new AppException("person id cant be empty");
	}
	
	AddressService address = new AddressService();
	
	Address addressGot = address.readAddress(id);
	
	Connection connection = Connections.getConnection();
	
	try {
		PreparedStatement statement = connection.prepareStatement(QueryStatement.readPersonQuery);
		
		statement.setLong(1, id);
		
		ResultSet result = statement.executeQuery();
		
		while(result.next()) {
			person.id = result.getLong("id");
			person.name = result.getString("name");
			person.email = result.getString("email");
			person.address_id = addressGot.getId();
			person.birth_date = result.getDate("birth_date");
			person.created_date = result.getDate("created_date");
		}
		
		if(person == null) {
			throw new AppException("person read failure");
		}
		connection.close();
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	return person;
}

}
