/*
Requirement
 To Perform Update Person in PersonService
Entity:
 -Person
 -PersonServiceUpdate
Method Signature:
 -public long update(Person person)
Jobs to be Done:
 1.Check whether the person id is equal to 0 then,throw new AppException"person id cannot be empty"
 2.Establish the Connection with getConnection of Connections Class
 3.Check Whether the connection is nullthen throw new AppException"the connection was failed"
 4.Prepare the statement with connection with QueryStatement of updatePerson
 5.Set the Statement values
 6.Execute the statment and store the values in changes
 7.Check Whether the changes is equal to 0 then throw new AppException
 8.close connection and return changes 
Pseudo Code:
public class PersonServiceUpdate {
public long update(Person person) throws AppException {
		
		int changes = 0;
			
		if(person.getId() == 0) {
			throw new AppException(" person id cannot be empty");
		}
			
		Connection connection = Connections.getConnection();
			
		if(connection == null) {
			throw new AppException("the connection was failed");
		}
			
		try {
			PreparedStatement statement = connection.prepareStatement(QueryStatement.updatePerson);
				
			statement.setString(1,person.getName());
			statement.setString(2,person.getEmail());
			statement.setDate(3,person.getBirthDate());
			statement.setDate(4,person.getCreatedDate());
			statement.setLong(5,person.getId());
				
			changes = statement.executeUpdate();
			
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(changes == 0) {
			throw new AppException("107");
		}
		
			
		return changes;
	}
}
*/
package com.kpr.training.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.kpr.training.connection.Connections;
import com.kpr.training.exception.AppException;
import com.kpr.training.model.Person;
import com.kpr.training.queryStatement.QueryStatement;

public class PersonServiceUpdate {
public long update(Person person) throws AppException {
		
		int changes = 0;
			
		if(person.getId() == 0) {
			throw new AppException(" person id cannot be empty");
		}
			
		Connection connection = Connections.getConnection();
			
		if(connection == null) {
			throw new AppException("the connection was failed");
		}
			
		try {
			PreparedStatement statement = connection.prepareStatement(QueryStatement.updatePerson);
				
			statement.setString(1,person.getName());
			statement.setString(2,person.getEmail());
			statement.setDate(3,person.getBirthDate());
			statement.setDate(4,person.getCreatedDate());
			statement.setLong(5,person.getId());
				
			changes = statement.executeUpdate();
			
			connection.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(changes == 0) {
			throw new AppException("107");
		}
		
			
		return changes;
	}
}
