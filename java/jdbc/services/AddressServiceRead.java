/*
Requirement:
 To Perform Read an Address in AddressService
Entity:
 -Address
-The class named as AddressServiceRead
Methodsignature:
  -public Address readAddress(long id)
Jobs to be Done:
  1.Create a address of Address type
  2.Check whether address id is equalto 0,then throw AppException 104
  3.Establish the Connection with getConnection method of Connections Class and store it in connection of Connection type
  4.Make a preparedStatement with connection and pass the readAddressQuery and store it to statement of preparedStatement
  5.set the id to the 1st field on the statement
  6.execute ,the Query with statement and store it to the result 
  7.To Store the street value to the address street 
  8.To Store the city value to the  address value
  9.Store the postalCode value to the address postalCode
  10.close() method,is used to close the connection and Return the address.
Pseudo Code:
public class AddressServiceRead {
public Address readAddress(long id) {
		
		Address address = null;
		
		try {
			if(id == 0) {
				throw new AppException("104");
			}
			try {

				Connection connection = Connections.getConnection();
				
				PreparedStatement statement = connection.prepareStatement(QueryStatement.readAddressQuery);
				
				statement.setLong(1, id);
				
				ResultSet result = statement.executeQuery();
				
				while(result.next()) {
					address.street = result.getString("street");
					address.city = result.getString("city");
					address.postal_code = result.getLong("postalcode");
				}
				
				if(address == null) {
					throw new AppException("102");
				}
				connection.close();
				
			} catch(SQLException e) {
				e.printStackTrace();
			}
		} catch(AppException e) {
			e.printStackTrace();
		}
		
		return address;
	}
}
*/
package com.kpr.training.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.kpr.training.connection.Connections;
import com.kpr.training.exception.AppException;
import com.kpr.training.model.Address;
import com.kpr.training.queryStatement.QueryStatement;

public class AddressServiceRead {
public Address readAddress(long id) {
		
		Address address = null;
		
		try {
			if(id == 0) {
				throw new AppException("104");
			}
			try {

				Connection connection = Connections.getConnection();
				
				PreparedStatement statement = connection.prepareStatement(QueryStatement.readAddressQuery);
				
				statement.setLong(1, id);
				
				ResultSet result = statement.executeQuery();
				
				while(result.next()) {
					address.street = result.getString("street");
					address.city = result.getString("city");
					address.postal_code = result.getLong("postalcode");
				}
				
				if(address == null) {
					throw new AppException("102");
				}
				connection.close();
				
			} catch(SQLException e) {
				e.printStackTrace();
			}
		} catch(AppException e) {
			e.printStackTrace();
		}
		
		return address;
	}
}
