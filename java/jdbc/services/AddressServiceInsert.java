/*
Problem Statement:
 To Perform Insert Address in Address Service
Entity:
- Address
-the class named as AddressServiceInsert
Method Signature:
 public long insert(Address address);
Jobs to be Done:
 1.create a class called AddressServiceInsert
 2.Create a addressId of type is long
 3.Check whether to get PostalCode()  is equal to 0, then throw new AppException 102
 4.Establish the Connection by getConnection method of Connections Class and store it to connection of Connection type
 5.Prepare the preparedStatement with insertAddressQuery query and store it to statement of preparedStatement
 6.use statement object,to getStreet(),getCity(),getPostalCode() values of address
 7.To execute,executeupdate() method is used  and store it to addressId
 8.Check whether the  is equal 0 then throw new AppException 103
 9.close the connection using Close() method,and return the addressId.
 Pseudo Code:
public class AddressServiceInsert {
	
	public long addressId ;
	
  public long insert(Address address)throws SQLException {
			
		try {
			if(address.getPostalCode() == 0) {
				throw new AppException("102");
			}
				
			Connection connection = Connections.getConnection();
				
			PreparedStatement statement = connection.prepareStatement(QueryStatement.insertAddressQuery);
				
			statement.setString(1, address.getStreet());
			statement.setString(2, address.getCity());
			statement.setLong(3, address.getPostalCode());
				
			addressId = statement.executeUpdate();
				
			if(addressId == 0) {
				throw new AppException("103");
			}
			connection.close();
				
		} catch(AppException e) {
			e.printStackTrace();
		}
	return addressId;
	}

}
*/
package com.kpr.training.service;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import com.kpr.training.connection.Connections;

import com.kpr.training.exception.AppException;
import com.kpr.training.model.Address;
import com.kpr.training.queryStatement.QueryStatement;

public class AddressServiceInsert {
	
	public long addressId ;
	
  public long insert(Address address)throws SQLException {
			
		try {
			if(address.getPostalCode() == 0) {
				throw new AppException("102");
			}
				
			Connection connection = Connections.getConnection();
				
			PreparedStatement statement = connection.prepareStatement(QueryStatement.insertAddressQuery);
				
			statement.setString(1, address.getStreet());
			statement.setString(2, address.getCity());
			statement.setLong(3, address.getPostalCode());
				
			addressId = statement.executeUpdate();
				
			if(addressId == 0) {
				throw new AppException("103");
			}
			connection.close();
				
		} catch(AppException e) {
			e.printStackTrace();
		}
	return addressId;
	}

}

