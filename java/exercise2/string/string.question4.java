Requirement:
Consider the following string:
    String hannah = "Did Hannah see bees? Hannah did.";
    - What is the value displayed by the expression hannah.length()?
    - What is the value returned by the method call hannah.charAt(12)?
    - Write an expression that refers to the letter b in the string referred to by hannah.
	
Entities:
 There is no Entities.
 
FunctionDeclaration:
 There is no function declaration.
 
Explanation:
 
 32 is the value displayed by the expression hannah.length()
 e is the value returned by the method call hannah.charAt(12)
 hannah.charAt(15)
 
 