Requirement:
To Write a program that computes your initials from your full name and displays them.
Entities:
ComputeInitials class is used in the program.

Functiondeclaration:
there is no function declared in this program.

Jobs to be Done:
1.first,ComputeInitials class is created.
2.In string datatype ,myname variable is assigned.
3.by calculating the length, is given as myname.length
4.using for loop ,intialization and condition is given.
5.print the result.

Sample program:

public class ComputeInitials {
    public static void main(String[] args) {
        String myName = "James Bond";
        StringBuffer myInitials = new StringBuffer();
        int length = myName.length();

        for (int i = 0; i < length; i++) {
            if (Character.isUpperCase(myName.charAt(i))) {
                myInitials.append(myName.charAt(i));
            }
        }
        System.out.println("My initials are: " + myInitials);
    }
}