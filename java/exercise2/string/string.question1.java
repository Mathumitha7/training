Requirement:
To sort and print following String[] alphabetically ignoring case. Also convert and print even indexed Strings into uppercase
       { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" }

Entities:
The Sort class is used in this program

FunctionDeclaration:
There is no functiondeclaration in this program.

Jobs to be done:
  1.import array function.
  2.create a  public class sort. 
  3.initialize the array.
  4.using for loop arrange the array in alphabetic order.
  5.print the output.


Sample program:

import java.util.Arrays; 
public class Sort{
    public static void main(String[] args){
        String [] place = {"Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" };
        Arrays.sort(place);
        System.out.println(Arrays.toString(place));
        for(int i=0;i<place.length;i++)
        {
            if(i%2==0) {
                place[i] = place[i].toUpperCase();
            }
        }
        System.out.print(Arrays.toString(place));
    }
}