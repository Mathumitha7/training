Requirement:
 How long is the string returned by the following expression? What is the string?
"Was it a car or a cat I saw?".substring(9, 12)

Entities:
 There is no entities
 
 Functiondeclaration:
 There is no functiondeclaration.
 
 Explanation:
 It's 3 characters in length: car. It does not include the space after car.
