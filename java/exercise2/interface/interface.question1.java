Requirement:
a)What methods would a class that implements the java.lang.CharSequence interface have to implement?
b)What is wrong with the following interface? and fix it.
    public interface SomethingIsWrong {
        void aMethod(int aValue){
            System.out.println("Hi Mom");
        }
    }
c)Is the following interface valid?
    public interface Marker {}

Entities:
SomethingIsWrong class are used in this program.

Functiondeclaration:
There is no function declaration.

solution:
a)charAt, length, subSequence, and toString.
b)It has a method implementation in it. Only default and static methods have implementations.
c)Yes. Methods are not required. Empty interfaces can be used as types and to mark classes without requiring any particular method implementations. For an example of a useful empty interface, see java.io.Serializable.
