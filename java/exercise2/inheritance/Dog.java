Requirement:
To demonstrate inheritance,overloading,overriding using Dog.
Entities:
    the class dog is used in this program.

Function Declaration:
    sound() is the functiondeclaration in this program

Jobs to be done:
    1. Declare the class Dog.
    2. call the method sound().
    3. Print the given statement.
	
program:

public class Dog extends Animal {
    public void sound() {
        System.out.println("the dog barks");
    }
}