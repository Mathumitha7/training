Entities:
  The class Snake is used here.

Function Declaration:
    sound() is the function declared in this program.

Jobs to be done:
    1. Declare the class Snake.
    2. call the method sound().
    3. To Print the given statement.
	
program:

public class Snake extends Animal{
    public void sound() {
        System.out.println("the snake crawls");
    }
}