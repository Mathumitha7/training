Entities:
    The class Cat is used here.

Function Declaration:
    sound() is function declared.

Jobs to be done:
    1. Declare the class Cat.
    2. call the method sound().
    3. Print the the statement as given.

program:

public class Cat extends Animal{
    public void sound() {
        System.out.println("the cat meows");
    }
}