Requirement:
demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of objects

Entities:
the class named InheritanceDemo is used.

Functiondeclaration:
there is no functiondeclared in this program.

program:

public class InheritanceDemo{
    public static void main(String[] args){
        Animal animal = new Animal();
        Dog dog = new Dog();
        Cat cat = new Cat();
        Snake snake = new Snake();
        animal.sound();
        animal.run();
        animal.run(7);
        cat.sound();
        cat.run();
        cat.run(6);
        dog.sound();
        dog.run();
        dog.run(8);
        snake.sound();
        snake.run();
        snake.run(9);
    }
}

