Requirement:
To Consider the following two classes:

public class ClassA {
    public void methodOne(int i) {
    }
    public void methodTwo(int i) {
    }
    public static void methodThree(int i) {
    }
    public static void methodFour(int i) {
    }
}

public class ClassB extends ClassA {
    public static void methodOne(int i) {
    }
    public void methodTwo(int i) {
    }
    public void methodThree(int i) {
    }
    public static void methodFour(int i) {
    }
}
a) Which method overrides a method in the superclass?
b)Which method hides a method in the superclass?
c)What do the other methods do?

Entities:
 The classes ClassA,ClassB are used.

Functiondeclaration:
 methodOne,methodTwo,methodThree,methodFour are the functions declared in this program.
 
solution:
 
Answer a:methodTwo
Answer b:methodFour
Answer c: They cause compile-time errors.