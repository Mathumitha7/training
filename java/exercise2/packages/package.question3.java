Requirement:
Assume that you have written some classes. Belatedly, you decide that they should be split into three packages, as listed in the table below. Furthermore, assume that the classes are currently in the default package (they have no package statements).

Package Name	Class Name
mygame.server	Server
mygame.shared	Utilities
mygame.client	Client

 Do you think you'll need to make any other changes to the source files to make them compile correctly? If so, what?

Entities:
there is no entities:

Functiondeclaration:
There is no function declaration

solution:
Yes, you need to add import statements. Client.java and Server.java need to import the Utilities class, which they can do in one of two ways:

import mygame.shared.*;
       --or--
import mygame.shared.Utilities;
Also, Server.java needs to import the Client class:

import mygame.client.Client;