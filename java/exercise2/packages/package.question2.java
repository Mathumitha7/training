Requirement:
Assume you have written some classes. Belatedly, you decide they should be split into three packages, as listed in the following table.
  Furthermore, assume the classes are currently in the default package (they have no package statements).
        Package Name    Class Name
        mygame.server   Server
        mygame.shared   Utilities
        mygame.client   Client
To adhere to the directory structure, you will need to create some subdirectories in the development directory and
put source files in the correct subdirectories. What subdirectories must you create? Which subdirectory does each source file go in?

Entities:
there is no entities:

Functiondeclaration:
There is no function declaration

solution:
Within the mygame directory, you need to create three subdirectories: client, server, and shared.

In mygame/client/ place:
Client.java
In mygame/server/ place:
Server.java
In mygame/shared/ place:
Utilities.java
