Requirement:
Assume you have written some classes. Belatedly, you decide they should be split into three packages, as listed in the following table.
  Furthermore, assume the classes are currently in the default package (they have no package statements).
        Package Name    Class Name
        mygame.server   Server
        mygame.shared   Utilities
        mygame.client   Client
  Which line of code will you need to add to each source file to put each class in the right package?
  
 Entities:
  There is no entities
  
 Functiondeclaration:
 There is no function declaration.
 
 Solution:
 The first line of each file must specify the package
In Client.java add:
package mygame.client;
In Server.java add:
package mygame.server;:
In Utilities.java add:
package mygame.shared;