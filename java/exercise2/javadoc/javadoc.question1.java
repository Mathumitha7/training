Requirement:
To Use the Java API documentation for the Box class (in the javax.swing package) to help you answer the following questions.
    - a)What static nested class does Box define?
    - b)What inner class does Box define?
    - c)What is the superclass of Box's inner class?
    - d)Which of Box's nested classes can you use from any class?
    - e)How do you create an instance of Box's Filler class?

Entities:
there is no class used here.

Functiondeclaration:
There is no function declaration

solution:

answer a :Box.Filler
answer b:Box.AccessibleBox
answer c:[java.awt.]Container.AccessibleAWTContainer
answer d:Box.Filler
answer e:new Box.Filler(minDimension, prefDimension, maxDimension)