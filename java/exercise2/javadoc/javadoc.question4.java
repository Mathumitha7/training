Requirement:
What Integer method would you use to convert a string expressed in base 5 into the equivalent int?
  For example, how would you convert the string "230" into the integer value 65? Show the code you would use to accomplish this task.
  
Entities:
there is no class used here.

Functiondeclaration:
There is no function declaration

solution
valueOf. Here's how:

String base5String = "230";
int result = Integer.valueOf(base5String, 5);
