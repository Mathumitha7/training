Requirment:
    Create a program that is similar to the previous one but instead of reading integer arguments, it reads floating-point arguments.
    
Entity
   class named CommandLineFloatAdder is used.

Function Declaration:
   No function is declared in this program.
    
Jobs to be Done
    1.Create a Class called CmdLineFloatAdder and declare two variables of double datatype and initialize as 0.
    2.Check the Condition wheather the input arguments is not less than 4.
    3.If the inputs are less than 4 then it prints a error message as Enter inputs more than 1
    4.If the Condition satisfies then the args are converted to Float usint the parseFloat method available in Float Class
    5.Then the iterated values are added to a double variable declared previously and prints it..
    
Program:

public class CommandLineFloatAdder {
    public static void main(String[] args){
        double sum = 0;
        double val = 0;
        if(args.length > 1) {
            for (int i = 0;i<args.length;i++) {
               val = Float.parseFloat(args[i]);
               sum = sum +val;
            }
            System.out.println(sum);
        } else {
            System.out.println("Enter More than 4 Values");
        }
    }
}