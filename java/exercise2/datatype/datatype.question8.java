Requirement:
     To Create a program that reads an unspecified number of integer arguments from the command line and adds them together.
     For example, suppose that you enter the following: java Adder 1 3 2 10
	
   Entities:
    Class named Adder is used.
  
   Function Declaration:
     No function is declared in this program.
	 
   Jobs to be Done
    1.Create a CommandLineIntAdder Class and initialize two values as sum and val and declare as 0.
    2.Check the Command Line arguments is greater than 4 with if Statment,
    3.If the Valus passed are greater than 4 then using the for loop convert the String arguments to Integer using 
      parseInt of Integer Class.
    4.Then add the argument values into sum and print the sum Value.
    5.If the Arguments passed are less than 4, Print the Error Message as Enter 4 Values.
	
program:

public class Adder {
    public static void main(String[] args) {
    int numArgs = args.length;

        if (numArgs < 2) {
            System.out.println("This program requires two command-line arguments.");
        } else {
        int sum = 0;

        for (int i = 0; i < numArgs; i++) {
                sum += Integer.valueOf(args[i]).intValue();
        }

            System.out.println(sum);
        }
    }
}