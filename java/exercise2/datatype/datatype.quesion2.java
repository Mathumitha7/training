Requirement:
   To print the type of the result value of following expressions
  - 100 / 24
  - 100.10 / 10
  - 'Z' / 2
  - 10.5 / 0.5
  - 12.4 % 5.5
  - 100 % 56

Entities:
   The class named Expressions is used.

Function Declaration:
   No function is declared in this program.

Job to be done:
    1. create a class named Expressions.
	2. print the type of results in output.
	
Program:

public class Expressions {
    public static void main (String args[]) {
        System.out.println(100 / 24);
        System.out.println(100.10 / 10);
        System.out.println('z' / 2);
        System.out.println(10.5 / 0.5);
        System.out.println(12.4 % 5.5);
        System.out.println(100 % 56);
    }
}

