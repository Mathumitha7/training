Requirement:
To print fibinocci series using for loop 

Entities:
 The class is named as  Fibonacci.
 
 Function Declaration:
 
 There is no function declared in this program.
 
Jobs to be done:
 1.Analyse the given statement.
 2.assign the vaues to num1,num2.
 3.using forcondition,assign the value 1 to i and give the condition and increment i  and enters into the loop.
 4.print the num1.
 5.sum of values num1 and num2 which is stores in values.
 6.assign num1 equals to num2 and num2 which equals to values.
 7.repeat the loop until statisfies the condition.

Solution:

public class Fibonacci{

    public static void main(String[] args) {

        int num1 = 0, num2 = 1;
        for (int i=1; i < 7; ++i)
        {
            System.out.print(num1+" ");
            int values = num1 + num2;
            num1 = num2;
            num2 = values;
        }
    }
}