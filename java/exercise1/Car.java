abstract class Vehicle{
    public abstract void color();
}
public class Car extends Vehicle{

   public void color(){
	System.out.println("Silver");
   }
   public static void main(String args[]){
	Vehicle obj = new Car();
	obj.color();
   }
}