Requirement:
To change the following program, explain why the value "6" is printed twice in a row:
       class PrePostDemo {
           public static void main(String[] args){
               int i = 3;
               i++;
               System.out.println(i);    // "4"
               ++i;
               System.out.println(i);    // "5"
               System.out.println(++i);  // "6"
               System.out.println(i++);  // "6"
               System.out.println(i);    // "7"
           }
       }

Entities:
   It requires class named PrePostDemo 
   
Function declaration:
   There is  no function declaration.
   
Jobs to be done:
    1.Read the code
	2.Analyse the code
	3.write the solution
	
solution:
In above code line (6),the pre-increment operation is used where Value is incremented first and then result is computed.
Nextline,post-increment(i++) is performed where Value is first used for computing the result and then incremented.

   
   
  
	   