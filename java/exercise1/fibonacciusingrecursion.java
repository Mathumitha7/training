Requirement:
 To print  the fibinocci series using recursion 

Entities:
 The class is named as  FibonacciCalc.
 
 Function Declaration:
 
 The functiondeclaration is fibonacciRecursion in this program.
 
Jobs to be done:
 1.get the input from user.
 2.using if condition,if n equals to 0 then returns 0 
 3.Next,check the condition n equals to 1 or n equals to 2 ,then returns 1
 4.sum of fibonacciRecursion(n-2) and fibonacciRecursion(n-1) and return the value.
 5.print the number.
 6.using for condition,print the values.
 7.repeat the loop until statisfies the condition.



solution:

public class FibonacciCalc{
	public static int fibonacciRecursion(int n){
	if(n == 0){
		return 0;
	}
	if(n == 1 || n == 2){
			return 1;
		}
	return fibonacciRecursion(n-2) + fibonacciRecursion(n-1);
	}
    public static void main(String args[]) {
	int maxNumber = 10;
	System.out.print("Fibonacci Series of "+maxNumber+" numbers: ");
	for(int i = 0; i < maxNumber; i++){
			System.out.print(fibonacciRecursion(i) +" ");
		}
	}
}




