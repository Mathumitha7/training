Requirements:
 What's wrong with the following program? And fix it.

    public class SomethingIsWrong {
        public static void main(String[] args) {
            Rectangle myRect;
            myRect.width = 40;
            myRect.height = 50;
            System.out.println("myRect's area is " + myRect.area());
        }
    }
	
Entities:
  SomethingIsWrong(given)

Function Declaration:
  There is no function is declared in class
Jobs To Be Done:
     1.To create an object myRect 
     2.Initialise values to the object
	 3.Print the values
Solution:
object will not be created for rectangle because the  object creation is incomplete. It will create a null value for myRect object. 
The code should be 

public class SomethingIsWrong {
        public static void main(String[] args) {
            Rectangle myRect = new Rectangle();
            myRect.width = 40;
            myRect.height = 50;
            System.out.println("myRect's area is " + myRect.area());
        }
    }
	 
	 