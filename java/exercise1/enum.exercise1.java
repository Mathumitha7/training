Requirement:
    To compare the enum values using equal method and == operator

Entities:
    There is no entities.
	
Functiondeclaration:
    There is no function declared in this program.

Jobs to be done:
    1.To compare values using equalmethod.
     2. To compare values using == operator.	

Solution:
   enum is a special data type to which the user can assign predefined constants to variable. 
   Generally, == is NOT a viable alternative to equals. When it is, however (such as with enum), there are two important differences to consider:

   == never throws NullPointerException
   
sample code:
      enum Color { PINK, WHITE };

      Color nothing = null;
      if (nothing == Color.PINK); 
      if (nothing.equals(Color.PINK)); 
      == is subject to type compatibility check at compile time

      enum Color { PINK, WHITE };
      enum Chiral { LEFT, RIGHT };

      if (Color.PINK.equals(Chiral.LEFT)); 
      if (Color.PINK == Chiral.LEFT); 

