Requirement:
To Change the following program to use compound assignments:
class ArithmeticDemo {

    public static void main (String[] args){
          
        int result = 1 + 2; // result is now 3
        System.out.println(result);

        result = result - 1; // result is now 2
        System.out.println(result);

        result = result * 2; // result is now 4
        System.out.println(result);

        result = result / 2; // result is now 2
        System.out.println(result);

        result = result + 8; // result is now 10
        result = result % 7; // result is now 3
        System.out.println(result);

    }
}

entities:
The class named as ArithmeticDemo.

FunctionDeclaration:
There is no functiondeclaration.

Jobs to be done:
 1.Read the code
 2.Analyse the code
 3.Replace the operator with compoundassignments.
 4.Check similarities with above the code.
 
 solution:
 
 class ArithmeticDemo {

    public static void main (String[] args){
        int result = 3;
        System.out.println(result);

        result -= 1; // result is now 2
        System.out.println(result);

        result *= 2; // result is now 4
        System.out.println(result);

        result /= 2; // result is now 2
        System.out.println(result);

        result += 8; // result is now 10
        result %= 7; // result is now 3
        System.out.println(result);

    }
}
