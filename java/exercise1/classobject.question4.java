Requirements:
Given the following class, called NumberHolder,
 write some code that creates an instance of the class and initializes its two member variables with provided values,and then displays the value of each member variable.

          public class NumberHolder {
          public int anInt;
          public float aFloat;
            }
	
Entities:
         NumberHolder(Given)
         NumberHolderDisplay (User Defined)
Function Declaration:
 There is no function declaration.

Jobs To be Done:
           1.To create a class NumberHolderDisplay
           2.To create an object aNumberHolder
		   3.Initialise the values to the object
		   4.Print the values
Solution:
    public class NumberHolderDisplay 
    {
      public static void main(String[] args) 
      {
	    NumberHolder aNumberHolder = new NumberHolder();
	    aNumberHolder.anInt = 1;
	    aNumberHolder.aFloat = 2.3f;
	    System.out.println(aNumberHolder.anInt);
	    System.out.println(aNumberHolder.aFloat);
    }
  }





