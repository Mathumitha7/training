Requirement:
Demonstrate abstract classes using Shape class.
    - Shape class should have methods to calculate area and perimeter
    - Define two more class Circle and Square by extending Shape class and implement the calculation for each class respectively

Entities:
 Abstract class is used in this program.
 class Triangle,Square,Rectangle,Circle are used.
 
Functiondeclaration:
  two functions area and perimeter is declared.
Jobs to be done:
  1.declare a abstract class shape.
  2.class Triangle,Rectangle,Circle,Square inherits shape.
  3.area and perimeter of each shape is calculated in their respective class.
  4.in the main class area create an object for each class and call area and perimeter  function.
 
Solution:

import java.lang.Math;

abstract class Shape
{
 abstract void area();
 abstract void perimeter();
 double area;
 double perimeter;
}

class Triangle extends Shape
{ 
 double b=50,h=15,c=20;
 void area()
 {
  area = (b*h)/2;
  System.out.println("area of Triangle "+area);
 }
 void perimeter()
 {
	 perimeter=h+b+c;
	 System.out.println("Perimeter of Triangle "+perimeter);
}
}
class Square extends Shape
{
  double a=10;
  void area()
  {
  area=a*a;
  System.out.println("area of Square " +area);
  }
  void perimeter()
  {
	  perimeter=4*a;
	  System.out.println("perimeter of Square " +perimeter);
  }
}
class Rectangle extends Shape
{
 double w=70,h=20;
 void area()
 {
  area = w*h;
  System.out.println("area of Rectangle "+area);
 }
 void perimeter()
 {
	 perimeter=2*(w+h);
	 System.out.println("perimeter of Rectangle "+perimeter);
}
}

class Circle extends Shape
{
 double r=5;
 void area()
 {
  area = Math.PI * r * r;
  System.out.println("area of Circle "+area);
 }
 void perimeter()
 {
	 perimeter=2*Math.PI*r;
	 System.out.println("perimeter of Circle "+perimeter);
}
}

public class Area
{
 public static void main(String [] args)
 {
  Triangle t= new Triangle();
  Rectangle r =new Rectangle();
  Circle c =new Circle();
  Square s=new Square();
  t.area();
  t.perimeter();
  r.area();
  r.perimeter();
  c.area();
  c.perimeter();
  s.area();
  s.perimeter();
 }
}