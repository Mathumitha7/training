Requirement:
Write a test program containing the previous code snippet and make aNumber 3. What is the output of the program?
if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
               System.out.println("third string")

Entities:
 No class is used.
		
Function Declaration:
   There is no function declared in this program.		

Jobs to be Done:
  1.Read the code.
  2.Give the value of aNumber equals to 3.
  3.Display the output of code.
  
  Solution:
  aNumber=3;
  if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
               System.out.println("third string")
  
  3 is greater than or equal to 0, so it enters into the second if statement. The second if statement's test fails because 3 is not equal to 0. Thus, the else clause executes and second string is displayed. The final println is completely outside of the if statement, so it always gets executed, and thus third string is displayed.

output: 
  second string
  third string