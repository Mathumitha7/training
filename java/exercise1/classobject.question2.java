Requirement:
  To find the output of the following code
  IdentifyMyParts a = new IdentifyMyParts();
Requirements:
    
What is the output from the following code:
   IdentifyMyParts a = new IdentifyMyParts();
   IdentifyMyParts b = new IdentifyMyParts();
    a.y = 5;
    b.y = 6;
    a.x = 1;
    b.x = 2;
    System.out.println("a.y = " + a.y);
    System.out.println("b.y = " + b.y);
    System.out.println("a.x = " + a.x);
    System.out.println("b.x = " + b.x);
    System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x); IdentifyMyParts b = new IdentifyMyParts();
    a.y = 5;
    b.y = 6;
    a.x = 1;
    b.x = 2;
    System.out.println("a.y = " + a.y);
    System.out.println("b.y = " + b.y);
    System.out.println("a.x = " + a.x);
    System.out.println("b.x = " + b.x);
    System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x);

Entities:
  It requires a class named IdentifyMyParts.

Function Declaration:
  No function is declared here.
  
Job to be done:
  1. Read the code
  2. Analyse the code
  3. Check the values of x and y in the code
  4. Display the result
  
Solution:
output:
a.y= 5
b.y= 6
a.x= 2
b.x= 2
IdentifyMyParts.x= 2

Here, x is a static variable. The value is assigned last to a static variable will be shared across all instance of the class. So,there is only one value for x.
