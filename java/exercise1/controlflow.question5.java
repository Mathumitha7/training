Requirement:
To  write a program Using braces, { and }, to further clarify the code.

       if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
               System.out.println("third string");

Entities:
    No class is used.
		
Function Declaration:
    There is no function declared in this program.		
Jobs to be done:
    1.Clarify the code using braces,{ and }.

Solution:			   
if (aNumber >= 0) {
    if (aNumber == 0) {
        System.out.println("first string");
    } else {
        System.out.println("second string");
    }
}

System.out.println("third string");