Requirement:
To find the output do you think the code will produce if aNumber is 3?

        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string")

Entities:
   No class is used.
		
Function Declaration:
   There is no function declared in this program.		

Jobs to be Done:
  1.Read the code.
  2.Analyse the code.
  3.Find the output of code.
  
 Solution:
 
  second string
  third string