Requirement:
To print fibinocci series using while loop 

Entities:
 The class is named as  Fibonacci.
 
 Function Declaration:
 
 There is no function declared in this program.
 
Jobs to be done:
 1.Analyse the given statement.
 2.assign the vaues to count,num1,num2.
 3.using while condition,check the condition and enters into the loop.
 4.print the values num1.
 5.sum of values num1 and num2 which is stores in sumofPrevTwo.
 6.assign num1 equals to num2 and num2 which equals to sumofPrevTwo.
 7.repeat the loop until statisfies the condition.

solution:
public class Fibonacci{

    public static void main(String[] args) {

        int count = 20, num1 = 0, num2 = 1;
        int i=1;
        while(i<=count)
        {
            System.out.print(num1+" ");
            int sumOfPrevTwo = num1 + num2;
            num1 = num2;
            num2 = sumOfPrevTwo;
            i++;
        }
    }
}