class Hospital
{ 
    public String hospitalId; 
    private String hospitalName;
    protected String area;
    public Hospital(String hospId,String hospName,String hosparea) { //Parameterized Constructor 
        hospitalId = hospId;
        hospitalName = hospName;
        area= hosparea;
    }
    public void printInfo() {  
        System.out.println("HospitalName: " + hospitalName);
        HospitalDept hospdept = new HospitalDept("ortho");
        hospdept.display();  
    }
    class HospitalDept{ 
        private String hospitalDept;
        
        public HospitalDept(String hospDept) {
            hospitalDept = hospDept;
        }
        public void display() {
            System.out.println("HospDept: " + hospitalDept);
        }
    }
}
class Main{
    public static void main(String args[]) {
        Hospital hosp = new Hospital("70001","Mathu","07-03-2001 ");
        hosp.printInfo(); 
        System.out.println("HospitalId: " + hosp.hospitalId); 
        System.out.println("Hospitallocation: " + hosp.area);
    }
}