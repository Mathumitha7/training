why cant we create the object of abstractclass?
 Because, the abstract class are incomplete,they have abstract methods that have nobody so if java allows you to create object of this class then if someone class the abstract method using then what would happen? there would be no actual implementation of the method to invoke.also because an object is concrete.An abstract class is like a template, so you have to extend it and built on it before you can use it.
 
 program:
 
 abstract class AbstractSample {
    public void myMethod(){
	   System.out.println("welcome");
	}
	 abstract public void anotherMethod();
}
public class Sample extends AbstractSample{

    public class AnotherMethod {
	   System.out.println("Abstract method");
  }
  public static void main(String args[])
  {
      AbstractSample a = new AbstractSample();
	  a.anotherMethod();
  }
 } 
error:
AbstractSample is abstract;cannot be instantiated
AbstractSample a = new AbstractSample();
                   ^
				   

	
	 