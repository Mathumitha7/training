Requirement:
     To Demonstrate abstract classes using Shape class.
Entities:
   abstract class shape is used.
   classes Circle1,Rectangle,TestAbstraction1 are used.
   
FunctionDeclaration:
   draw function is declared in this program.
    

Jobs to be done:
  1.create abstract class shape and declare a function named draw.
  2.create a class Rectangle and inherit shape.
  3.create a class circle1 and inherit shape.
  4.create a main class TestAbstraction1 and create an object for shape and call draw.


Solution:
abstract class Shape{  
abstract void draw();  
}   
class Rectangle extends Shape{  
void draw(){System.out.println("drawing rectangle");}  
}  
class Circle1 extends Shape{  
void draw(){System.out.println("drawing circle");}  
}  
 
class TestAbstraction1{  
public static void main(String args[]){  
Shape s=new Circle1();
s.draw();  
}  
}  
