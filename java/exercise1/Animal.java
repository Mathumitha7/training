class Animal {
    protected void display() {
        System.out.println("It is animal");
    }
}

class Dog extends Animal {
    public static void main(String[] args) {

        Dog dog = new Dog();
        dog.display();
    }
} 
