Requirement:
TO write a program Using only spaces and line breaks, reformat the code snippet to make the control flow easier to understand.
          if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
            else System.out.println("second string");
             System.out.println("third string");

Entities:
 No class is used.
		
Function Declaration:
   There is no function declared in this program.		

Jobs to be Done:
  1.To reformat the code using only spaces and line breaks
  			   
solution:

if (aNumber >= 0)
    if (aNumber == 0)
        System.out.println("first string");
    else
        System.out.println("second string");

System.out.println("third string");


			   